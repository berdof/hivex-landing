const routes = require('next-routes');

// Name   Page      Pattern
module.exports = routes() // ----   ----      -----
  .add('index')
  .add('work-with-us')
  .add('technologies', '/technologies/:technologyName')
  .add('interaction-models', '/interaction-models/:modelType')
  .add('cases', '/cases/:caseName?');
