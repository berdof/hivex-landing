export const TechnologiesSubroutes = [
  {
    name: 'Web-Frontend',
    route: '/technologies/web-frontend',
  },
  {
    name: 'Web-Backend',
    route: '/technologies/web-backend',
  },
  {
    name: 'Mobile',
    route: '/technologies/mobile',
  },
  {
    name: 'DevOps',
    route: '/technologies/dev-ops',
  },
  {
    name: 'Machine Learning & AI',
    route: '/technologies/machine-learning',
  },
  {
    name: 'Quality Assurance',
    route: '/technologies/qa',
  },
  {
    name: 'Big data',
    route: '/technologies/big-data',
  },
];

export const InteractionModelTypes = [
  {
    link: '/interaction-models/dedicated-team',
    value: 'Dedicated team',
  },
  {
    link: '/interaction-models/hourly-payment',
    value: 'Hourly payment',
  },
];