import { Component } from 'react';
import Head from 'next/head';
import classnames from 'classnames';

import Page from 'components/Page/Page';
import Form from 'components/Form/Form';
import Footer from 'components/Footer/Footer';
import SocialLinks from 'components/SocialLinks/SocialLinks';

import styles from './WorkWithUs.module.scss';

export default function WorkWithUs() {
  return (
    <Page>
      <Head>
        <title>Hivex - Work with Us</title>
      </Head>
      <div className={styles.container}>
        <div className={styles.containerIn}>
          <Form formClassName={styles.form}/>
          <div className={styles.credentials}>
            <div className={styles.item}>
              <h2 className={styles.itemTitle}>Address</h2>
              <span className={styles.itemValue}>
                Estonia, Tallin, Kesklinna linnaosa, Tuukri tn 19-315
              </span>
            </div>
            {/*<div className={styles.item}>*/}
            {/*  <h2 className={styles.itemTitle}>Phone</h2>*/}
            {/*  <span className={styles.itemValue}>+7 (945) 765 35-35</span>*/}
            {/*</div>*/}
            <div className={styles.item}>
              <h2 className={styles.itemTitle}>Email</h2>
              <a
                className={styles.itemMailLink}
                href="mailto:hello@hivex.group"
              >
                hello@hivex.group
              </a>
            </div>
            {/*<div className={styles.item}>*/}
            {/*  <h2 className={styles.itemTitle}>Social network</h2>*/}
            {/*  <SocialLinks />*/}
            {/*</div>*/}
          </div>
        </div>
        <Footer className={styles.footer}/>
      </div>
    </Page>
  );
}
