import { Component } from 'react';
import Head from 'next/head';
import classnames from 'classnames';
import ReactFullpage from '@fullpage/react-fullpage';
import Slider from 'react-slick';

import Page from 'components/Page/Page';
import Button from 'components/Button/Button';
import Hexagons from 'components/Hexagons/Hexagons';
import Form from 'components/Form/Form';
import Footer from 'components/Footer/Footer';
import ReadMoreButton from 'components/ReadMoreButton/ReadMoreButton';

import IconDedicatedTeam from 'public/assets/models-dedicated-team.svg';
import IconHourly from 'public/assets/models-hourly.svg';
import IconTechFront from 'public/assets/technologies/technology-front.svg';
import IconTechMobile from 'public/assets/technologies/technology-mobile.svg';
import IconTechAI from 'public/assets/technologies/technology-ai.svg';
import IconTechQA from 'public/assets/technologies/technology-qa.svg';
import IconTechBack from 'public/assets/technologies/technology-backend.svg';
import IconTechDevOps from 'public/assets/technologies/technology-devops.svg';
import IconTechBigData from 'public/assets/technologies/technology-bigdata.svg';
import Arrow from 'public/assets/arrow.svg';

import styles from './Index.module.scss';

const originalColors = [
  'rgba(250,250,250, 0)',
  'rgba(250,250,250, 0)',
  'rgba(250,250,250, 0)',
  'rgba(250,250,250, 0)',
  'rgba(250,250,250, 0)',
  'rgba(250,250,250, 0)',
  'rgba(250,250,250, 0)',
];

const sliderSettings = {
  dots: true,
  infinite: false,
  autoplay: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerPadding: '50px',
  nextArrow: <ArrowNext />,
  prevArrow: <ArrowNext />,
};

function ReadFullBtn({ link = '/' }) {
  return (
    <a className={styles.readFullBtn} href={link}>
      Read full review
    </a>
  );
}

function ArrowNext(props) {
  return (
    <button {...props}>
      <Arrow />
    </button>
  );
}

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sectionsColor: [...originalColors],
      fullpages: [
        {
          text: 'Section 1',
        },
        {
          text: 'Section 2',
        },
        {
          text: 'Section 3',
        },
      ],
    };
  }

  componentDidMount() {
    window.fullpage_api = fullpage_api;
  }

  onLeave = (origin, destination, direction) => {
    console.log('onLeave', { origin, destination, direction });
    // arguments are mapped in order of fullpage.js callback arguments do something
    // with the event
  };

  moveSectionDown = () => {
    fullpage_api.moveSectionDown();
  };

  moveTo = (path) => {
    fullpage_api.moveTo(path);
  };

  renderMainSlide = () => {
    return (
      <div
        key="main"
        className={classnames(['section', styles.section, styles.mainSlide])}
      >
        <article className={styles.textBlock}>
          <h2 className={styles.textBlockTitle}>
            Get professional product advisory and a full-stack team for software
            delivery
          </h2>
          <p className={styles.textBlockParagraph}>
            We will increase your team with missing experts or form a new one
            for the project goals.
          </p>
        </article>
        <Button className={styles.starkWork} onClick={() => this.moveTo('contactUs')}>
          Start work with us
        </Button>
        <div className={styles.hexagons}>
          <Hexagons />
        </div>
      </div>
    );
  };

  renderInteractionModelsSlide = () => {
    const interactionModels = [
      {
        id: 0,
        title: 'Dedicated team',
        icon: <IconDedicatedTeam className={styles.modelsListIcon} />,
        text:
          'The customer receives with a team of engineers at his disposal,selected according to the requirements of the project.The team becomes the part of his company and lives up with the challenge.',
        link: '/interaction-models/dedicated-team',
      },
      {
        id: 1,
        title: 'Time rates',
        icon: <IconHourly className={styles.modelsListIcon} />,
        text:
          'Using this model, the customer pays for the time of specialists who work on the software product with their direct involvement',
        link: '/interaction-models/hourly-payment',
      },
    ];

    return (
      <div
        key="howWeWork"
        className={classnames(['section', styles.section, styles.modelsSlide])}
      >
        <article className={styles.textBlock}>
          <h2 className={styles.textBlockTitle}>Interaction models</h2>
          <p className={styles.textBlockParagraph}>
            We will expand your team with missing experts or form a new one to
            meet project objectives. Simply select the appropriate interaction
            model
          </p>
        </article>
        <ul className={styles.modelsList}>
          {interactionModels.map(({ icon, ...item }) => (
            <li key={item.id} className={styles.modelsListItem}>
              {icon}
              <h3 className={styles.modelsListItemTitle}>{item.title}</h3>
              <p className={styles.modelsListItemDescription}>{item.text}</p>
              <ReadMoreButton link={item.link} />
            </li>
          ))}
        </ul>
      </div>
    );
  };

  renderOperateSlide = () => {
    const technologies = [
      {
        icon: <IconTechFront class={styles.technologiesListIcon} />,
        title: 'Web Frontend',
        link: '/technologies/web-frontend',
      },
      {
        icon: <IconTechMobile class={styles.technologiesListIcon} />,
        title: 'Mobile',
        link: '/technologies/mobile',
      },
      {
        icon: <IconTechAI class={styles.technologiesListIcon} />,
        title: 'Machine Learning & AI',
        link: '/technologies/machine-learning',
      },
      {
        icon: <IconTechQA class={styles.technologiesListIcon} />,
        title: 'Quality Assurance',
        link: '/technologies/qa',
      },
      {
        icon: <IconTechBack class={styles.technologiesListIcon} />,
        title: 'Web backend',
        link: '/technologies/big-data',
      },
      {
        icon: <IconTechDevOps class={styles.technologiesListIcon} />,
        title: 'DevOps',
        link: '/technologies/dev-ops',
      },
      {
        icon: <IconTechBigData class={styles.technologiesListIcon} />,
        title: 'Big data',
        link: '/technologies/big-data',
      },
    ];

    return (
      <div
        key="technologies"
        className={classnames([
          'section',
          styles.section,
          styles.technologiesSlide,
        ])}
      >
        <article className={styles.textBlock}>
          <h2 className={styles.textBlockTitle}>The way we operate</h2>
          <p className={styles.textBlockParagraph}>
            We create unique software for our customers that enables them to
            achieve best results.
          </p>
        </article>
        <ul className={classnames(styles.technologiesList)}>
          {technologies.map(({ link, icon, title }) => (
            <li key={link} className={styles.technologiesListItem}>
              {icon}
              <div className={styles.technologiesListItemInfo}>
                <h3 className={styles.technologiesListItemTitle}>{title}</h3>
                {false && <ReadMoreButton link={link} />}
              </div>
            </li>
          ))}
          <li
            className={classnames(
              styles.technologiesListItem,
              styles.technologiesListHelper
            )}
          >
            <h3 className={styles.technologiesListHelperTitle}>
              Have you got a unique project?
            </h3>
            <p className={styles.technologiesListHelperDescription}>
              We will advise you and help you find a solution
            </p>
            <Button secondary onClick={() => this.moveTo('contactUs')}>Find a solution</Button>
          </li>
        </ul>
      </div>
    );
  };

  renderPortfolioSlide = () => {
    const portfolio = [
      {
        id: 0,
        title: 'ASOmaker',
        link: '/cases/asomaker',
        image: '/assets/portfolio/asomaker-slide.png',
      },
      /*{
        id: 1,
        title: 'ASOmaker',
        link: '/cases/asomaker',
        image: '/assets/portfolio/asomaker-slide.png',
      },*/
    ];
    return (
      <div
        key="portfolio"
        className={classnames([
          'section',
          styles.section,
          styles.portfolioSlide,
        ])}
      >
        <article className={styles.textBlock}>
          <h2 className={styles.textBlockTitle}>Our projects</h2>
          <p className={styles.textBlockParagraph}>
            We are proud of our projects, as they demonstrate our demanding
            attitude to work and show a high level of performance
          </p>
        </article>
        <div className={styles.portfolioSlider}>
          <div className={styles.portfolioSliderBg}>
            <img
              className={styles.portfolioSliderBgHex1}
              src="/assets/hex-bg.svg"
              alt=""
            />
            <img
              className={styles.portfolioSliderBgHex2}
              src="/assets/hex-bg.svg"
              alt=""
            />
            <img
              className={styles.portfolioSliderBgHex3}
              src="/assets/hex-bg.svg"
              alt=""
            />
            <img
              className={styles.portfolioSliderBgHex4}
              src="/assets/hex-bg.svg"
              alt=""
            />
          </div>
          <Slider {...sliderSettings}>
            {portfolio.map((item) => (
              <div key={item.id} className={styles.portfolioSliderItem}>
                <div className={styles.portfolioSliderHeader}>
                  <h3 className={styles.portfolioSliderHeaderTitle}>
                    {item.title}
                  </h3>
                  <ReadMoreButton link={item.link} />
                </div>
                <div className={styles.portfolioSliderItemImageWrapper}>
                  <img
                    className={styles.portfolioSliderItemImage}
                    src={item.image}
                    alt={item.title}
                  />
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </div>
    );
  };

  renderReviewsSlide = () => {
    const reviews = [
      // {
      //   text: `I've been working with the guys since the very beginning of the
      //           project. I had an opportunity to observe the process of a team
      //           creation, their ability to reach the highest level of efficiency and
      //           their eagerness to get all possible benefits from a team working.
      //           We've been developing asomaker.com - a project with the increased
      //           level of complexity due to the following features: - technical
      //           complexity: this single-page application id based on the latest
      //           technological solutions such as React JS, MOBX`,
      //   author: 'Sergey Polyakov',
      //   position: 'CTO, Asomaker',
      //   avatar: 'assets/reviews/sergey-gusev.png',
      //   logo: 'assets/reviews/scentbird.svg',
      // },
      {
        text: `I've been working with the guys since the very beginning of the
                project. I had an opportunity to observe the process of a team
                creation, their ability to reach the highest level of efficiency and
                their eagerness to get all possible benefits from a team working.
                We've been developing asomaker.com - a project with the increased
                level of complexity due to the following features: - technical
                complexity: this single-page application id based on the latest
                technological solutions such as React JS, MOBX`,
        author: 'Sergey Polyakov',
        position: 'CTO, Asomaker',
        avatar: 'assets/reviews/sergey-polyakov.png',
        logo: 'assets/reviews/asomaker.png',
      },
    ];
    return (
      <div
        key="reviews"
        className={classnames(['section', styles.section, styles.reviewsSlide])}
      >
        <article className={styles.textBlock}>
          <h2 className={styles.textBlockTitle}>Reviews</h2>
          <p className={styles.textBlockParagraph}>
            We have worked with leading insurance companies, banks, retailers,
            car brands, state projects, eCommerce, media, etc.
          </p>
        </article>
        <Slider {...sliderSettings}>
          {reviews.map(({ text, author, position, avatar, logo }) => (
            <div className={styles.reviewsSlideItem}>
              <div className={styles.reviewsSlideItemBody}>
                <div className={styles.reviewsSlideInfo}>
                  <img
                    className={styles.reviewsSlideAvatar}
                    src={avatar}
                    width={145}
                    alt=""
                  />
                  <div className={styles.reviewsSlideInfoHead}>
                    <span className={styles.reviewsSlideAuthor}>{author}</span>
                    <span className={styles.reviewsSlidePosition}>
                      {position}
                    </span>
                    <img
                      className={styles.reviewsSlideLogo}
                      src={logo}
                      width={101}
                      alt=""
                    />
                  </div>
                </div>
                <div className={styles.reviewsSlideTextContainer}>
                  <p className={styles.reviewsSlideText}>{text}</p>
                  {/*<ReadFullBtn />*/}
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    );
  };

  renderFormSlide = () => {
    return (
      <div
        key="contactUs"
        className={classnames(['section', styles.section, styles.formSlide])}
      >
        <Form formClassName={styles.formArea} />
        <Footer className={styles.footer} />
      </div>
    );
  };

  render() {
    return (
      <Page>
        <Head>
          <title>Hivex</title>
        </Head>
        <div className={styles.containerLeft}>
          <ul id="sliderNav" className={styles.containerLeftMenu}>
            <li className={styles.leftMenuItem} data-menuanchor="howWeWork">
              <a href="#howWeWork">How we are working</a>
            </li>
            <li className={styles.leftMenuItem} data-menuanchor="technologies">
              <a href="#technologies">Technologies</a>
            </li>
            <li className={styles.leftMenuItem} data-menuanchor="portfolio">
              <a href="#portfolio">Portfolio</a>
            </li>
            <li className={styles.leftMenuItem} data-menuanchor="reviews">
              <a href="#reviews">Reviews</a>
            </li>
            <li className={styles.leftMenuItem} data-menuanchor="contactUs">
              <a href="#contactUs">Contact us</a>
            </li>
          </ul>
        </div>
        <div className={styles.wrapInCenter}>
          <ReactFullpage
            //fullpage options
            //navigation
            //fitToSection={false}
            //bigSectionsDestination={'top'}
            //scrollBar={true}
            //scrollOverflow={true}
            responsiveHeight={721}
            responsiveWidth={601}
            fadingEffect={'sections'}
            keyboardScrolling={false}
            menu="#sliderNav"
            //debug
            disable={false}
            anchors={[
              'main',
              'howWeWork',
              'technologies',
              'portfolio',
              'reviews',
              'contactUs',
            ]}
            onLeave={this.onLeave.bind(this)}
            sectionsColor={this.state.sectionsColor}
            licenseKey={'38214F7C-D54945DA-9422B4AD-09C55DD2'}
            scrollingSpeed={1000} /* Options here */
            render={({ state, fullpageApi }) => {
              return (
                <ReactFullpage.Wrapper>
                  {this.renderMainSlide()}
                  {this.renderInteractionModelsSlide()}
                  {this.renderOperateSlide()}
                  {this.renderPortfolioSlide()}
                  {this.renderReviewsSlide()}
                  {this.renderFormSlide()}
                </ReactFullpage.Wrapper>
              );
            }}
          />
        </div>
      </Page>
    );
  }
}
