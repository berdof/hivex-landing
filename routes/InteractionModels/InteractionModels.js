import React from 'react';
import Head from 'next/head';
import classnames from 'classnames';

import InteractionModelsData from './interactionModelsData';
import {InteractionModelTypes} from 'constants/mainConstants';
import Page from 'components/Page/Page';
import Tabs from 'components/Tabs/Tabs';
import Form from 'components/Form/Form';
import Button from 'components/Button/Button';
import Footer from 'components/Footer/Footer';
import {WorkflowIcon} from 'components/IdIcon/IdIcon';

import styles from './InteractionModels.module.scss';

class InteractionModels extends React.Component {
  static async getInitialProps({query}) {
    return {
      query: {
        modelType: 'dedicated-team',
      },
      ...query,
    };
  }

  scrollToContact = () =>
    this.contactUsRef.scrollIntoView({behavior: 'smooth'});

  render() {
    const modelData = InteractionModelsData[this.props.query.modelType];

    return (
      <Page>
        <Head>
          <title>Hivex - {modelData?.title}</title>
        </Head>
        <div className={styles.container}>
          <article className={styles.blockCol}>
            <div className={styles.blockText}>
              <h2 className={styles.blockTextTitle}>Interaction Models</h2>
              <p className={styles.blockTextDescription}>
                We will expand your team with missing experts or form a new one
                for the project goals. Just choose the appropriate interaction
                model.
              </p>
            </div>
          </article>
          <article className={styles.tabsContainer}>
            <Tabs
              className={
                this.props.query.modelType === 'dedicated-team'
                  ? styles.toLeft
                  : styles.toRight
              }
              tabs={InteractionModelTypes}
            />
          </article>
          <article
            className={classnames(styles.blockRow, styles.blockImageWrap)}
          >
            <div className={styles.blockText}>
              <h2 className={styles.blockTextTitle}>
                {modelData.description?.title}
              </h2>
              <p className={styles.blockTextDescription}>
                {modelData.description?.text}
              </p>
              <Button onClick={this.scrollToContact} className={styles.btn}>
                Start work with us
              </Button>
            </div>
            <div className={styles.backgroundImageWrapper}>
              <img alt="Model info" src={modelData.description?.image}/>
            </div>
          </article>
          <article className={styles.blockCol}>
            <div className={styles.blockText}>
              <h2 className={styles.blockTextTitle}>
                {modelData.advantages?.title}
              </h2>
            </div>
            <ul className={styles.advantagesContainer}>
              {modelData.advantages?.list.map((item) => (
                <li className={styles.advantagesItem} key={item.id}>
                  <span>{item.text}</span>
                </li>
              ))}
            </ul>
          </article>
          <article className={styles.blockCol}>
            <div className={styles.blockText}>
              <h2 className={styles.blockTextTitle}>
                {modelData.theWayItWorks?.title}
              </h2>
            </div>
            <ul className={styles.workflowContainer}>
              {modelData.theWayItWorks?.list.map((item) => (
                <li key={item.id} className={styles.workflowItem}>
                  <WorkflowIcon name={item.icon}/>
                  <span>{item.text}</span>
                </li>
              ))}
            </ul>
          </article>
          <article
            className={classnames(styles.blockRow, styles.suitableSection)}
          >
            <div className={styles.blockText}>
              <h2 className={styles.blockTextTitle}>
                {modelData.whenItSuitable.title}
              </h2>
              <ul className={styles.suitableList}>
                {modelData.whenItSuitable.list.map((item) => (
                  <li key={item.id} className={styles.suitableItem}>
                    <span>{item.text}</span>
                  </li>
                ))}
              </ul>
            </div>
            <div className={styles.modelImageWrapper}>
              <img
                className={styles.modelImage}
                alt="Model info"
                src={modelData.whenItSuitable.image}
              />
            </div>
          </article>
          <div
            className={styles.blockCol}
            ref={(ref) => (this.contactUsRef = ref)}
          >
            <Form/>
          </div>
          <Footer className={styles.footer}/>
        </div>
      </Page>
    );
  }
}

export default InteractionModels;
