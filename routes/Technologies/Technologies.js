import {Component} from 'react';
import Head from 'next/head';
import classnames from 'classnames';

import TechnologiesData from './tecnologiesData';
import Page from 'components/Page/Page';
import Form from 'components/Form/Form';
import Button from 'components/Button/Button';
import {ProgrammingLanguagesIcon, TechnologiesIcon,} from 'components/IdIcon/IdIcon';
import Footer from 'components/Footer/Footer';

import styles from './Technologies.module.scss';

const defaultTechnologyName = 'web-frontend'

export default class Technologies extends Component {
  static async getInitialProps({query}) {
    console.log('1', query);
    return {
      ...{
        query: {
          technologyName: defaultTechnologyName
        }
      },
      query
    };
  }

  constructor(props) {
    super(props);
    const {technologyName} = props.query;

    this.state = {
      currentLang:
      TechnologiesData.technologiesTypes[technologyName || defaultTechnologyName].defaultValue,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {technologyName} = prevProps.query;
    const {technologyName: newTechnologyName} = this.props.query;

    if (technologyName !== newTechnologyName)
      this.setState({
        currentLang:
        TechnologiesData.technologiesTypes[newTechnologyName].defaultValue,
      });
  }

  changeLang = (value) => {
    this.setState({
      currentLang: value,
    });
  };

  render() {
    const {
      query: {technologyName},
    } = this.props;

    const {currentLang} = this.state;
    const technologyData = TechnologiesData.technologiesTypes[technologyName || defaultTechnologyName];

    const programmingLanguageData =
      TechnologiesData.programmingLanguagesList[currentLang];

    const programmingLanguagesList = technologyData.list;

    return (
      <Page>
        <Head>
          <title>Hivex - {technologyData.title}</title>
        </Head>
        <div className={styles.container}>
          <article className={styles.article}>
            <div className={styles.titleBlock}>
              <h1 className={styles.titleBlockTitle}>{technologyData.title}</h1>
              <p className={styles.titleBlockDescription}>
                {technologyData.description}
              </p>
            </div>
            <div className={styles.titleImage}>
              <TechnologiesIcon name={technologyName}/>
            </div>
          </article>
          <div className={styles.picker}>
            <div className={styles.listContainer}>
              <ul className={styles.visualList}>
                {programmingLanguagesList.map((item) => {
                  const isSelected = currentLang === item.key;

                  return (
                    <li
                      key={item.key}
                      className={classnames(styles.visualListItem, {
                        [styles.visualListItemActive]: isSelected,
                      })}
                      onClick={() => this.changeLang(item.key)}
                    >
                      <ProgrammingLanguagesIcon
                        isSelected={isSelected}
                        name={item.key}
                      />
                    </li>
                  );
                })}
              </ul>
              <ul className={styles.list}>
                {programmingLanguagesList.map((item) => (
                  <li
                    key={item.key}
                    className={classnames(styles.listItem, {
                      [styles.listItemActive]: currentLang === item.key,
                    })}
                  >
                    <span
                      onClick={() => this.changeLang(item.key)}
                      className={styles.listItemValue}
                    >
                      {item.title}
                    </span>
                    <span className={styles.listItemLine}/>
                  </li>
                ))}
              </ul>
            </div>
            <article className={styles.languageInfo}>
              <h2 className={styles.languageInfoTitle}>
                {programmingLanguageData.title}
              </h2>
              <p className={styles.languageInfoDescription}>
                {programmingLanguageData.description}
              </p>
              <Button className={styles.startWorkingWithUs}>
                Start working with us
              </Button>
            </article>
          </div>
          <Form formClassName={styles.form}/>
          <Footer/>
        </div>
      </Page>
    );
  }
}
