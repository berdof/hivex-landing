import React, { Component } from 'react';
import classnames from 'classnames';
import Head from 'next/head';

import casesData from './casesData';
import Page from 'components/Page/Page';
import ReadMoreButton from 'components/ReadMoreButton/ReadMoreButton';
import Footer from 'components/Footer/Footer';

import styles from './Cases.module.scss';

function LinkToResource({ title, link, className = '' }) {
  return (
    <a className={classnames(styles.linkToResource, className)} href={link}>
      {title}
    </a>
  );
}

class Cases extends Component {
  static async getInitialProps({ query }) {
    return {
      query,
    };
  }

  get currentCaseData() {
    const {
      query: { caseName },
    } = this.props;

    return casesData.find((item) => item.key === caseName);
  }

  get filteredCases() {
    const {
      query: { caseName },
    } = this.props;

    return casesData.filter((item) => item.key !== caseName);
  }

  get pageTitle() {
    const {
      query: { caseName },
    } = this.props;

    return caseName || 'Portfolio';
  }

  renderList = () => {
    return (
      <div className={styles.caseListContainer}>
        <span className={styles.caseListTitle}>
          Check out the following projects
        </span>
        <ul className={styles.caseList}>
          {this.filteredCases.map((item) => (
            <li className={styles.caseListItem}>
              <div className={styles.caseListItemImageWrapper}>
                <img
                  className={styles.caseListItemImage}
                  src={item.titleImage}
                  alt="Case image"
                />
              </div>
              <div className={styles.caseListItemContainer}>
                <span className={styles.caseListItemTitle}>{item.title}</span>
                <p className={styles.caseListItemDescription}>
                  {item.projectDescription}
                </p>
                <ReadMoreButton link={`/cases/${item.key}`} />
              </div>
            </li>
          ))}
        </ul>
      </div>
    );
  };

  // TODO Remove temporary numbers
  renderCasePage = () => {
    const {
      query: { caseName },
    } = this.props;

    const { currentCaseData } = this;

    return (
      <>
        <div className={styles.head}>
          <div className={classnames(styles.headInfo, styles.hideOnMobile)}>
            <span className={styles.year}>{currentCaseData.year}</span>
            <div className={styles.infoItemContainer}>
              <span className={styles.infoItemTitle}>Model:</span>
              <span className={styles.infoItemValue}>
                {currentCaseData.model}
              </span>
            </div>
            <div className={styles.infoItemContainer}>
              <span className={styles.infoItemTitle}>Term:</span>
              <span className={styles.infoItemValue}>
                {currentCaseData.term}
              </span>
            </div>
            <div className={styles.infoItemContainer}>
              <span className={styles.infoItemTitle}>Technologies:</span>
              <ul className={styles.infoItemList}>
                {currentCaseData.technologies.map((item) => (
                  <li key={item.id}>
                    <span className={styles.infoItemValue}>{item.title}</span>
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div
            className={classnames(styles.mobileHeadInfo, styles.hideOnDesktop)}
          >
            <div className={styles.mobileHeadInfoTitle}>
              <h1 className={styles.caseTitle}>{currentCaseData.title}</h1>
              <LinkToResource
                title={currentCaseData.linkTitle}
                link={currentCaseData.linkToResource}
              />
            </div>
            <div className={styles.mobileHeadInfoData}>
              <div className={styles.mobileHeadInfoDataBlock}>
                <div className={styles.infoItemContainer}>
                  <span className={styles.infoItemTitle}>Model:</span>
                  <span className={styles.infoItemValue}>
                    {currentCaseData.model}
                  </span>
                </div>
                <div className={styles.infoItemContainer}>
                  <span className={styles.infoItemTitle}>Term:</span>
                  <span className={styles.infoItemValue}>
                    {currentCaseData.term}
                  </span>
                </div>
                <div className={styles.infoItemContainer}>
                  <span className={styles.infoItemTitle}>Technologies:</span>
                  <ul className={styles.infoItemList}>
                    {currentCaseData.technologies.map((item) => (
                      <li key={item.id}>
                        <span className={styles.infoItemValue}>
                          {item.title}
                        </span>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className={styles.mobileHeadInfoDataBlock}>
                <span className={styles.year}>{currentCaseData.year}</span>
              </div>
            </div>
          </div>
          <article className={styles.headDescription}>
            <h1 className={classnames(styles.caseTitle, styles.hideOnMobile)}>
              {currentCaseData.title}
            </h1>
            <span className={styles.projectDescription}>
              {currentCaseData.projectDescription}
            </span>
            <LinkToResource
              title={currentCaseData.linkTitle}
              link={currentCaseData.linkToResource}
              className={styles.hideOnMobile}
            />
            <p className={styles.caseDescription}>
              Case description: {currentCaseData.caseDescription}
            </p>
          </article>
        </div>
        <div className={styles.caseImagesList}>
          {currentCaseData.images.map((item) => (
            <img
              className={styles.caseImage}
              key={item.id}
              src={item.src}
              alt="Case image"
            />
          ))}
        </div>
        <div className={styles.challengeContainer}>
          <span className={styles.challengeTitle}>Challenge</span>
          <p className={styles.challengeDescription}>
            {currentCaseData.challenge}
          </p>
        </div>
        <ul className={styles.numbersContainer}>
          {currentCaseData.temporaryNumbers.map((item) => (
            <li className={styles.numbersItem} key={item.id}>
              <span className={styles.numbersItemTitle}>{item.count}</span>
              <span className={styles.numbersItemDescription}>
                {item.title}
              </span>
            </li>
          ))}
        </ul>
      </>
    );
  };

  render() {
    const {
      query: { caseName },
    } = this.props;

    return (
      <Page>
        <Head>
          <title>Hivex - {this.pageTitle}</title>
        </Head>
        <div className={styles.container}>
          {caseName && this.renderCasePage()}
          {false && this.renderList()}
          <Footer className={styles.footer} />
        </div>
      </Page>
    );
  }
}

export default Cases;
