/*class EventEmitter {
  funcs:{},
  on(eventName, func){
    if(!this[eventName]){
      this[eventName] = [];
    }
    this[eventName].push(func);
  }
  off(eventName, func){
    this.funcs[eventName] = this.funcs[eventName].filter((item)=> func !== item);
  }
  emit(eventName, ...args){
    this.funcs[eventName].forEach((func)=>func(...args));
  }
}

const func1 = (data) => {
  console.log(`func1 data:`, data);
}

const func2 = (data) => {
  console.log(`func2 data:`, data);
}

const ee = new EventEmitter();

ee.on(`event`, func1);
ee.on(`event`, func2);
ee.emit(`event`, `hello`);// вывод в консоль func1 data: hello и func2 data: hello

ee.emit(`event`, `world`);// вывод в консоль объекта для func1 и func2
ee.off(`event`, func1);
ee.emit(`event`, `hello`);// выводится только func2 data: hello


*/

///-------


class Queue {
  queue = [];
  isQueueInProcess = false;
  add(fn, cb = () => {}) {
    this.queue.push({
      cb, fn
    });
    if(!this.isQueueInProcess){
      this.loop();
    }
  }

  loop() {
    const {fn, cb} = this.queue.shift() || {};
    if (fn) {
      this.isQueueInProcess = true;
      fn((msg) => {
        cb(msg);
        if (this.queue.length) {
          this.loop();
        }else{
          this.isQueueInProcess = false;
        }
      })
    }
  }
}

// -----------------------------------------------

const q = new Queue();

const first = (cb) => {
  setTimeout(() => cb('async first in action'), 1000)
}
const second = (cb) => {
  setTimeout(() => cb('async second in action'), 1000)
}
const third = () => {
  console.info('sync third in action')
}

const last = (cb) => {
  setTimeout(() => cb('async last in action'), 1000)
}

// -----------------------------------------------

q.add(first, data => {
  console.info(data);
});

q.add(second, data => {
  console.info(data);
});

q.add(third, data => {
  console.info(data);
});


setTimeout(() => {
  q.add(last, data => {
    console.info(data);
  });
}, 600)

setTimeout(() => {
  q.add(third);
}, 5000)

setTimeout(() => {
  q.add(first, data => {
    console.info(data);
  });
}, 6000)




///
PO для атм

















