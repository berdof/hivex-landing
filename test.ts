const person = {
  name: 'Chuck Norris',
  age: 68,
};

// TODO: затипизировать
function getObjValue<T extends {}, P extends keyof T>(obj: T, propName: P): T[P]{
  return obj[propName];
}

getObjValue(person, 'name');

///////


const students: Array<{ name: string, age: number }> = [
  { name: 'Kyle', age: 17 },
  { name: 'John', age: 17 },
  { name: 'Alex', age: 18 },
  { name: 'Alex', age: 24 },
];

const teachers: Array<{ name: string, spec: string }> = [
  { name: 'Anna', spec: 'math' },
  { name: 'Olga', spec: 'lang' },
  { name: 'Anna', spec: 'chem' },
];

// TODO: реализовать и затипизировать
function find<T, P extends keyof T>(collection: T[], propName: P, value: T[P]): T[]{
  return collection.filter((item)=>{
    return item[propName] === value;
  })
}

const resultStd = find(students, 'age', 17);
const resultTch = find(teachers, 'spec', 'math');

console.info('resultStd', resultStd);
console.info('resultTch', resultTch);


//////////-----

