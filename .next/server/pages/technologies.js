module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Y0jz");


/***/ }),

/***/ "73Hj":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Button_wrapHeader__1JYsI",
	"wrap": "Button_wrap__1NxA6",
	"wrapInLeft": "Button_wrapInLeft__2YmlP",
	"wrapInCenter": "Button_wrapInCenter__2cX9s",
	"hidden": "Button_hidden__19hY1",
	"btn": "Button_btn__3A27M",
	"btnMain": "Button_btnMain__1v6TM",
	"btnSecondary": "Button_btnSecondary__19dlM"
};


/***/ }),

/***/ "8cHP":
/***/ (function(module, exports, __webpack_require__) {

const routes = __webpack_require__("90Kz"); // Name   Page      Pattern


module.exports = routes() // ----   ----      -----
.add('index').add('work-with-us').add('technologies', '/technologies/:technologyName').add('interaction-models', '/interaction-models/:modelType').add('cases', '/cases/:caseName?');

/***/ }),

/***/ "8wsC":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Button; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Button_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("73Hj");
/* harmony import */ var _Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Button_module_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



function Button(props) {
  const {
    className = '',
    children,
    secondary = false,
    onClick = () => {},
    disabled = false
  } = props;
  return __jsx("button", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.btn, className, {
      [_Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.btnSecondary]: secondary,
      [_Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.btnMain]: !secondary
    }),
    disabled: disabled,
    onClick: onClick
  }, children);
}

/***/ }),

/***/ "90Kz":
/***/ (function(module, exports) {

module.exports = require("next-routes");

/***/ }),

/***/ "CSOn":
/***/ (function(module, exports) {

module.exports = require("react-transition-group");

/***/ }),

/***/ "E1mO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./public/assets/facebook.svg
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



var _ref = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16 .273c-8.836 0-16 7.164-16 16s7.164 16 16 16 16-7.164 16-16-7.164-16-16-16zm0 1c8.271 0 15 6.73 15 15 0 8.271-6.729 15-15 15s-15-6.729-15-15c0-8.27 6.729-15 15-15zm-5.333 12.334h2.666V11.35c0-2.022 1.064-3.078 3.462-3.078H20v3.334h-1.923c-.616 0-.744.252-.744.89v1.11H20l-.24 2.666h-2.427v8h-4v-8h-2.666v-2.666z",
  fill: "#2B2B2B"
});

function SvgFacebook(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", _extends({
    width: 32,
    height: 33,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _ref);
}

/* harmony default export */ var facebook = (SvgFacebook);
// CONCATENATED MODULE: ./public/assets/twitter.svg
function twitter_extends() { twitter_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return twitter_extends.apply(this, arguments); }



var twitter_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16 .273c-8.836 0-16 7.164-16 16s7.164 16 16 16 16-7.164 16-16-7.164-16-16-16zm0 1c8.271 0 15 6.73 15 15 0 8.271-6.729 15-15 15s-15-6.729-15-15c0-8.27 6.729-15 15-15zm6.781 11.222a6.569 6.569 0 001.886-.518 6.616 6.616 0 01-1.639 1.7c.208 4.616-3.235 9.764-9.33 9.764a9.284 9.284 0 01-5.031-1.474 6.606 6.606 0 004.862-1.362 3.287 3.287 0 01-3.066-2.278 3.29 3.29 0 001.482-.056c-1.578-.318-2.669-1.74-2.633-3.26.443.245.948.393 1.485.41a3.287 3.287 0 01-1.016-4.382 9.323 9.323 0 006.766 3.43 3.286 3.286 0 015.593-2.994 6.568 6.568 0 002.085-.796 3.297 3.297 0 01-1.444 1.816z",
  fill: "#2B2B2B"
});

function SvgTwitter(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", twitter_extends({
    width: 32,
    height: 33,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), twitter_ref);
}

/* harmony default export */ var twitter = (SvgTwitter);
// CONCATENATED MODULE: ./public/assets/instagram.svg
function instagram_extends() { instagram_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return instagram_extends.apply(this, arguments); }



var instagram_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16 .273c-8.836 0-16 7.164-16 16s7.164 16 16 16 16-7.164 16-16-7.164-16-16-16zm0 1c8.271 0 15 6.73 15 15 0 8.271-6.729 15-15 15s-15-6.729-15-15c0-8.27 6.729-15 15-15zm-4.55 7A3.45 3.45 0 008 11.723v9.101a3.45 3.45 0 003.45 3.45h9.1a3.45 3.45 0 003.45-3.45v-9.102a3.45 3.45 0 00-3.45-3.449h-9.1zM16 19.723a3.45 3.45 0 100-6.899 3.45 3.45 0 000 6.899zm5.665-8.126a1 1 0 11-2 0 1 1 0 012 0z",
  fill: "#2B2B2B"
});

function SvgInstagram(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", instagram_extends({
    width: 32,
    height: 33,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), instagram_ref);
}

/* harmony default export */ var instagram = (SvgInstagram);
// EXTERNAL MODULE: ./components/SocialLinks/SocialLinks.module.scss
var SocialLinks_module = __webpack_require__("kvya");
var SocialLinks_module_default = /*#__PURE__*/__webpack_require__.n(SocialLinks_module);

// CONCATENATED MODULE: ./components/SocialLinks/SocialLinks.js
var __jsx = external_react_default.a.createElement;






function SocialLinks(props) {
  return __jsx("ul", {
    className: SocialLinks_module_default.a.container
  }, __jsx("li", {
    className: SocialLinks_module_default.a.item
  }, __jsx("a", {
    className: SocialLinks_module_default.a.link,
    href: "/"
  }, __jsx(facebook, null))), __jsx("li", {
    className: SocialLinks_module_default.a.item
  }, __jsx("a", {
    className: SocialLinks_module_default.a.link,
    href: "/"
  }, __jsx(twitter, null))), __jsx("li", {
    className: SocialLinks_module_default.a.item
  }, __jsx("a", {
    className: SocialLinks_module_default.a.link,
    href: "/"
  }, __jsx(instagram, null))));
}

/* harmony default export */ var SocialLinks_SocialLinks = __webpack_exports__["a"] = (SocialLinks);

/***/ }),

/***/ "HXcA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Footer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_SocialLinks_SocialLinks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("E1mO");
/* harmony import */ var _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("kzqI");
/* harmony import */ var _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Footer_module_scss__WEBPACK_IMPORTED_MODULE_3__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



function Footer({
  className = ''
}) {
  return __jsx("footer", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.container, className)
  }, __jsx("div", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.info
  }, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoHead
  }, "Estonia, Tallin, Kesklinna linnaosa, Tuukri tn 19-315"),  false && false,  false && false, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoCopyRight
  }, "Copyright \xA9 ", new Date().getFullYear(), ". Hivex")), __jsx("nav", null, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.columnHead
  }, "Company"), __jsx("ul", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navIn
  }, __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "howWeWork"
  }, __jsx("a", {
    href: "#howWeWork"
  }, "How we are working")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "technologies"
  }, __jsx("a", {
    href: "#technologies"
  }, "Technologies")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "portfolio"
  }, __jsx("a", {
    href: "#portfolio"
  }, "Portfolio")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "reviews"
  }, __jsx("a", {
    href: "#reviews"
  }, "Reviews")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "contactUs"
  }, __jsx("a", {
    href: "#contactUs"
  }, "Contact us")))), __jsx("div", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.cooperation
  }, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.columnHead
  }, "Cooperation"), __jsx("a", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.cooperationEmailUs,
    href: "mailto:hello@hivex.group"
  }, "hello@hivex.group")));
}

/***/ }),

/***/ "JSqF":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Technologies_wrapHeader__fq2j7",
	"wrap": "Technologies_wrap__SXPTk",
	"wrapInLeft": "Technologies_wrapInLeft__R0SuO",
	"wrapInCenter": "Technologies_wrapInCenter__27_qG",
	"hidden": "Technologies_hidden__1UFox",
	"container": "Technologies_container__AaLyl",
	"article": "Technologies_article__2jxMy",
	"titleBlock": "Technologies_titleBlock__1f_li",
	"titleBlockTitle": "Technologies_titleBlockTitle__22gBR",
	"titleBlockDescription": "Technologies_titleBlockDescription__2hhG8",
	"titleImage": "Technologies_titleImage__VWG5y",
	"picker": "Technologies_picker__261f8",
	"listContainer": "Technologies_listContainer__1VaHv",
	"visualList": "Technologies_visualList__2hIOF",
	"visualListItem": "Technologies_visualListItem__2Z4lC",
	"visualListItemActive": "Technologies_visualListItemActive__3-VqQ",
	"list": "Technologies_list__e-9Uh",
	"listItem": "Technologies_listItem__mgvW8",
	"listItemActive": "Technologies_listItemActive__26HrJ",
	"listItemValue": "Technologies_listItemValue__2RDTA",
	"listItemLine": "Technologies_listItemLine__MSAQt",
	"languageInfo": "Technologies_languageInfo__1l3rE",
	"languageInfoTitle": "Technologies_languageInfoTitle__2bxy3",
	"languageInfoDescription": "Technologies_languageInfoDescription__1nkbo",
	"startWorkingWithUs": "Technologies_startWorkingWithUs__IVj5M",
	"form": "Technologies_form__2Y6bt"
};


/***/ }),

/***/ "K2gz":
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),

/***/ "Lueu":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "CookiesConfirmation_wrapHeader__3KL9K",
	"wrap": "CookiesConfirmation_wrap__1yCFG",
	"wrapInLeft": "CookiesConfirmation_wrapInLeft__XALEJ",
	"wrapInCenter": "CookiesConfirmation_wrapInCenter__2YbBH",
	"hidden": "CookiesConfirmation_hidden__3yaH_",
	"container": "CookiesConfirmation_container__1BxNj",
	"info": "CookiesConfirmation_info__3cOhH",
	"title": "CookiesConfirmation_title__3-EA8",
	"description": "CookiesConfirmation_description__2FsRZ",
	"agreeBtn": "CookiesConfirmation_agreeBtn__1NTZ6"
};


/***/ }),

/***/ "PjcW":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "c", function() { return /* binding */ WorkflowIcon; });
__webpack_require__.d(__webpack_exports__, "b", function() { return /* binding */ TechnologiesIcon; });
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ ProgrammingLanguagesIcon; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__("K2gz");
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// CONCATENATED MODULE: ./public/assets/interaction-models/team.svg
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



var team_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#team_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42 15.31a8 8 0 018 0l17.98 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928L50 70.691a8 8 0 01-8 0L24.02 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928L42 15.309z",
  fill: "url(#team_svg__paint0_linear)"
}));

var _ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M39.952 43.976c.144-.108.288-.18.432-.252-1.476-.612-3.78-.684-4.5-.684-.864 0-3.78.072-5.112.972-1.692 1.188-1.872 4.824-1.872 5.58 0 .216.072.432.252.612a.85.85 0 00.576.252h7.632c.108-1.764.54-5.076 2.592-6.48zM35.884 41.744a4.248 4.248 0 100-8.496 4.248 4.248 0 000 8.496zM63.1 49.484c-.036-.936-.252-4.32-1.908-5.436-1.332-.9-4.248-.972-5.112-.972-.756 0-3.06.072-4.5.684.144.072.288.18.432.252 2.016 1.368 2.52 4.5 2.592 6.444h7.632c.468 0 .828-.36.828-.828a.272.272 0 00.036-.144zM56.08 41.744a4.248 4.248 0 100-8.496 4.248 4.248 0 000 8.496zM51.112 45.344c-1.332-.9-4.248-.972-5.112-.972-.864 0-3.78.072-5.112.972-1.692 1.152-1.872 4.824-1.908 5.544 0 .216.072.432.252.612a.85.85 0 00.576.252h12.348c.468 0 .828-.36.828-.828v-.144c-.036-.9-.252-4.32-1.872-5.436zM46 43.076a4.248 4.248 0 100-8.496 4.248 4.248 0 000 8.496z",
  fill: "#9B2FFF"
});

var _ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "team_svg__paint0_linear",
  x1: 46,
  y1: 73,
  x2: 46,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "team_svg__filter0_d",
  x: 0.019,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgTeam(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", _extends({
    width: 92,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), team_ref, _ref2, _ref3);
}

/* harmony default export */ var team = (SvgTeam);
// CONCATENATED MODULE: ./public/assets/interaction-models/validated.svg
function validated_extends() { validated_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return validated_extends.apply(this, arguments); }



var validated_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#validated_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42 15.31a8 8 0 018 0l17.98 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928L50 70.691a8 8 0 01-8 0L24.02 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928L42 15.309z",
  fill: "url(#validated_svg__paint0_linear)"
}));

var validated_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M46.014 31.5C38.836 31.5 33 35.751 33 40.981c0 2.993 1.89 5.65 4.85 7.384-.275.867-.85 1.986-2 3.329-.713.839.136 2.125 1.15 1.734 2.137-.811 4.603-1.958 6.877-3.077.685.084 1.397.14 2.11.14C53.163 50.49 59 46.24 59 41.009c.027-5.258-5.808-9.509-12.986-9.509zm5.506 7.44l-6.11 6.712a.603.603 0 01-.876 0l-4.027-4.419a.626.626 0 01.027-.867l1.26-1.203a.595.595 0 01.85.029L45 41.764l4.41-4.867c.22-.252.604-.252.85-.028l1.26 1.203c.22.251.247.615 0 .867z",
  fill: "#9B2FFF"
});

var validated_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "validated_svg__paint0_linear",
  x1: 46,
  y1: 73,
  x2: 46,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "validated_svg__filter0_d",
  x: 0.019,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgValidated(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", validated_extends({
    width: 92,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), validated_ref, validated_ref2, validated_ref3);
}

/* harmony default export */ var validated = (SvgValidated);
// CONCATENATED MODULE: ./public/assets/interaction-models/complete.svg
function complete_extends() { complete_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return complete_extends.apply(this, arguments); }



var complete_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#complete_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42.344 15.31a8 8 0 018 0l17.981 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928l-17.98 10.382a8 8 0 01-8 0L24.362 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928l17.981-10.382z",
  fill: "url(#complete_svg__paint0_linear)"
}));

var complete_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M38.135 40.215h-4.493c-.42 0-.749.33-.749.749v11.98c0 .42.33.749.749.749h4.492c.42 0 .749-.33.749-.75v-11.98a.74.74 0 00-.748-.748zM59.797 41.634c0-1.22-.838-2.455-2.438-2.455h-7.254c1.037-1.854 1.341-4.461.621-6.37-.53-1.402-1.54-2.221-2.85-2.304l-.02-.002a1.57 1.57 0 00-1.66 1.436c-.186 1.892-1.015 5.239-2.204 6.427-1 1-1.856 1.42-3.276 2.114-.205.1-.43.21-.667.328.004.051.007.103.007.156v11.858l.506.173c2.343.808 4.368 1.505 7.463 1.505h5.864c1.6 0 2.438-1.235 2.438-2.455 0-.362-.073-.725-.219-1.057.533-.096 1-.353 1.346-.745a2.516 2.516 0 00.608-1.669c0-.36-.073-.723-.218-1.055 1.285-.222 1.953-1.324 1.953-2.415 0-.632-.225-1.269-.664-1.735.438-.466.664-1.103.664-1.735z",
  fill: "#9B2FFF"
});

var complete_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "complete_svg__paint0_linear",
  x1: 46.344,
  y1: 73,
  x2: 46.344,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "complete_svg__filter0_d",
  x: 0.364,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgComplete(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", complete_extends({
    width: 93,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), complete_ref, complete_ref2, complete_ref3);
}

/* harmony default export */ var complete = (SvgComplete);
// CONCATENATED MODULE: ./public/assets/interaction-models/tools.svg
function tools_extends() { tools_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return tools_extends.apply(this, arguments); }



var tools_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#tools_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42 15.31a8 8 0 018 0l17.98 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928L50 70.691a8 8 0 01-8 0L24.02 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928L42 15.309z",
  fill: "url(#tools_svg__paint0_linear)"
}));

var tools_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M53.616 38.586c.365-.721.308-1.14-.183-1.48-.49-.34-.78-.225-1.614.57l-2.683-2.235c.128-.206.237-.411.374-.594a.467.467 0 00.022-.55 4.378 4.378 0 00-4.343-2.566h-.43c.12-.157.249-.307.385-.45.77-.757 1.745-1.124 2.803-1.327a4.224 4.224 0 013.285.767c.239.17.492.32.756.448.175.094.371.142.57.14.46-.032.915.121 1.263.424.358.263.698.549 1.018.856.175.203.317.432.422.679.068.16.118.327.149.499.236.935.855 1.183 1.682.658.71.614 1.454 1.232 2.147 1.845a.725.725 0 01.086 1.055c-.155.2-.321.39-.496.57-.659.722-1.323 1.427-1.982 2.162-.442.482-.744.517-1.255.112-.659-.52-1.309-1.05-1.976-1.583zM51.286 38.509l-1.266 1.6-11.524 14.543a1.455 1.455 0 01-2.21.234c-.539-.442-1.08-.882-1.61-1.332a1.395 1.395 0 01-.152-2.105L48.31 36.182c0-.015.034-.023.091-.066l2.886 2.393zM60.2 49.417a5.618 5.618 0 00-.704-1.75 5.048 5.048 0 00-6.468-1.78.24.24 0 01-.202 0c-.924-.59-1.862-1.16-2.76-1.79a40.9 40.9 0 01-.856-.628l-2.282 2.74c.438.38.863.776 1.275 1.186.687.673 1.323 1.4 1.973 2.11a.4.4 0 01.089.286 4.903 4.903 0 00.154 1.996 4.962 4.962 0 002.795 3.262 4.848 4.848 0 003.87.094c.045-.023.089-.05.13-.08a1.158 1.158 0 00-.088-.096l-2.852-2.362a.348.348 0 01-.137-.21 80.362 80.362 0 01-.077-1.863.31.31 0 01.069-.185c.285-.342.553-.684.835-1.021a.33.33 0 01.177-.091 74.634 74.634 0 011.78-.266.43.43 0 01.285.083 278.71 278.71 0 012.164 1.803c.263.219.516.447.804.698.02-.117.032-.171.037-.228.037-.302.072-.605.106-.907l-.077-.855a1.062 1.062 0 01-.04-.146zM41.776 36.661c-.217-.253-.363-.476-.169-.798.012-.03.02-.06.026-.091a5.053 5.053 0 00-2.216-5.473 4.914 4.914 0 00-4.237-.53 2.803 2.803 0 00-.285.117l.37.325c.856.75 1.694 1.497 2.535 2.25a.31.31 0 01.103.191c.014.613.02 1.23.023 1.842a.336.336 0 01-.077.203c-.285.319-.55.636-.833.946a.438.438 0 01-.234.132c-.57.08-1.14.14-1.71.216a.383.383 0 01-.329-.108 648.56 648.56 0 01-2.344-2.076c-.174-.146-.353-.305-.561-.488l-.172 1.115.055.881c.014.043.034.083.045.126.107.42.237.834.388 1.24a4.792 4.792 0 002.866 2.655 4.768 4.768 0 003.28.029.348.348 0 01.262 0c.359.228.724.453 1.063.713.608.47 1.22.935 1.82 1.425l2.372-2.686a30.456 30.456 0 01-2.041-2.156z",
  fill: "#9B2FFF"
});

var tools_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "tools_svg__paint0_linear",
  x1: 46,
  y1: 73,
  x2: 46,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "tools_svg__filter0_d",
  x: 0.019,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgTools(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", tools_extends({
    width: 92,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), tools_ref, tools_ref2, tools_ref3);
}

/* harmony default export */ var tools = (SvgTools);
// CONCATENATED MODULE: ./public/assets/interaction-models/changes.svg
function changes_extends() { changes_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return changes_extends.apply(this, arguments); }



var changes_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#changes_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42.183 15.31a8 8 0 018 0l17.981 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928l-17.98 10.382a8 8 0 01-8 0L24.202 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928l17.98-10.382z",
  fill: "url(#changes_svg__paint0_linear)"
}));

var changes_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M38.175 38.624a3.04 3.04 0 013.057-3.056h6.554v3.056c0 .63.347 1.198.882 1.481.252.126.505.19.757.19.346 0 .661-.095.945-.316l6.586-4.726c.44-.315.693-.82.693-1.355 0-.536-.252-1.04-.693-1.355l-6.586-4.727c-.504-.346-1.166-.41-1.702-.126-.535.284-.882.851-.882 1.481v3.057h-6.523a6.368 6.368 0 00-6.365 6.365v7.688l3.309-2.363v-5.294h-.032zM54.183 46.376a3.04 3.04 0 01-3.056 3.056H44.54v-3.056c0-.63-.347-1.197-.882-1.481-.536-.284-1.198-.22-1.702.126l-6.617 4.727c-.441.315-.693.819-.693 1.355 0 .535.252 1.04.693 1.355l6.586 4.726c.283.19.63.315.945.315.252 0 .504-.063.756-.189.536-.284.883-.85.883-1.48v-3.057h6.585a6.368 6.368 0 006.365-6.366v-7.72l-3.308 2.364v5.325h.031z",
  fill: "#9B2FFF"
});

var changes_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "changes_svg__paint0_linear",
  x1: 46.183,
  y1: 73,
  x2: 46.183,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "changes_svg__filter0_d",
  x: 0.203,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgChanges(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", changes_extends({
    width: 93,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), changes_ref, changes_ref2, changes_ref3);
}

/* harmony default export */ var changes = (SvgChanges);
// CONCATENATED MODULE: ./public/assets/interaction-models/tasks.svg
function tasks_extends() { tasks_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return tasks_extends.apply(this, arguments); }



var tasks_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#tasks_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42 15.31a8 8 0 018 0l17.98 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928L50 70.691a8 8 0 01-8 0L24.02 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928L42 15.309z",
  fill: "url(#tasks_svg__paint0_linear)"
}));

var tasks_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42.257 34.128h17.465c1.05 0 1.904-.854 1.904-1.904 0-1.05-.886-1.871-1.937-1.871H42.257c-1.05 0-1.904.853-1.904 1.904 0 1.05.854 1.87 1.904 1.87zM59.69 40.595H42.256c-1.05 0-1.904.853-1.904 1.904 0 1.05.854 1.904 1.904 1.904h17.465c1.05 0 1.904-.854 1.904-1.904 0-1.05-.886-1.904-1.937-1.904zM59.69 50.871H42.256c-1.05 0-1.904.854-1.904 1.904 0 1.05.854 1.904 1.904 1.904h17.465c1.05 0 1.904-.853 1.904-1.904-.032-1.05-.886-1.904-1.937-1.904zM33.23 34.981h2.856c.722 0 1.313-.59 1.313-1.313v-2.856c0-.722-.59-1.313-1.313-1.313H33.23c-.722 0-1.313.59-1.313 1.313v2.856c0 .723.59 1.313 1.313 1.313zM36.086 50.018H33.23c-.722 0-1.313.59-1.313 1.313v2.856c0 .722.59 1.313 1.313 1.313h2.856c.722 0 1.313-.59 1.313-1.313V51.33c0-.723-.59-1.313-1.313-1.313zM38.417 38.724a1.625 1.625 0 00-2.331 0l-1.445 1.444-1.444-1.444a1.625 1.625 0 00-2.331 0c-.657.624-.657 1.674 0 2.33l1.477 1.445-1.444 1.445a1.625 1.625 0 000 2.33c.328.329.755.493 1.149.493s.853-.164 1.149-.492l1.444-1.445 1.445 1.445c.328.328.755.492 1.149.492s.853-.164 1.149-.492c.656-.624.656-1.675 0-2.331l-1.412-1.445 1.445-1.444c.623-.624.623-1.675 0-2.331z",
  fill: "#9B2FFF"
});

var tasks_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "tasks_svg__paint0_linear",
  x1: 46,
  y1: 73,
  x2: 46,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "tasks_svg__filter0_d",
  x: 0.019,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgTasks(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", tasks_extends({
    width: 92,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), tasks_ref, tasks_ref2, tasks_ref3);
}

/* harmony default export */ var tasks = (SvgTasks);
// CONCATENATED MODULE: ./public/assets/interaction-models/targets.svg
function targets_extends() { targets_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return targets_extends.apply(this, arguments); }



var targets_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#targets_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42.344 15.31a8 8 0 018 0l17.981 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928l-17.98 10.382a8 8 0 01-8 0L24.363 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928l17.98-10.382z",
  fill: "url(#targets_svg__paint0_linear)"
}));

var targets_ref2 = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#targets_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M37.344 42.5c0 2.102.721 4.036 1.93 5.568a6.882 6.882 0 110-11.136 8.961 8.961 0 00-1.93 5.567zm20.118 6.882a6.882 6.882 0 110-13.765 6.882 6.882 0 010 13.765zm-7.071-12.45a8.962 8.962 0 00-1.929 5.567c0 2.103.72 4.037 1.929 5.569a6.882 6.882 0 110-11.136z",
  fill: "#9B2FFF"
}));

var targets_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "targets_svg__paint0_linear",
  x1: 46.344,
  y1: 73,
  x2: 46.344,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "targets_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "translate(28.345 24.5)",
  d: "M0 0h36v36H0z"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "targets_svg__filter0_d",
  x: 0.364,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgTargets(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", targets_extends({
    width: 93,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), targets_ref, targets_ref2, targets_ref3);
}

/* harmony default export */ var targets = (SvgTargets);
// CONCATENATED MODULE: ./public/assets/interaction-models/changes-gear.svg
function changes_gear_extends() { changes_gear_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return changes_gear_extends.apply(this, arguments); }



var changes_gear_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#changes-gear_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M42.344 15.31a8 8 0 018 0l17.981 10.38a8 8 0 014 6.929V53.38a8 8 0 01-4 6.928l-17.98 10.382a8 8 0 01-8 0L24.362 60.309a8 8 0 01-4-6.928V32.62a8 8 0 014-6.928l17.981-10.382z",
  fill: "url(#changes-gear_svg__paint0_linear)"
}));

var changes_gear_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M32.147 50.601l1.258.22c.146.548.363 1.074.645 1.565l-.728 1.042a.964.964 0 00.107 1.227l.757.754a.97.97 0 001.23.107l1.048-.733c.497.29 1.029.512 1.584.663l.221 1.254a.965.965 0 00.953.8h1.07a.97.97 0 00.954-.8l.221-1.254a6.541 6.541 0 001.569-.65l1.048.732a.97.97 0 001.23-.107l.757-.754a.965.965 0 00.107-1.227l-.735-1.045c.285-.49.504-1.017.652-1.564l1.258-.22a.97.97 0 00.802-.951v-1.077a.964.964 0 00-.802-.95l-1.255-.227a6.484 6.484 0 00-.652-1.564l.735-1.045a.964.964 0 00-.107-1.227l-.754-.763a.97.97 0 00-1.23-.108l-1.06.74a6.548 6.548 0 00-1.569-.65l-.221-1.255a.965.965 0 00-.954-.8H39.22a.97.97 0 00-.953.8l-.218 1.254a6.545 6.545 0 00-1.568.65l-1.049-.733a.971.971 0 00-1.23.108l-.766.751a.964.964 0 00-.107 1.227l.722 1.057a6.488 6.488 0 00-.642 1.558l-1.261.22a.97.97 0 00-.803.951v1.064a.964.964 0 00.803.96zm7.604-4.566a3.081 3.081 0 012.841 1.893 3.058 3.058 0 01-.666 3.342 3.077 3.077 0 01-3.351.664 3.073 3.073 0 01-1.899-2.832c0-.814.324-1.593.901-2.169a3.08 3.08 0 012.174-.898zm15.256 7.139v-4.048H53.73a.381.381 0 01-.308-.592l2.503-3.707a.381.381 0 01.307-.168.381.381 0 01.308.168l2.51 3.707a.38.38 0 01-.308.592h-1.289v5.268a1.225 1.225 0 01-1.23 1.226h-7.389a.55.55 0 01-.55-.548v-1.35a.548.548 0 01.55-.549h6.172zm5.535-15.807a.969.969 0 00.802-.95v-1.064a.964.964 0 00-.802-.95l-1.258-.222a6.495 6.495 0 00-.633-1.566l.734-1.046a.963.963 0 00-.107-1.227l-.763-.763a.97.97 0 00-1.23-.107l-1.058.732a6.539 6.539 0 00-1.574-.65l-.215-1.254a.965.965 0 00-.954-.8h-1.07a.97.97 0 00-.953.8l-.221 1.254a6.54 6.54 0 00-1.569.65l-1.048-.732a.97.97 0 00-1.23.107l-.766.754a.964.964 0 00-.108 1.227l.736 1.045a6.485 6.485 0 00-.652 1.564l-1.258.22a.97.97 0 00-.803.951v1.064a.964.964 0 00.803.95l1.26.222c.149.547.368 1.073.653 1.563l-.735 1.046a.964.964 0 00.107 1.226l.75.782a.97.97 0 001.23.108l1.05-.733c.491.284 1.019.502 1.567.65l.222 1.254a.966.966 0 00.953.8h1.07a.97.97 0 00.953-.8l.228-1.26a6.533 6.533 0 001.568-.65l1.049.733a.971.971 0 001.23-.108l.766-.751a.964.964 0 00.107-1.227l-.722-1.057a6.48 6.48 0 00.642-1.558l1.249-.227zm-7.593 1.598a3.081 3.081 0 01-2.84-1.893 3.057 3.057 0 01.666-3.342 3.077 3.077 0 013.351-.664 3.073 3.073 0 011.898 2.832c0 .814-.324 1.593-.9 2.169a3.08 3.08 0 01-2.175.898zm-15.255-7.139v4.048h1.276a.382.382 0 01.372.401.379.379 0 01-.064.19l-2.494 3.708a.38.38 0 01-.307.168.381.381 0 01-.308-.168l-2.518-3.707a.38.38 0 01.307-.592h1.28v-5.268a1.225 1.225 0 011.23-1.226h7.398a.55.55 0 01.55.549v1.349a.547.547 0 01-.55.549h-6.172z",
  fill: "#9B2FFF"
});

var changes_gear_ref3 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "changes-gear_svg__paint0_linear",
  x1: 46.344,
  y1: 73,
  x2: 46.344,
  y2: 13,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#F4F4F4"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "changes-gear_svg__filter0_d",
  x: 0.364,
  y: 0.237,
  width: 91.962,
  height: 97.525,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 6
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 10
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.05 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgChangesGear(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", changes_gear_extends({
    width: 93,
    height: 98,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), changes_gear_ref, changes_gear_ref2, changes_gear_ref3);
}

/* harmony default export */ var changes_gear = (SvgChangesGear);
// CONCATENATED MODULE: ./public/assets/technologies/title/front.svg
function front_extends() { front_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return front_extends.apply(this, arguments); }



var front_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#front_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M140.493 65.503V41.79h70.023v23.712h-70.023z",
  fill: "url(#front_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M140.493 65.503V41.79h70.023v23.712h-70.023z",
  fill: "url(#front_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M236.676 27.318v56.107h-26.16V41.79h-70.023V27.318h96.183z",
  fill: "url(#front_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M236.681 106.914v-23.49h-26.16v23.49h26.16z",
  fill: "url(#front_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M114.554 42.902l56.774 78.706-24.157 6.011-56.44-78.483 23.823-6.234z",
  fill: "url(#front_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M114.554 42.902l56.774 78.706-24.157 6.011-56.44-78.483 23.823-6.234z",
  fill: "url(#front_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M171.328 121.608l-24.157 6.012v23.712l24.157-6.234v-23.49z",
  fill: "url(#front_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M171.328 121.608l-24.157 6.012v23.712l24.157-6.234v-23.49z",
  fill: "url(#front_svg__paint7_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M147.171 127.619v23.712l-56.44-79.373V49.137l56.44 78.482z",
  fill: "url(#front_svg__paint8_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M26.5 147.77V91.886h26.161v41.523h69.8v14.361h-95.96z",
  fill: "url(#front_svg__paint9_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M26.5 171.482v-23.711h96.183v23.711H26.5z",
  fill: "url(#front_svg__paint10_linear)"
}));

var front_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint5_linear",
  x1: 188.289,
  y1: 34.824,
  x2: 98.513,
  y2: 76.577,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint10_linear",
  x1: 115.284,
  y1: 161.036,
  x2: 100.124,
  y2: 203.361,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint1_linear",
  x1: 205.129,
  y1: 55.056,
  x2: 186.212,
  y2: 93.505,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint2_linear",
  x1: 256.917,
  y1: 21.968,
  x2: 180.367,
  y2: 86.119,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint3_linear",
  x1: 234.669,
  y1: 96.565,
  x2: 211.827,
  y2: 114.075,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint4_linear",
  x1: 156.087,
  y1: 99.788,
  x2: 103.605,
  y2: 129.091,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint0_linear",
  x1: 219.755,
  y1: 56.486,
  x2: 184.452,
  y2: 93.454,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#B7BFC8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint6_linear",
  x1: 169.469,
  y1: 138.236,
  x2: 143.964,
  y2: 152.504,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#B7BFC8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint7_linear",
  x1: 169.469,
  y1: 138.236,
  x2: 143.964,
  y2: 152.504,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint8_linear",
  x1: 142.83,
  y1: 106.327,
  x2: 74.463,
  y2: 132.306,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "front_svg__paint9_linear",
  x1: 142.654,
  y1: 86.557,
  x2: 66.386,
  y2: 150.577,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DAE4F0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "front_svg__filter0_d",
  x: 0.593,
  y: 0.908,
  width: 280.274,
  height: 214.257,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 9.139,
  dy: 8.636
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 17.523
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgFront(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", front_extends({
    width: 243,
    height: 216,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), front_ref, front_ref2);
}

/* harmony default export */ var front = (SvgFront);
// CONCATENATED MODULE: ./public/assets/technologies/title/back.svg
function back_extends() { back_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return back_extends.apply(this, arguments); }



var back_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#back_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M62.906 170.532v-20.83l21.79 12.418 13.38-5.207 12.177 3.044 3.044 9.213h30.764l2.964-9.213 12.177-2.964 13.219 5.367 21.631-12.818.16 20.91-22.031 12.417-12.979-5.207-12.417 3.124-2.804 9.214h-30.684l-3.044-9.214-12.418-2.964-13.218 5.288-21.711-12.578z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M62.906 170.532v-20.83l21.79 12.418 13.38-5.207 12.177 3.044 3.044 9.213h30.764l2.964-9.213 12.177-2.964 13.219 5.367 21.631-12.818.16 20.91-22.031 12.417-12.979-5.207-12.417 3.124-2.804 9.214h-30.684l-3.044-9.214-12.418-2.964-13.218 5.288-21.711-12.578z",
  fill: "url(#back_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M205.749 154.189v-20.91h-22.032l-2.483 20.91h24.515z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M205.749 154.189v-20.91h-22.032l-2.483 20.91h24.515z",
  fill: "url(#back_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M194.132 116.135V98.99l-21.791 14.821 21.791 2.324z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M194.132 116.135V98.99l-21.791 14.821 21.791 2.324z",
  fill: "url(#back_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M51.209 154.028v-20.749l23.874.481 1.362 22.912-25.236-2.644z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M51.209 154.028v-20.749l23.874.481 1.362 22.912-25.236-2.644z",
  fill: "url(#back_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M62.986 114.292V99.15l9.694 4.887v10.255h-9.694z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M62.986 114.292V99.15l9.694 4.887v10.255h-9.694z",
  fill: "url(#back_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M128.6 106.441c-16.99 0-30.764 7.999-30.764 17.866 0 3.916 2.17 7.537 5.849 10.481 0 0 6.889-7.277 24.915-7.277s24.915 7.277 24.915 7.277c3.679-2.944 5.849-6.565 5.849-10.481 0-9.867-13.774-17.866-30.764-17.866z",
  fill: "url(#back_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M72.04 141.936l-4.968-6.89-15.862-1.762v-17.706l15.863-1.762 4.967-7.13-9.053-7.53 21.63-12.659 13.14 5.207 12.417-2.723 2.964-9.133h30.684l3.124 9.133 12.257 2.804 12.899-5.368 22.031 12.578-9.053 7.611 5.128 6.97 15.542 2.002v17.706l-15.542 1.762-5.128 7.21 8.973 7.291-21.631 12.818-13.219-5.368-12.177 2.965-2.964 9.213h-30.764l-3.044-9.213-12.177-3.045-13.38 5.208-21.79-12.418 9.133-7.771zm56.56.561c16.99 0 30.764-8.071 30.764-18.026s-13.774-18.026-30.764-18.026c-16.99 0-30.764 8.071-30.764 18.026s13.774 18.026 30.764 18.026z",
  fill: "url(#back_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M134.991 101.627V89.932l12.234 6.972 7.511-2.924 6.837 1.71 1.709 5.172h17.272l1.664-5.172 6.837-1.665 7.421 3.014 12.144-7.197.09 11.74-12.369 6.971-7.286-2.923-6.972 1.754-1.574 5.172h-17.227l-1.709-5.172-6.972-1.664-7.421 2.968-12.189-7.061z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M134.991 101.627V89.932l12.234 6.972 7.511-2.924 6.837 1.71 1.709 5.172h17.272l1.664-5.172 6.837-1.665 7.421 3.014 12.144-7.197.09 11.74-12.369 6.971-7.286-2.923-6.972 1.754-1.574 5.172h-17.227l-1.709-5.172-6.972-1.664-7.421 2.968-12.189-7.061z",
  fill: "url(#back_svg__paint7_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M215.187 92.451v-11.74h-12.369l-1.394 11.74h13.763z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M215.187 92.451v-11.74h-12.369l-1.394 11.74h13.763z",
  fill: "url(#back_svg__paint8_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M208.665 71.086v-9.625l-12.234 8.32 12.234 1.305z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M208.665 71.086v-9.625l-12.234 8.32 12.234 1.305z",
  fill: "url(#back_svg__paint9_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M128.424 92.361v-11.65l13.403.27.765 12.864-14.168-1.484z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M128.424 92.361v-11.65l13.403.27.765 12.864-14.168-1.484z",
  fill: "url(#back_svg__paint10_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M135.036 70.052V61.55l5.442 2.744v5.757h-5.442z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M135.036 70.052V61.55l5.442 2.744v5.757h-5.442z",
  fill: "url(#back_svg__paint11_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M171.869 65.643c-9.539 0-17.272 4.49-17.272 10.03 0 2.198 1.218 4.231 3.284 5.884 0 0 3.868-4.085 13.988-4.085s13.988 4.085 13.988 4.085c2.066-1.653 3.284-3.686 3.284-5.884 0-5.54-7.733-10.03-17.272-10.03z",
  fill: "url(#back_svg__paint12_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M140.118 85.565l-2.788-3.868-8.906-.99v-9.94l8.906-.99 2.788-4.002-5.082-4.228 12.144-7.107 7.376 2.924 6.972-1.53 1.664-5.127h17.227l1.754 5.128 6.882 1.574 7.241-3.014 12.369 7.062-5.082 4.273 2.878 3.913 8.726 1.124v9.94l-8.726.99-2.878 4.048 5.037 4.093-12.144 7.197-7.421-3.014-6.837 1.664-1.664 5.173h-17.272l-1.709-5.173-6.837-1.709-7.511 2.924-12.234-6.972 5.127-4.363zm31.755.315c9.539 0 17.272-4.53 17.272-10.12 0-5.59-7.733-10.12-17.272-10.12-9.539 0-17.272 4.53-17.272 10.12 0 5.59 7.733 10.12 17.272 10.12z",
  fill: "url(#back_svg__paint13_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M39.354 95.932V80.804l15.827 9.019 9.717-3.782 8.844 2.21 2.21 6.692h22.344l2.153-6.691 8.844-2.153 9.601 3.898 15.71-9.31.116 15.187-16.001 9.019-9.426-3.782-9.019 2.269-2.036 6.691H75.953l-2.211-6.691-9.019-2.153-9.6 3.84-15.769-9.135z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M39.354 95.932V80.804l15.827 9.019 9.717-3.782 8.844 2.21 2.21 6.692h22.344l2.153-6.691 8.844-2.153 9.601 3.898 15.71-9.31.116 15.187-16.001 9.019-9.426-3.782-9.019 2.269-2.036 6.691H75.953l-2.211-6.691-9.019-2.153-9.6 3.84-15.769-9.135z",
  fill: "url(#back_svg__paint14_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M143.099 84.062V68.876h-16.001l-1.804 15.186h17.805z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M143.099 84.062V68.876h-16.001l-1.804 15.186h17.805z",
  fill: "url(#back_svg__paint15_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M134.662 56.424V43.973l-15.827 10.764 15.827 1.687z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M134.662 56.424V43.973l-15.827 10.764 15.827 1.687z",
  fill: "url(#back_svg__paint16_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M30.86 83.946v-15.07l17.338.35.99 16.64-18.329-1.92z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M30.86 83.946v-15.07l17.338.35.99 16.64-18.329-1.92z",
  fill: "url(#back_svg__paint17_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M39.412 55.086V44.09l7.04 3.55v7.447h-7.04z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M39.412 55.086V44.09l7.04 3.55v7.447h-7.04z",
  fill: "url(#back_svg__paint18_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M87.068 49.385c-12.34 0-22.343 5.809-22.343 12.975 0 2.844 1.576 5.474 4.248 7.613 0 0 5.004-5.285 18.095-5.285 13.092 0 18.096 5.285 18.096 5.285 2.672-2.139 4.247-4.769 4.247-7.613 0-7.166-10.003-12.975-22.343-12.975z",
  fill: "url(#back_svg__paint19_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M45.987 75.162l-3.608-5.004-11.52-1.28V56.019l11.52-1.28 3.608-5.179-6.575-5.469 15.71-9.193 9.542 3.782 9.019-1.978 2.153-6.634H98.12l2.269 6.634 8.902 2.036 9.368-3.898 16.001 9.135-6.575 5.527 3.724 5.062 11.288 1.455v12.859l-11.288 1.28-3.724 5.237 6.517 5.295-15.71 9.31-9.601-3.9-8.844 2.154-2.153 6.691H75.952l-2.21-6.691-8.845-2.211-9.717 3.782-15.826-9.019 6.633-5.644zm41.079.407c12.34 0 22.343-5.861 22.343-13.091 0-7.23-10.004-13.092-22.343-13.092-12.34 0-22.344 5.861-22.344 13.092 0 7.23 10.004 13.091 22.344 13.091z",
  fill: "url(#back_svg__paint20_linear)"
}));

var back_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint10_linear",
  x1: 213.658,
  y1: 84.985,
  x2: 143.452,
  y2: 115.526,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint20_linear",
  x1: 129.192,
  y1: 59.917,
  x2: 85.822,
  y2: 116.759,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint1_linear",
  x1: 203.025,
  y1: 140.89,
  x2: 77.976,
  y2: 195.289,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint2_linear",
  x1: 203.025,
  y1: 140.89,
  x2: 77.976,
  y2: 195.289,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint3_linear",
  x1: 203.025,
  y1: 140.89,
  x2: 77.976,
  y2: 195.289,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint4_linear",
  x1: 203.025,
  y1: 140.89,
  x2: 77.976,
  y2: 195.289,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint5_linear",
  x1: 154.631,
  y1: 122.3,
  x2: 128.24,
  y2: 161.725,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#B7BFC8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint6_linear",
  x1: 186.602,
  y1: 120.946,
  x2: 126.888,
  y2: 199.21,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint7_linear",
  x1: 213.658,
  y1: 84.985,
  x2: 143.452,
  y2: 115.526,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint8_linear",
  x1: 213.658,
  y1: 84.985,
  x2: 143.452,
  y2: 115.526,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint9_linear",
  x1: 213.658,
  y1: 84.985,
  x2: 143.452,
  y2: 115.526,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint0_linear",
  x1: 203.025,
  y1: 140.89,
  x2: 77.976,
  y2: 195.289,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint11_linear",
  x1: 213.658,
  y1: 84.985,
  x2: 143.452,
  y2: 115.526,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint12_linear",
  x1: 186.483,
  y1: 74.546,
  x2: 171.667,
  y2: 96.681,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#B7BFC8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint13_linear",
  x1: 204.437,
  y1: 73.781,
  x2: 170.912,
  y2: 117.721,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint14_linear",
  x1: 141.12,
  y1: 74.404,
  x2: 50.299,
  y2: 113.913,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint15_linear",
  x1: 141.12,
  y1: 74.404,
  x2: 50.299,
  y2: 113.913,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint16_linear",
  x1: 141.12,
  y1: 74.404,
  x2: 50.299,
  y2: 113.913,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint17_linear",
  x1: 141.12,
  y1: 74.404,
  x2: 50.299,
  y2: 113.913,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint18_linear",
  x1: 141.12,
  y1: 74.404,
  x2: 50.299,
  y2: 113.913,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "back_svg__paint19_linear",
  x1: 105.974,
  y1: 60.902,
  x2: 86.807,
  y2: 89.537,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#B7BFC8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "back_svg__filter0_d",
  x: 0.859,
  y: 0.068,
  width: 264.328,
  height: 239.931,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 10,
  dy: 10
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 20
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgBack(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", back_extends({
    width: 266,
    height: 240,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), back_ref, back_ref2);
}

/* harmony default export */ var back = (SvgBack);
// CONCATENATED MODULE: ./public/assets/technologies/title/mobile.svg
function mobile_extends() { mobile_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return mobile_extends.apply(this, arguments); }



var mobile_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#mobile_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M268.095 124.853l-75.612 43.736-.064 16.411 75.599-43.381.077-16.766z",
  fill: "url(#mobile_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M192.479 168.422L30.178 74.99v16.767l162.301 93.141v-16.476z",
  fill: "url(#mobile_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M268.091 125.023L105.293 31.446 30.179 75.158l162.299 93.602 75.613-43.737z",
  fill: "url(#mobile_svg__paint2_linear)"
}));

var mobile_ref2 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#mobile_svg__filter1_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M88.659 43.454c-7.991-4.66-20.875-4.66-28.825 0-2.232 1.308-4.964-.521-5.937 1.119 0 1.42.015 5.913 0 7.216-.036 3.102 1.971 6.212 6.018 8.572 7.991 4.66 20.875 4.66 28.826 0 3.975-2.33 5.942-5.391 5.922-8.453-.01-1.571 0-5.087 0-7.335-1-1.397-4.068.016-6.004-1.12z",
  fill: "#CCDEF2"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M88.659 43.454c-7.991-4.66-20.875-4.66-28.825 0-2.232 1.308-4.964-.521-5.937 1.119 0 1.42.015 5.913 0 7.216-.036 3.102 1.971 6.212 6.018 8.572 7.991 4.66 20.875 4.66 28.826 0 3.975-2.33 5.942-5.391 5.922-8.453-.01-1.571 0-5.087 0-7.335-1-1.397-4.068.016-6.004-1.12z",
  fill: "url(#mobile_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M88.659 36.428c-7.991-4.66-20.875-4.66-28.825 0-7.95 4.66-7.91 12.248.081 16.908 7.991 4.66 20.875 4.66 28.826 0 7.95-4.66 7.868-12.248-.082-16.907z",
  fill: "#CCDEF2"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M88.659 36.428c-7.991-4.66-20.875-4.66-28.825 0-7.95 4.66-7.91 12.248.081 16.908 7.991 4.66 20.875 4.66 28.826 0 7.95-4.66 7.868-12.248-.082-16.907z",
  fill: "url(#mobile_svg__paint4_linear)"
}));

var mobile_ref3 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#mobile_svg__filter2_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M178.463 44.969l-22.619 13.05v8.938l22.619-12.98V44.97z",
  fill: "url(#mobile_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M155.839 58.018l-22.619-13.05v9.01l22.619 12.962v-8.922z",
  fill: "url(#mobile_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M178.462 45.263l-22.765-13.078-22.474 13.078 22.619 13.05 22.62-13.05z",
  fill: "url(#mobile_svg__paint7_linear)"
}));

var _ref4 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#mobile_svg__filter3_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M141.388 83.523l-22.62 13.05v8.939l22.62-12.98v-9.009z",
  fill: "url(#mobile_svg__paint8_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M118.772 96.573l-22.62-13.05v9.01l22.62 12.962v-8.922z",
  fill: "url(#mobile_svg__paint9_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M141.389 83.813l-22.765-13.079L96.15 83.813l22.619 13.05 22.62-13.05z",
  fill: "url(#mobile_svg__paint10_linear)"
}));

var _ref5 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#mobile_svg__filter4_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M242.191 103.724l-60.409 35.471v5.974l60.409-35.401v-6.044z",
  fill: "url(#mobile_svg__paint11_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M181.785 139.197l-22.619-13.05v6.044l22.619 12.963v-5.957z",
  fill: "url(#mobile_svg__paint12_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M242.189 103.723L219.58 90.75l-60.415 35.688 22.619 13.05 60.405-35.765z",
  fill: "url(#mobile_svg__paint13_linear)"
}));

var _ref6 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#mobile_svg__filter5_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M189.575 110.74c-3.925-2.289-10.251-2.289-14.156 0-1.096.642-2.437-.256-2.915.549 0 .697.007 2.904 0 3.544-.018 1.523.968 3.051 2.955 4.21 3.925 2.288 10.251 2.288 14.156 0 1.952-1.145 2.918-2.648 2.908-4.152-.005-.771 0-2.498 0-3.602-.492-.686-1.998.008-2.948-.549z",
  fill: "#CCDEF2"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M189.575 110.74c-3.925-2.289-10.251-2.289-14.156 0-1.096.642-2.437-.256-2.915.549 0 .697.007 2.904 0 3.544-.018 1.523.968 3.051 2.955 4.21 3.925 2.288 10.251 2.288 14.156 0 1.952-1.145 2.918-2.648 2.908-4.152-.005-.771 0-2.498 0-3.602-.492-.686-1.998.008-2.948-.549z",
  fill: "url(#mobile_svg__paint14_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M189.577 107.292c-3.924-2.288-10.251-2.288-14.155 0-3.904 2.289-3.884 6.015.04 8.303 3.924 2.289 10.251 2.289 14.155 0 3.904-2.288 3.864-6.014-.04-8.303z",
  fill: "#CCDEF2"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M189.577 107.292c-3.924-2.288-10.251-2.288-14.155 0-3.904 2.289-3.884 6.015.04 8.303 3.924 2.289 10.251 2.289 14.155 0 3.904-2.288 3.864-6.014-.04-8.303z",
  fill: "url(#mobile_svg__paint15_linear)"
}));

var _ref7 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M228.831 78.387l-12.222 7.05v4.83l12.222-7.013v-4.867z",
  fill: "url(#mobile_svg__paint16_linear)"
});

var _ref8 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M216.612 85.437l-12.221-7.05v4.867l12.221 7.004v-4.82z",
  fill: "url(#mobile_svg__paint17_linear)"
});

var _ref9 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M228.831 78.549l-12.299-7.067-12.143 7.067 12.221 7.05 12.221-7.05z",
  fill: "url(#mobile_svg__paint18_linear)"
});

var _ref10 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint6_linear",
  x1: 154.099,
  y1: 57.264,
  x2: 133.22,
  y2: 72.054,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint18_linear",
  x1: 224.209,
  y1: 80.962,
  x2: 213.937,
  y2: 91.399,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#DCECFF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint17_linear",
  x1: 215.672,
  y1: 85.03,
  x2: 204.391,
  y2: 93.021,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint16_linear",
  x1: 227.891,
  y1: 85.033,
  x2: 216.606,
  y2: 93.023,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D7DDE4"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DFECFB"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint15_linear",
  x1: 188.74,
  y1: 113.456,
  x2: 180.2,
  y2: 122.006,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#D8EAFF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint14_linear",
  x1: 190.983,
  y1: 112.717,
  x2: 185.8,
  y2: 123.533,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D7DEE7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D6DFEA"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint0_linear",
  x1: 262.202,
  y1: 152.622,
  x2: 192.399,
  y2: 202.044,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D7DFE9"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D9EAFF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint1_linear",
  x1: 99.961,
  y1: 102.742,
  x2: 30.178,
  y2: 152.171,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint2_linear",
  x1: 152.784,
  y1: 90.083,
  x2: 89.244,
  y2: 154.648,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#E8EDF3"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint3_linear",
  x1: 91.527,
  y1: 47.481,
  x2: 80.973,
  y2: 69.505,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#E9F1FB"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#E5EEFA"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint4_linear",
  x1: 86.954,
  y1: 48.98,
  x2: 69.563,
  y2: 66.391,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#CEE5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint5_linear",
  x1: 176.723,
  y1: 57.27,
  x2: 155.838,
  y2: 72.057,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D4DCE6"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#E9F3FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint13_linear",
  x1: 195.848,
  y1: 130.904,
  x2: 176.837,
  y2: 150.222,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#D6E7FB"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint7_linear",
  x1: 169.907,
  y1: 49.729,
  x2: 150.895,
  y2: 69.047,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#CEE5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint8_linear",
  x1: 139.648,
  y1: 95.825,
  x2: 118.762,
  y2: 110.612,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#CFDAE7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DBE7F5"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint9_linear",
  x1: 117.032,
  y1: 95.819,
  x2: 96.153,
  y2: 110.609,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint10_linear",
  x1: 132.834,
  y1: 88.279,
  x2: 113.823,
  y2: 107.597,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#CCE3FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint11_linear",
  x1: 202.662,
  y1: 135.481,
  x2: 181.776,
  y2: 150.269,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D3DAE2"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "mobile_svg__paint12_linear",
  x1: 180.045,
  y1: 138.443,
  x2: 159.166,
  y2: 153.233,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "mobile_svg__filter1_d",
  x: 47.156,
  y: 31.008,
  width: 54.247,
  height: 44.403,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 4.814
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 3.37
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "mobile_svg__filter5_d",
  x: 168.652,
  y: 104.613,
  width: 27.725,
  height: 22.886,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 2.889
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 1.926
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "mobile_svg__filter4_d",
  x: 152.424,
  y: 88.824,
  width: 96.507,
  height: 67.9,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 4.814
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 3.37
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "mobile_svg__filter3_d",
  x: 89.41,
  y: 68.809,
  width: 58.719,
  height: 48.258,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 4.814
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 3.37
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "mobile_svg__filter2_d",
  x: 126.48,
  y: 30.259,
  width: 58.724,
  height: 48.253,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dy: 4.814
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 3.37
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "mobile_svg__filter0_d",
  x: 0.725,
  y: 0.86,
  width: 317.214,
  height: 232.85,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 10.195,
  dy: 9.062
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 19.824
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgMobile(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", mobile_extends({
    width: 318,
    height: 234,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), mobile_ref, mobile_ref2, mobile_ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9, _ref10);
}

/* harmony default export */ var mobile = (SvgMobile);
// CONCATENATED MODULE: ./public/assets/technologies/title/qa.svg
function qa_extends() { qa_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return qa_extends.apply(this, arguments); }



var qa_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#qa_svg__filter0_d)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M131.402 172.392l23.166-3.342 22.808-8.998-23.779 26.221-22.195-13.881zm84.021-18.509v-22.576l-26.95 3.88-126.824 14.628v22.577l23.78 13.881 19.023-6.169 34.877 20.051-9.512-11.781v22.578l23.78 13.882 61.826-70.951z",
  fill: "url(#qa_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M131.399 149.812l45.973-12.339-23.779 26.221-22.194-13.882zm84.02-18.509l-26.95-15.424-126.823 33.933 23.779 13.882 19.024-6.17 34.876 20.051-9.512 10.797 23.78 13.882 61.826-70.951z",
  fill: "#282828"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M131.399 149.812l45.973-12.339-23.779 26.221-22.194-13.882zm84.02-18.509l-26.95-15.424-126.823 33.933 23.779 13.882 19.024-6.17 34.876 20.051-9.512 10.797 23.78 13.882 61.826-70.951z",
  fill: "url(#qa_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M73.165 103.101c-7.993-4.688-12.79-2.178-12.79-8.428 0-6.25 4.797-10.938 12.79-15.625 7.994-4.688 17.586-7.813 28.777-7.813 11.191 0 19.185 1.562 27.178 6.25 7.994 4.688 11.191 10.938 11.191 17.188 0 6.25-3.219 5.399-12.812 10.086-4.796 3.125-10.618 2.364-17.013 3.926l1.048-13.25-30.375-1.563v12.354c-4.796-1.563-6.395-1.563-7.994-3.125zm76.738-36.554c-14.388-14.627-30.866-20.68-50.05-20.68-19.185 0-34.681 12.867-49.07 20.68C39.606 72.616 32.28 62.615 30 70.57v24.104c0 10.938 6.395 20.313 20.783 28.126 7.994 3.125 17.586 7.813 28.777 9.376v12.5h28.777v-10.938c15.987-1.562 30.375-4.688 41.566-10.938 14.389-7.813 20.784-17.188 20.784-28.126V70.57c-2.198-8.09-9.454 2.13-20.784-4.022z",
  fill: "url(#qa_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M73.165 86.252c-7.993-4.688-12.79-9.375-12.79-15.626 0-6.25 4.797-10.937 12.79-15.625 7.994-4.688 17.586-7.813 28.777-7.813 11.191 0 19.185 1.563 27.178 6.25 7.994 4.688 11.191 10.938 11.191 17.188 0 6.25-3.197 10.938-12.79 15.626-4.796 3.125-11.191 4.688-17.585 6.25l1.598-17.188-30.375-1.562v15.625c-4.796-1.562-6.395-1.562-7.994-3.125zm76.738-43.751C135.515 34.688 119.528 30 100.343 30c-19.184 0-35.171 4.688-49.56 12.5C36.395 50.314 30 59.69 30 70.627c0 10.938 6.395 20.314 20.783 28.127 7.994 3.125 17.586 7.812 28.777 9.375v12.5h28.777V109.69c15.987-1.562 30.375-4.687 41.566-10.937 14.389-7.813 20.784-17.189 20.784-28.127 0-10.937-6.395-20.313-20.784-28.126z",
  fill: "#282828"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M73.165 86.252c-7.993-4.688-12.79-9.375-12.79-15.626 0-6.25 4.797-10.937 12.79-15.625 7.994-4.688 17.586-7.813 28.777-7.813 11.191 0 19.185 1.563 27.178 6.25 7.994 4.688 11.191 10.938 11.191 17.188 0 6.25-3.197 10.938-12.79 15.626-4.796 3.125-11.191 4.688-17.585 6.25l1.598-17.188-30.375-1.562v15.625c-4.796-1.562-6.395-1.562-7.994-3.125zm76.738-43.751C135.515 34.688 119.528 30 100.343 30c-19.184 0-35.171 4.688-49.56 12.5C36.395 50.314 30 59.69 30 70.627c0 10.938 6.395 20.314 20.783 28.127 7.994 3.125 17.586 7.812 28.777 9.375v12.5h28.777V109.69c15.987-1.562 30.375-4.687 41.566-10.937 14.389-7.813 20.784-17.189 20.784-28.127 0-10.937-6.395-20.313-20.784-28.126z",
  fill: "url(#qa_svg__paint3_linear)"
}));

var qa_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "qa_svg__paint0_linear",
  x1: 63.878,
  y1: 167.771,
  x2: 202.95,
  y2: 124.41,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#E1E4E9"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.532,
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDE0E4"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "qa_svg__paint1_linear",
  x1: 186.34,
  y1: 173.878,
  x2: 123.544,
  y2: 239.489,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#E2F0FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#FEFEFF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "qa_svg__paint2_linear",
  x1: 47.67,
  y1: 123.076,
  x2: 92.082,
  y2: 157.561,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#DFE5EC"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "qa_svg__paint3_linear",
  x1: 144.082,
  y1: 90.855,
  x2: 78.416,
  y2: 150.68,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#E2F0FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#FEFEFF"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "qa_svg__filter0_d",
  x: 0,
  y: 0,
  width: 265.423,
  height: 274.834,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 10,
  dy: 10
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 20
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
})));

function SvgQa(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", qa_extends({
    width: 266,
    height: 275,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), qa_ref, qa_ref2);
}

/* harmony default export */ var qa = (SvgQa);
// CONCATENATED MODULE: ./public/assets/technologies/title/devops.svg
function devops_extends() { devops_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return devops_extends.apply(this, arguments); }



var devops_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  opacity: 0.15,
  filter: "url(#devops_svg__filter0_f)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M150 127l-8-68 102 77.722L172 154l-22-27z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M172.036 153.893l-21.945-26.932-7.97-67.75 101.662 77.465-71.747 17.217z",
  stroke: "#000",
  strokeWidth: 0.19
}));

var devops_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M158.931 113.593c-3.314-1.949-6.514-2.981-9.371-3.095v-.574c0-28.544-20.115-63.394-44.915-77.723-20-11.578-36.8-6.305-42.743 11.005-1.486-1.146-3.086-2.293-4.686-3.21-16.343-9.515-29.6-1.834-29.6 16.966s13.257 41.728 29.6 51.243c8.229 4.7 92.001 53.306 101.715 58.923 12.8 7.451 23.2 1.49 23.2-13.298s-10.4-32.786-23.2-40.237z",
  fill: "url(#devops_svg__paint0_linear)"
});

var devops_ref3 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M158.931 113.593c-3.314-1.949-6.514-2.981-9.371-3.095v-.574c0-28.544-20.115-63.394-44.915-77.723-20-11.578-36.8-6.305-42.743 11.005-1.486-1.146-3.086-2.293-4.686-3.21-16.343-9.515-29.6-1.834-29.6 16.966s13.257 41.728 29.6 51.243c8.229 4.7 92.001 53.306 101.715 58.923 12.8 7.451 23.2 1.49 23.2-13.298s-10.4-32.786-23.2-40.237z",
  fill: "url(#devops_svg__paint1_linear)"
});

var devops_ref4 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M158.931 113.593c-3.314-1.949-6.514-2.981-9.371-3.095v-.574c0-28.544-20.115-63.394-44.915-77.723-20-11.578-36.8-6.305-42.743 11.005-1.486-1.146-3.086-2.293-4.686-3.21-16.343-9.515-29.6-1.834-29.6 16.966s13.257 41.728 29.6 51.243c8.229 4.7 92.001 53.306 101.715 58.923 12.8 7.451 23.2 1.49 23.2-13.298s-10.4-32.786-23.2-40.237z",
  fill: "url(#devops_svg__paint2_linear)"
});

var devops_ref5 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M227.506 127.347c0-14.788-10.4-32.672-23.201-40.123-3.314-1.949-6.514-2.98-9.371-3.095v-.573c0-28.545-20.114-63.394-44.915-77.724-14.057-8.139-26.171-6.993-34.628-1.26l-14.286 8.253c-8.115-4.242-15.315-4.242-20.457-.688l-42.972 24.99c5.257-2.292 12.114-1.49 19.543 2.867 1.6.917 3.2 2.063 4.686 3.21 5.828-17.31 22.743-22.47 42.743-11.005 24.8 14.33 44.914 49.178 44.914 77.723v.573c2.857.229 6.057 1.147 9.372 3.095 12.8 7.452 23.2 25.45 23.2 40.123 0 6.19-1.829 10.891-5.029 13.642l42.743-24.647c4.686-2.178 7.658-7.451 7.658-15.361z",
  fill: "url(#devops_svg__paint3_linear)"
});

var devops_ref6 = /*#__PURE__*/external_react_["createElement"]("g", {
  opacity: 0.15,
  filter: "url(#devops_svg__filter1_f)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M36.252 81.127l13.808 3.452v13.808l17.26 10.356-32.794 3.452 1.726-31.068z",
  fill: "#000"
}));

var devops_ref7 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M45.18 99.373l-22.593 13.034v25.998l22.592-12.964V99.373z",
  fill: "url(#devops_svg__paint4_linear)"
});

var devops_ref8 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M22.592 112.407L0 99.373v26.068l22.592 12.947v-25.981z",
  fill: "url(#devops_svg__paint5_linear)"
});

var devops_ref9 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M45.185 99.37L22.448 86.306.001 99.37l22.592 13.033 22.592-13.034z",
  fill: "url(#devops_svg__paint6_linear)"
});

var devops_ref10 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M45.18 71.746L22.588 84.78v25.998L45.18 97.814V71.746z",
  fill: "url(#devops_svg__paint7_linear)"
});

var _ref11 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M22.593 84.78L0 71.746v26.068l22.592 12.947V84.78z",
  fill: "url(#devops_svg__paint8_linear)"
});

var _ref12 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M45.185 71.752L22.448 58.69.001 71.752l22.592 13.034 22.592-13.034z",
  fill: "url(#devops_svg__paint9_linear)"
});

var _ref13 = /*#__PURE__*/external_react_["createElement"]("g", {
  opacity: 0.15,
  filter: "url(#devops_svg__filter2_f)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M56.655 135.233l11.33-7.506 14.87 8.63-31.069 13.808 4.869-14.932z",
  fill: "#000"
}));

var _ref14 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M69.358 113.178l-22.593 13.034v25.997l22.593-12.964v-26.067z",
  fill: "url(#devops_svg__paint10_linear)"
});

var _ref15 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M46.762 126.212L24.17 113.178v26.067l22.592 12.947v-25.98z",
  fill: "url(#devops_svg__paint11_linear)"
});

var _ref16 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M69.352 113.172l-22.737-13.063-22.448 13.063 22.593 13.034 22.592-13.034z",
  fill: "url(#devops_svg__paint12_linear)"
});

var _ref17 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint5_linear",
  x1: 20.854,
  y1: 111.654,
  x2: 0,
  y2: 126.426,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D8E5F5"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint12_linear",
  x1: 60.807,
  y1: 117.633,
  x2: 41.819,
  y2: 136.928,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#DDE9F8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint11_linear",
  x1: 45.024,
  y1: 125.459,
  x2: 24.17,
  y2: 140.23,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D8E5F5"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint0_linear",
  x1: 54.581,
  y1: 104.369,
  x2: 145.315,
  y2: 89.653,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#0788FC"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#84B8FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint1_linear",
  x1: 120.821,
  y1: 55.234,
  x2: 40.504,
  y2: 118.146,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.735,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint2_linear",
  x1: 170.246,
  y1: 71.451,
  x2: 91.362,
  y2: 174.763,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDE7F2"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint3_linear",
  x1: 191.608,
  y1: 112.375,
  x2: 79.253,
  y2: 187.17,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#DFEBFA"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint4_linear",
  x1: 43.441,
  y1: 111.659,
  x2: 22.581,
  y2: 126.429,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#B7BFC8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint10_linear",
  x1: 67.62,
  y1: 125.464,
  x2: 46.759,
  y2: 140.234,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D4DBE3"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#CDE1F8"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint6_linear",
  x1: 36.641,
  y1: 103.83,
  x2: 17.652,
  y2: 123.125,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint7_linear",
  x1: 43.442,
  y1: 84.032,
  x2: 22.582,
  y2: 98.802,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#D4DBE3"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#CDE1F8"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint8_linear",
  x1: 20.855,
  y1: 84.027,
  x2: 0.001,
  y2: 98.799,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D8E5F5"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "devops_svg__paint9_linear",
  x1: 36.641,
  y1: 76.213,
  x2: 17.652,
  y2: 95.508,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#DDE9F8"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "devops_svg__filter1_f",
  x: 4.526,
  y: 51.127,
  width: 92.794,
  height: 91.068,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "BackgroundImageFix",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 15,
  result: "effect1_foregroundBlur"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "devops_svg__filter2_f",
  x: 21.786,
  y: 97.727,
  width: 91.068,
  height: 82.438,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "BackgroundImageFix",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 15,
  result: "effect1_foregroundBlur"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "devops_svg__filter0_f",
  x: 112,
  y: 29,
  width: 162,
  height: 155,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "BackgroundImageFix",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 15,
  result: "effect1_foregroundBlur"
})));

function SvgDevops(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", devops_extends({
    width: 274,
    height: 184,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), devops_ref, devops_ref2, devops_ref3, devops_ref4, devops_ref5, devops_ref6, devops_ref7, devops_ref8, devops_ref9, devops_ref10, _ref11, _ref12, _ref13, _ref14, _ref15, _ref16, _ref17);
}

/* harmony default export */ var devops = (SvgDevops);
// CONCATENATED MODULE: ./public/assets/technologies/title/ai.svg
function ai_extends() { ai_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return ai_extends.apply(this, arguments); }



var ai_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#ai_svg__filter0_di)"
}, /*#__PURE__*/external_react_["createElement"]("circle", {
  cx: 58.789,
  cy: 118.9,
  r: 38.789,
  fill: "url(#ai_svg__paint0_radial)"
}));

var ai_ref2 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#ai_svg__filter1_di)"
}, /*#__PURE__*/external_react_["createElement"]("circle", {
  cx: 165.791,
  cy: 179.935,
  r: 38.789,
  fill: "url(#ai_svg__paint1_radial)"
}));

var ai_ref3 = /*#__PURE__*/external_react_["createElement"]("g", {
  filter: "url(#ai_svg__filter2_di)"
}, /*#__PURE__*/external_react_["createElement"]("circle", {
  cx: 166.005,
  cy: 58.789,
  r: 38.789,
  fill: "url(#ai_svg__paint2_radial)"
}));

var ai_ref4 = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M85.899 100.067l51.635-30.87s2.118.109 3.849 2.755c1.731 2.646.676 5.106.676 5.106l-51.636 30.87s-2.262.332-3.993-2.608c-1.73-2.94-.531-5.253-.531-5.253z",
  fill: "#000"
});

var ai_ref5 = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M85.899 100.067l51.635-30.87s2.118.109 3.849 2.755c1.731 2.646.676 5.106.676 5.106l-51.636 30.87s-2.262.332-3.993-2.608c-1.73-2.94-.531-5.253-.531-5.253z",
  fill: "url(#ai_svg__paint3_linear)"
});

var ai_ref6 = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M88.991 128.544l51.042 31.841s.871 1.934-.693 4.682-4.235 2.933-4.235 2.933l-51.041-31.841s-1.33-1.861.496-4.743c1.825-2.882 4.431-2.872 4.431-2.872z",
  fill: "#000"
});

var ai_ref7 = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M88.991 128.544l51.042 31.841s.871 1.934-.693 4.682-4.235 2.933-4.235 2.933l-51.041-31.841s-1.33-1.861.496-4.743c1.825-2.882 4.431-2.872 4.431-2.872z",
  fill: "url(#ai_svg__paint4_linear)"
});

var ai_ref8 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("radialGradient", {
  id: "ai_svg__paint0_radial",
  cx: 0,
  cy: 0,
  r: 1,
  gradientUnits: "userSpaceOnUse",
  gradientTransform: "rotate(110.674 1.034 81.082) scale(52.3167)"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.479,
  stopColor: "#F5F5F5"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.825,
  stopColor: "#E7E7E7"
})), /*#__PURE__*/external_react_["createElement"]("radialGradient", {
  id: "ai_svg__paint1_radial",
  cx: 0,
  cy: 0,
  r: 1,
  gradientUnits: "userSpaceOnUse",
  gradientTransform: "rotate(110.674 33.433 148.595) scale(52.3167)"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.479,
  stopColor: "#F5F5F5"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.825,
  stopColor: "#E7E7E7"
})), /*#__PURE__*/external_react_["createElement"]("radialGradient", {
  id: "ai_svg__paint2_radial",
  cx: 0,
  cy: 0,
  r: 1,
  gradientUnits: "userSpaceOnUse",
  gradientTransform: "rotate(110.674 75.425 88.095) scale(52.3167)"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#fff"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.479,
  stopColor: "#F5F5F5"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.825,
  stopColor: "#E7E7E7"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "ai_svg__filter0_di",
  x: 0,
  y: 60.111,
  width: 137.577,
  height: 137.577,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 10,
  dy: 10
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 15
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0",
  result: "hardAlpha"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 1.751,
  dy: -1.751
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 2.626
}), /*#__PURE__*/external_react_["createElement"]("feComposite", {
  in2: "hardAlpha",
  operator: "arithmetic",
  k2: -1,
  k3: 1
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "shape",
  result: "effect2_innerShadow"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "ai_svg__filter1_di",
  x: 107.002,
  y: 121.146,
  width: 137.577,
  height: 137.577,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 10,
  dy: 10
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 15
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0",
  result: "hardAlpha"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 1.751,
  dy: -1.751
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 2.626
}), /*#__PURE__*/external_react_["createElement"]("feComposite", {
  in2: "hardAlpha",
  operator: "arithmetic",
  k2: -1,
  k3: 1
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "shape",
  result: "effect2_innerShadow"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "ai_svg__filter2_di",
  x: 107.216,
  y: 0,
  width: 137.577,
  height: 137.577,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 10,
  dy: 10
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 15
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "BackgroundImageFix",
  result: "effect1_dropShadow"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "effect1_dropShadow",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  "in": "SourceAlpha",
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0",
  result: "hardAlpha"
}), /*#__PURE__*/external_react_["createElement"]("feOffset", {
  dx: 1.751,
  dy: -1.751
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 2.626
}), /*#__PURE__*/external_react_["createElement"]("feComposite", {
  in2: "hardAlpha",
  operator: "arithmetic",
  k2: -1,
  k3: 1
}), /*#__PURE__*/external_react_["createElement"]("feColorMatrix", {
  values: "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  in2: "shape",
  result: "effect2_innerShadow"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ai_svg__paint3_linear",
  x1: 117.779,
  y1: 91.643,
  x2: 113.013,
  y2: 83.495,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DCDCDC"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.599,
  stopColor: "#E8EEF5"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D7DFE8"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ai_svg__paint4_linear",
  x1: 111.043,
  y1: 153.06,
  x2: 116.116,
  y2: 145.099,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#DCDCDC"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.599,
  stopColor: "#E8EEF5"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#D7DFE8"
})));

function SvgAi(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", ai_extends({
    width: 245,
    height: 259,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), ai_ref, ai_ref2, ai_ref3, ai_ref4, ai_ref5, ai_ref6, ai_ref7, ai_ref8);
}

/* harmony default export */ var ai = (SvgAi);
// CONCATENATED MODULE: ./public/assets/technologies/title/bigdata.svg
function bigdata_extends() { bigdata_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return bigdata_extends.apply(this, arguments); }



var bigdata_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 102.993c-14.829-8.647-38.736-8.647-53.49 0-4.141 2.428-9.21-.967-11.016 2.076 0 2.635.028 10.974 0 13.392-.068 5.755 3.658 11.527 11.168 15.906 14.829 8.648 38.736 8.648 53.489 0 7.376-4.323 11.027-10.005 10.989-15.687-.02-2.915 0-9.439 0-13.611-1.856-2.592-7.548.029-11.14-2.076z",
  fill: "#CCDEF2"
});

var bigdata_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 102.993c-14.829-8.647-38.736-8.647-53.49 0-4.141 2.428-9.21-.967-11.016 2.076 0 2.635.028 10.974 0 13.392-.068 5.755 3.658 11.527 11.168 15.906 14.829 8.648 38.736 8.648 53.489 0 7.376-4.323 11.027-10.005 10.989-15.687-.02-2.915 0-9.439 0-13.611-1.856-2.592-7.548.029-11.14-2.076z",
  fill: "url(#bigdata_svg__paint0_linear)"
});

var bigdata_ref3 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 89.954c-14.829-8.647-38.736-8.647-53.49 0-14.752 8.647-14.677 22.727.152 31.374 14.829 8.647 38.736 8.647 53.489 0s14.602-22.727-.151-31.374z",
  fill: "#CCDEF2"
});

var bigdata_ref4 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 89.954c-14.829-8.647-38.736-8.647-53.49 0-14.752 8.647-14.677 22.727.152 31.374 14.829 8.647 38.736 8.647 53.489 0s14.602-22.727-.151-31.374z",
  fill: "url(#bigdata_svg__paint1_linear)"
});

var bigdata_ref5 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 61.255c-14.829-8.647-38.736-8.647-53.49 0-4.141 2.427-9.21-.968-11.016 2.076 0 2.635.028 10.973 0 13.391-.068 5.755 3.658 11.528 11.168 15.907 14.829 8.647 38.736 8.647 53.489 0 7.376-4.323 11.027-10.005 10.989-15.687-.02-2.915 0-9.44 0-13.61-1.856-2.593-7.548.028-11.14-2.077z",
  fill: "#CCDEF2"
});

var bigdata_ref6 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 61.255c-14.829-8.647-38.736-8.647-53.49 0-4.141 2.427-9.21-.968-11.016 2.076 0 2.635.028 10.973 0 13.391-.068 5.755 3.658 11.528 11.168 15.907 14.829 8.647 38.736 8.647 53.489 0 7.376-4.323 11.027-10.005 10.989-15.687-.02-2.915 0-9.44 0-13.61-1.856-2.593-7.548.028-11.14-2.077z",
  fill: "url(#bigdata_svg__paint2_linear)"
});

var bigdata_ref7 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 48.216c-14.829-8.647-38.736-8.647-53.49 0-14.752 8.647-14.677 22.727.152 31.374 14.829 8.647 38.736 8.647 53.489 0s14.602-22.727-.151-31.374z",
  fill: "#CCDEF2"
});

var bigdata_ref8 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.507 48.216c-14.829-8.647-38.736-8.647-53.49 0-14.752 8.647-14.677 22.727.152 31.374 14.829 8.647 38.736 8.647 53.489 0s14.602-22.727-.151-31.374z",
  fill: "url(#bigdata_svg__paint3_linear)"
});

var bigdata_ref9 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.51 19.528c-14.83-8.647-38.737-8.647-53.49 0-4.141 2.428-9.21-.967-11.016 2.077 0 2.634.028 10.973 0 13.39-.068 5.756 3.658 11.528 11.168 15.908 14.828 8.647 38.736 8.647 53.489 0 7.376-4.324 11.027-10.006 10.989-15.688-.02-2.915 0-9.44 0-13.61-1.857-2.593-7.548.029-11.14-2.077z",
  fill: "#CCDEF2"
});

var bigdata_ref10 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.51 19.528c-14.83-8.647-38.737-8.647-53.49 0-4.141 2.428-9.21-.967-11.016 2.077 0 2.634.028 10.973 0 13.39-.068 5.756 3.658 11.528 11.168 15.908 14.828 8.647 38.736 8.647 53.489 0 7.376-4.324 11.027-10.006 10.989-15.688-.02-2.915 0-9.44 0-13.61-1.857-2.593-7.548.029-11.14-2.077z",
  fill: "url(#bigdata_svg__paint4_linear)"
});

var bigdata_ref11 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.51 6.485c-14.829-8.647-38.736-8.647-53.489 0s-14.677 22.728.151 31.375c14.83 8.647 38.736 8.647 53.49 0 14.752-8.647 14.601-22.728-.152-31.375z",
  fill: "#CCDEF2"
});

var bigdata_ref12 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M64.51 6.485c-14.829-8.647-38.736-8.647-53.489 0s-14.677 22.728.151 31.375c14.83 8.647 38.736 8.647 53.49 0 14.752-8.647 14.601-22.728-.152-31.375z",
  fill: "url(#bigdata_svg__paint5_linear)"
});

var bigdata_ref13 = /*#__PURE__*/external_react_["createElement"]("g", {
  opacity: 0.4,
  filter: "url(#bigdata_svg__filter0_f)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M125.206 153.626l20.868-12.763 27.39 15.651-50.866 19.564 2.608-22.452z",
  fill: "#000"
}));

var bigdata_ref14 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.286 140.412l-44.345 25.584v17.525l44.345-25.447v-17.662z",
  fill: "url(#bigdata_svg__paint6_linear)"
});

var bigdata_ref15 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M106.948 165.996l-44.345-25.584v17.662l44.345 25.413v-17.491z",
  fill: "url(#bigdata_svg__paint7_linear)"
});

var bigdata_ref16 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.296 140.414l-44.629-25.641-44.061 25.641 44.345 25.584 44.345-25.584z",
  fill: "url(#bigdata_svg__paint8_linear)"
});

var bigdata_ref17 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.286 119.551l-44.345 25.583v17.525l44.345-25.446v-17.662z",
  fill: "url(#bigdata_svg__paint9_linear)"
});

var _ref18 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M106.948 145.134l-44.345-25.583v17.662l44.345 25.413v-17.492z",
  fill: "url(#bigdata_svg__paint10_linear)"
});

var _ref19 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.296 119.552l-44.63-25.64-44.06 25.641 44.345 25.583 44.345-25.584z",
  fill: "url(#bigdata_svg__paint11_linear)"
});

var _ref20 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.286 98.682l-44.345 25.583v17.525l44.345-25.446V98.682z",
  fill: "url(#bigdata_svg__paint12_linear)"
});

var _ref21 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M106.948 124.265L62.603 98.682v17.662l44.345 25.413v-17.492z",
  fill: "url(#bigdata_svg__paint13_linear)"
});

var _ref22 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.296 98.685l-44.63-25.64-44.06 25.64 44.345 25.584 44.345-25.584z",
  fill: "url(#bigdata_svg__paint14_linear)"
});

var _ref23 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.286 77.822l-44.345 25.584v17.525l44.345-25.447V77.822z",
  fill: "url(#bigdata_svg__paint15_linear)"
});

var _ref24 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M106.948 103.406L62.603 77.822v17.662l44.345 25.414v-17.492z",
  fill: "url(#bigdata_svg__paint16_linear)"
});

var _ref25 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.296 77.812l-44.63-25.64-44.06 25.64 44.345 25.584 44.345-25.584z",
  fill: "url(#bigdata_svg__paint17_linear)"
});

var _ref26 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.288 56.95l-44.345 25.583v17.525l44.345-25.447V56.95z",
  fill: "url(#bigdata_svg__paint18_linear)"
});

var _ref27 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M106.95 82.533L62.605 56.949v17.662l44.345 25.413V82.533z",
  fill: "url(#bigdata_svg__paint19_linear)"
});

var _ref28 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.298 56.945l-44.629-25.64-44.06 25.64 44.344 25.584 44.345-25.584z",
  fill: "url(#bigdata_svg__paint20_linear)"
});

var _ref29 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint10_linear",
  x1: 103.537,
  y1: 143.656,
  x2: 62.603,
  y2: 172.651,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint20_linear",
  x1: 134.527,
  y1: 65.701,
  x2: 97.255,
  y2: 103.574,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#D8E1EB"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint1_linear",
  x1: 61.342,
  y1: 113.245,
  x2: 29.072,
  y2: 145.553,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#D8E5F4"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint2_linear",
  x1: 69.828,
  y1: 68.728,
  x2: 50.243,
  y2: 109.596,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EFEFEF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#E2E6EB"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint3_linear",
  x1: 61.342,
  y1: 71.507,
  x2: 29.072,
  y2: 103.815,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#D8E5F4"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint4_linear",
  x1: 69.831,
  y1: 27.002,
  x2: 50.246,
  y2: 67.87,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EFEFEF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#E2E6EB"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint5_linear",
  x1: 61.346,
  y1: 29.777,
  x2: 29.075,
  y2: 62.084,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#D8E5F4"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint6_linear",
  x1: 147.875,
  y1: 164.528,
  x2: 106.929,
  y2: 193.519,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#C8CFD7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDECFD"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint7_linear",
  x1: 103.537,
  y1: 164.518,
  x2: 62.603,
  y2: 193.513,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint8_linear",
  x1: 134.524,
  y1: 149.169,
  x2: 97.253,
  y2: 187.042,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint9_linear",
  x1: 147.875,
  y1: 143.667,
  x2: 106.929,
  y2: 172.658,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#C8CFD7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDECFD"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint0_linear",
  x1: 69.828,
  y1: 110.467,
  x2: 50.243,
  y2: 151.334,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EFEFEF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#E2E6EB"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint11_linear",
  x1: 134.524,
  y1: 128.308,
  x2: 97.252,
  y2: 166.181,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint12_linear",
  x1: 147.875,
  y1: 122.798,
  x2: 106.929,
  y2: 151.788,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#C8CFD7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDECFD"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint13_linear",
  x1: 103.537,
  y1: 122.787,
  x2: 62.603,
  y2: 151.782,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint14_linear",
  x1: 134.524,
  y1: 107.441,
  x2: 97.252,
  y2: 145.314,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint15_linear",
  x1: 147.875,
  y1: 101.938,
  x2: 106.929,
  y2: 130.929,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#C8CFD7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDECFD"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint16_linear",
  x1: 103.537,
  y1: 101.928,
  x2: 62.603,
  y2: 130.923,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint17_linear",
  x1: 134.524,
  y1: 86.568,
  x2: 97.252,
  y2: 124.441,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.082,
  stopColor: "#BFD4ED"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#EDF5FF"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint18_linear",
  x1: 147.877,
  y1: 81.065,
  x2: 106.931,
  y2: 110.056,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#C8CFD7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#DDECFD"
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "bigdata_svg__paint19_linear",
  x1: 103.539,
  y1: 81.055,
  x2: 62.605,
  y2: 110.05,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#EDF5FF"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#BFD4ED"
})), /*#__PURE__*/external_react_["createElement"]("filter", {
  id: "bigdata_svg__filter0_f",
  x: 119.551,
  y: 137.817,
  width: 56.96,
  height: 41.309,
  filterUnits: "userSpaceOnUse",
  colorInterpolationFilters: "sRGB"
}, /*#__PURE__*/external_react_["createElement"]("feFlood", {
  floodOpacity: 0,
  result: "BackgroundImageFix"
}), /*#__PURE__*/external_react_["createElement"]("feBlend", {
  "in": "SourceGraphic",
  in2: "BackgroundImageFix",
  result: "shape"
}), /*#__PURE__*/external_react_["createElement"]("feGaussianBlur", {
  stdDeviation: 1.523,
  result: "effect1_foregroundBlur"
})));

function SvgBigdata(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", bigdata_extends({
    width: 177,
    height: 184,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), bigdata_ref, bigdata_ref2, bigdata_ref3, bigdata_ref4, bigdata_ref5, bigdata_ref6, bigdata_ref7, bigdata_ref8, bigdata_ref9, bigdata_ref10, bigdata_ref11, bigdata_ref12, bigdata_ref13, bigdata_ref14, bigdata_ref15, bigdata_ref16, bigdata_ref17, _ref18, _ref19, _ref20, _ref21, _ref22, _ref23, _ref24, _ref25, _ref26, _ref27, _ref28, _ref29);
}

/* harmony default export */ var bigdata = (SvgBigdata);
// CONCATENATED MODULE: ./public/assets/technologies/languages/java.svg
function java_extends() { java_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return java_extends.apply(this, arguments); }



var java_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#java_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M144.914 40.758c-7.566-.761-16.142-1.623-24.698.202-14.906 3.195-11.782 17.871-11.642 18.495.032.145.148.274.317.372.18.103.421.172.69.186.52.026 1.003-.16 1.161-.447.047-.088 4.876-8.79 12.206-12.802 2.553-1.396 8.854-1.016 15.524-.613 6.11.369 13.034.787 18.899-.015 12.896-1.772 15.285-9.456 15.307-9.533.088-.301-.202-.596-.694-.707-.493-.11-1.052-.006-1.34.25-.04.036-4.169 3.646-12.472 5.027-3.427.573-7.983.115-13.258-.415z",
  fill: "url(#java_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M160.108 53.594c.55.044 1.072-.151 1.226-.46.154-.307-.108-.633-.614-.764-1.041-.27-25.598-6.559-33.853-1.793-5.69 3.285-6.266 6.424-6.686 8.716-.165.899-.307 1.675-.744 2.156-1.272 1.378-5.899 2.524-7.574 2.85-.482.093-.794.363-.757.656.021.159.14.302.324.408.157.09.361.155.591.178.517.054 12.711 1.262 17.514-2.99 1.836-1.61 2.389-3.243 2.877-4.683.757-2.23 1.304-3.841 6.088-4.638 5.59-.923 21.449.35 21.608.364z",
  fill: "url(#java_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M79.307 46.972c-1.213.35-1.973.953-2.196 1.746-.746 2.65 4.59 7.426 15.028 13.452h.001a170.49 170.49 0 004.52 2.514c17.285 9.289 28.673 9.861 29.149 9.88.519.023.995-.166 1.146-.454.152-.287-.059-.596-.513-.745-7.482-2.467-19.026-8.474-26.638-12.868-8.517-4.917-12.242-7.775-12.85-8.65 1.293-.117 5.99 1.052 10.974 2.983.476.185 1.117.13 1.481-.125.365-.256.334-.63-.072-.864-2.442-1.41-14.881-8.364-20.03-6.87z",
  fill: "url(#java_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M140.501 76.117c-3.442-1.987-8.431-2.876-8.642-2.913-.489-.085-1.015.033-1.278.285-.263.253-.194.577.167.786.073.042 7.282 4.272 3.787 7.05-2.999 2.405-16.265 1.45-21.124.878-.525-.061-1.051.101-1.254.387-.173.244-.069.518.243.698.055.032.116.06.182.085.843.315 20.711 7.643 28.469 1.177 4.759-3.96 2.327-6.772-.55-8.433z",
  fill: "url(#java_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M111.679 79.863c.361-.143.543-.394.467-.642l-1.169-3.832c-.057-.187-.256-.35-.538-.445-.052-.017-5.207-1.755-11.792-4.796a100.453 100.453 0 01-8.023-4.166c-5.925-3.421-9.103-6.06-9.157-6.68a.301.301 0 01.207-.163c.388-.106 1.276.015 1.764.143.536.142 1.172.018 1.45-.282.277-.3.1-.674-.403-.852-4.861-1.715-8.097-2.057-9.62-1.018-2.697 1.843 2.613 6.654 7.98 10.214a49.86 49.86 0 002.582 1.59c9.59 5.535 24.892 10.896 25.046 10.949.374.13.845.123 1.206-.02z",
  fill: "url(#java_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M75.927 66.523c.488.06.984-.076 1.216-.33.232-.256.138-.565-.22-.767-.539-.294-5.357-2.836-8.615-1.328-.975.45-1.525 1.058-1.635 1.806-.306 2.089 2.996 5.341 10.093 9.943a57.657 57.657 0 002.612 1.6c9.579 5.53 19.029 7.523 19.426 7.605.386.08.816.031 1.121-.127.304-.158.435-.4.338-.63l-1.921-4.586c-.067-.16-.237-.298-.473-.387-.038-.015-3.82-1.448-9.189-3.806-1.016-.447-2.198-1.058-3.512-1.817-4.722-2.726-9.203-6.203-9.706-7.076.08-.064.22-.122.465-.1z",
  fill: "url(#java_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M65.858 80.097c21.96 12.656 37.638 17.202 42.361 17.094 1.672-.038 2.581-.463 3.05-.813a1.856 1.856 0 00.71-2.152c-.118-.312-.615-.535-1.168-.513-.553.024-.992.27-1.03.59-.04.182-.258.4-1.068.4-1.753-.034-13.471-3.83-29.473-12.177a174.277 174.277 0 01-6.668-3.66c-13.636-7.871-21.748-14.71-21.969-16.096 1.23-.127 5.92 1.242 10.329 3.03.496.202 1.181.137 1.54-.146.358-.282.26-.677-.224-.891a33.37 33.37 0 01-.942-.474c-6.572-3.357-14.573-7.003-18.138-5.376-1.086.496-1.68 1.173-1.764 2.01-.207 2.051 2.102 6.27 24.454 19.173v.001z",
  fill: "url(#java_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M111.837 100.079c-.167-.261-.605-.43-1.087-.42-.067 0-6.815.088-18.758-4.469-4.559-1.752-12.072-5.644-22.332-11.568A710.962 710.962 0 0149.911 71.75c-.389-.248-1.035-.278-1.493-.078-.458.2-.577.568-.276.85.077.073 7.926 7.377 21.941 15.468A170.622 170.622 0 0091.15 98.277c13.541 5.485 20.06 2.653 20.331 2.53.379-.173.522-.466.356-.728z",
  fill: "url(#java_svg__paint7_linear)"
}));

var java_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint0_linear",
  x1: 192.765,
  y1: 28.281,
  x2: 170.93,
  y2: -0.918,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint1_linear",
  x1: 173.599,
  y1: 46.328,
  x2: 156.88,
  y2: 24.504,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint2_linear",
  x1: 134.626,
  y1: 70.178,
  x2: 76.629,
  y2: 76.631,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint3_linear",
  x1: 154.872,
  y1: 76.852,
  x2: 139.289,
  y2: 59.167,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint4_linear",
  x1: 120.069,
  y1: 75.347,
  x2: 72.746,
  y2: 79.544,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint5_linear",
  x1: 108.521,
  y1: 80.24,
  x2: 62.863,
  y2: 78.344,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint6_linear",
  x1: 118.35,
  y1: 92.856,
  x2: 55.562,
  y2: 114.53,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "java_svg__paint7_linear",
  x1: 116.157,
  y1: 98.578,
  x2: 58.575,
  y2: 115.216,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "java_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "matrix(.86604 .49997 -.86604 .49997 115.135 3)",
  d: "M0 0h111.005v111.005H0z"
})));

function SvgJava(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", java_extends({
    width: 215,
    height: 138,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), java_ref, java_ref2);
}

/* harmony default export */ var java = (SvgJava);
// CONCATENATED MODULE: ./public/assets/technologies/languages/amazon-aws.svg
function amazon_aws_extends() { amazon_aws_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return amazon_aws_extends.apply(this, arguments); }



var amazon_aws_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#amazon-aws_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M84.6 45.462c-1.11.64-1.89 1.23-2.34 1.731-.42.52-.84 1.109-1.2 1.802a1.08 1.08 0 01-.48.519c-.3.173-.78.242-1.47.19l-3.15-.363c-.45-.052-.81-.156-1.05-.295a.926.926 0 01-.48-.762c.03-.502.15-.987.33-1.437.21-.468.48-.97.84-1.559-5.1.243-9.42-.658-12.96-2.701-2.52-1.455-3.81-3.031-3.84-4.711-.03-1.68 1.14-3.221 3.54-4.607 2.55-1.472 5.52-2.147 8.91-1.992 3.39.156 6.6 1.109 9.69 2.893 1.02.588 1.98 1.247 2.94 1.974.959.727 1.859 1.524 2.789 2.373l2.19-1.265c2.28-1.316 3.39-2.51 3.39-3.585-.03-1.09-1.2-2.286-3.51-3.62a21.46 21.46 0 00-3.63-1.645 46.47 46.47 0 00-4.26-1.281c-.69-.156-1.17-.294-1.44-.381a3.114 3.114 0 01-.57-.225c-.42-.243-.33-.537.3-.9l1.47-.85c.48-.277.9-.45 1.26-.484.36-.035.84 0 1.47.12 1.59.295 3.3.763 5.13 1.404 1.86.623 3.6 1.42 5.25 2.372 3.57 2.061 5.37 4.036 5.43 5.94.03 1.888-1.59 3.794-4.89 5.699l-9.72 5.61.06.035zm-16.74-4.4c.99.572 2.19 1.057 3.63 1.473 1.44.415 3.06.588 4.77.537 1.05-.053 2.04-.208 2.94-.52.9-.312 1.89-.745 2.91-1.334l1.26-.727a24.844 24.844 0 00-2.25-1.888 27.309 27.309 0 00-2.64-1.732c-2.01-1.16-3.87-1.784-5.67-1.887-1.8-.104-3.42.277-4.92 1.143-1.41.814-2.1 1.628-2.07 2.476-.03.849.66 1.663 2.04 2.46zM88.708 56.84c-.54-.312-.81-.572-.84-.831-.06-.243.15-.607.54-1.04l16.14-17.457c.42-.45.72-.727.93-.849.48-.277.99-.294 1.47-.017l2.94 1.697c.57.33.87.607.87.85.06.242-.18.588-.57 1.02l-14.82 14.375 24.54-8.763c.75-.26 1.32-.38 1.74-.346.419.034.959.208 1.499.52l2.4 1.385c.57.329.87.606.9.866.06.242-.15.606-.6 1.004l-15.359 14.34 25.289-8.607c.78-.243 1.38-.347 1.77-.33.42.035.93.191 1.47.503l2.79 1.61c.48.278.51.572 0 .866-.15.087-.33.156-.54.243-.21.087-.51.19-.96.312l-30.419 9.213c-.78.243-1.38.347-1.8.312-.42-.035-.93-.19-1.44-.485l-2.58-1.49c-.57-.328-.87-.606-.9-.865-.03-.26.15-.606.63-1.022l14.7-13.855-23.94 8.486c-.75.26-1.32.381-1.77.364-.45-.017-.96-.208-1.5-.52l-2.58-1.49zm37.739 22.722c-1.56-.9-2.94-1.905-4.08-2.979-1.139-1.073-1.919-1.974-2.249-2.684-.21-.433-.24-.797-.09-1.022.15-.225.39-.433.66-.589l1.529-.883c.63-.364 1.17-.398 1.62-.139.18.104.33.225.45.364.12.139.27.364.45.606.57.849 1.32 1.697 2.25 2.511a20.5 20.5 0 003.21 2.27c1.89 1.09 3.69 1.748 5.37 1.956 1.68.208 3.18-.034 4.41-.745.84-.484 1.26-1.039 1.29-1.68.03-.64-.48-1.523-1.47-2.65l-3-3.29c-1.5-1.662-2.1-3.186-1.74-4.537.33-1.334 1.29-2.477 2.85-3.377a12.051 12.051 0 014.14-1.455 18.68 18.68 0 014.62-.173c1.59.12 3.12.415 4.68.9 1.56.485 2.97 1.126 4.29 1.888a18.3 18.3 0 011.89 1.23c.6.45 1.11.883 1.62 1.316.45.433.87.848 1.23 1.264.36.416.6.762.72 1.04.18.38.24.692.15.952-.06.242-.3.485-.72.727l-1.41.814c-.63.364-1.2.416-1.65.156-.24-.139-.51-.433-.78-.866-.93-1.437-2.46-2.771-4.59-4-1.71-.988-3.33-1.611-4.83-1.82-1.5-.207-2.85.018-4.08.728-.84.485-1.26 1.074-1.23 1.75.03.675.57 1.645 1.65 2.857l2.91 3.239c1.47 1.645 2.07 3.1 1.77 4.347-.3 1.247-1.26 2.32-2.82 3.221-1.29.745-2.73 1.264-4.26 1.559-1.56.277-3.18.38-4.83.26-1.68-.104-3.33-.433-5.01-.953a23.215 23.215 0 01-4.92-2.113z",
  fill: "url(#amazon-aws_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M117.539 91.253c-17.16-1.49-35.37-7.534-47.7-14.652-17.28-9.975-26.46-22.653-27.6-35.573-.09-1.021 1.89-1.195 2.34-.173 5.34 11.604 16.56 23.242 32.79 32.611 10.95 6.322 25.26 11.95 41.01 15.64 2.4.519 1.98 2.39-.84 2.147z",
  fill: "url(#amazon-aws_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M126.329 90.923c.36-1.663-7.56-5.265-11.16-6.893-1.08-.485-.39-1.057 1.11-.918 9.6.97 17.729 6.98 17.489 8.365-.27 1.403-10.919 5.957-20.639 5.472-1.5-.069-1.92-.727-.66-1.039 4.17-1.021 13.53-3.342 13.86-4.987z",
  fill: "url(#amazon-aws_svg__paint2_linear)"
}));

var amazon_aws_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "amazon-aws_svg__paint0_linear",
  x1: 174.939,
  y1: 64.472,
  x2: 57.482,
  y2: 36.51,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "amazon-aws_svg__paint1_linear",
  x1: 132.193,
  y1: 84.292,
  x2: 32.573,
  y2: 96.156,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "amazon-aws_svg__paint2_linear",
  x1: 140.845,
  y1: 87.924,
  x2: 123.397,
  y2: 70.264,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "amazon-aws_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "scale(1.22477 .70706) rotate(45 -.35 90.902)",
  d: "M0 0h105.305v63.044H0z"
})));

function SvgAmazonAws(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", amazon_aws_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), amazon_aws_ref, amazon_aws_ref2);
}

/* harmony default export */ var amazon_aws = (SvgAmazonAws);
// CONCATENATED MODULE: ./public/assets/technologies/languages/android.svg
function android_extends() { android_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return android_extends.apply(this, arguments); }



var android_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#android_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M87.991 35.627c-2.944-1.699-7.721-1.699-10.665 0l-21.33 12.314c-2.944 1.7-2.944 4.458 0 6.157 2.944 1.7 7.721 1.7 10.665 0l21.33-12.314c2.944-1.7 2.944-4.457 0-6.157z",
  fill: "url(#android_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M151.981 72.569c-2.943-1.7-7.721-1.7-10.665 0l-21.33 12.314c-2.943 1.699-2.943 4.457 0 6.157 2.944 1.699 7.722 1.699 10.665 0l21.33-12.314c2.944-1.7 2.944-4.458 0-6.157z",
  fill: "url(#android_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M66.352 60.434c-2.944 1.699-2.944 4.457 0 6.157l-10.356 5.978c-2.944 1.7-2.944 4.458 0 6.157 2.943 1.7 7.721 1.7 10.665 0l10.356-5.978 10.665 6.156-10.356 5.979c-2.944 1.7-2.944 4.458 0 6.157 2.944 1.7 7.721 1.7 10.665 0l10.356-5.979c2.943 1.7 7.721 1.7 10.665 0l31.995-18.47-42.66-24.628-31.995 18.47z",
  fill: "url(#android_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M155.175 49.153l10.5-.582c2.075-.114 3.594-1.176 3.397-2.374-.208-1.197-2.037-2.075-4.111-1.96l-11.476.634c-1.466-2.245-3.706-4.357-6.836-6.163-3.173-1.832-6.895-3.131-10.847-3.972l1.264-6.56c.224-1.194-1.258-2.272-3.333-2.404-2.069-.13-3.93.73-4.164 1.924l-1.173 6.083c-8.714-.517-17.731 1.074-24.407 4.928l42.66 24.628c6.719-3.879 9.47-9.124 8.526-14.182zm-27.19-5.828c-1.472-.85-1.472-2.229 0-3.078 1.472-.85 3.861-.85 5.332 0 1.472.85 1.472 2.228 0 3.078-1.471.85-3.86.85-5.332 0zm10.665 6.157c-1.472-.85-1.472-2.229 0-3.078 1.472-.85 3.861-.85 5.332 0 1.472.85 1.472 2.228 0 3.078-1.471.85-3.86.85-5.332 0z",
  fill: "url(#android_svg__paint3_linear)"
}));

var android_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "android_svg__paint0_linear",
  x1: 103.742,
  y1: 32.779,
  x2: 95.451,
  y2: 20.696,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "android_svg__paint1_linear",
  x1: 167.732,
  y1: 69.72,
  x2: 159.442,
  y2: 57.638,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "android_svg__paint2_linear",
  x1: 158.446,
  y1: 56.872,
  x2: 117.768,
  y2: 10.672,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "android_svg__paint3_linear",
  x1: 182.234,
  y1: 43.141,
  x2: 129.743,
  y2: 5.416,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "android_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "scale(1.22477 .70706) rotate(45 25.85 115.524)",
  d: "M0 0h98.519v98.519H0z"
})));

function SvgAndroid(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", android_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), android_ref, android_ref2);
}

/* harmony default export */ var android = (SvgAndroid);
// CONCATENATED MODULE: ./public/assets/technologies/languages/django.svg
function django_extends() { django_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return django_extends.apply(this, arguments); }



var django_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#django_svg__clip0)",
  fillRule: "evenodd",
  clipRule: "evenodd"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M62.209 34.699c-3.297 1.883-2.847 4.4 1.879 6.101l8.69-5.017c-2.702-2.926-7.419-2.885-10.57-1.084zm23.252-6.102l5.284 3.05-25.013 14.441c-5.894-2.446-11.27-4.944-12.8-8.068-1.97-4.022 5.61-8.813 12.682-8.813 3.586 0 8.457 1.355 11.509 4.068l7.868-4.542c.156-.102.313-.204.47-.136",
  fill: "url(#django_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M93.798 39.784l-5.284-3.05 5.52-3.187 5.284 3.05-5.52 3.187z",
  fill: "url(#django_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M141.242 85.612c3.522 4.618 16.709-2.26 9.747-4.949-4.324-1.67-11.524 2.62-9.747 4.949zm13.739-7.66c4.87 2.267 6.248 5.304 4.58 7.796-2.845 4.25-11.085 6.256-16.44 5.152-2.776-.572-4.833-1.661-6.459-2.78-5.027-3.456-2.888-8.7 5.284-11.186a18.184 18.184 0 012.701-.61c3.233-.484 6.617-.104 10.334 1.627",
  fill: "url(#django_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M87.105 52.055c-3.918.574-.834 3.034 2.23 4l3.641-2.102c-2.074-.904-4.028-2.169-5.871-1.898zm3.523 8.677c-4.617-2.375-12.738-5.346-12.213-8.406.308-1.798 3.722-3.595 6.576-3.797 3.687-.26 7.67 1.519 11.156 3.187 1.002-.245 1.966-1.026 2.114-1.492-2.173-2.112-6.22-3.12-9.16-3.796l4.11-2.373c4.532.969 13.225 4.253 12.096 7.39-.593 1.647-4.024 3.136-6.694 4.677l-7.985 4.61z",
  fill: "url(#django_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M108.947 71.58l-5.284-3.052 6.811-3.932c2.243-1.294 5.449-2.727 5.167-3.932-.221-.944-2.777-2.01-4.462-2.576l-12.8 7.39-5.285-3.051 16.323-9.424c6.805 2.309 18.061 7.07 11.978 11.39-2.998 2.13-8.617 4.68-12.448 7.186",
  fill: "url(#django_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M122.452 72.867c-.549 1.344 1.319 3.046 4.228 3.66 2.639-1.649 5.42-3.298 8.338-4.948-3.361-3.333-11.255-1.918-12.566 1.288zm24.426 1.22c-1.879.542-3.757.832-5.636 1.22-.667-.243-.801-.711-1.292-.745-5.845 3.235-12.266 8.489-18.672 9.559-3.857.644-7.976.109-12.096-1.966-3.001-1.512-4.723-3.036-5.989-4.678l4.932-2.847c1.003 2.204 5.947 6.383 10.569 5.695 1.36-.203 2.212-.732 3.758-1.356-1.162-.604-2.745-.928-4.11-1.695-5.612-3.155-4.628-7.679 2.819-10.44 2.88-1.069 6.171-1.368 9.159-.95 6.208.87 11.958 4.395 16.558 8.204",
  fill: "url(#django_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M91.004 41.396c-3.898 2.243-6.693 3.858-11.42 6.587-4.871 2.553-8.519 4.719-12.443 5.02-.774.06-2.126.017-3.17-.068-.878-.071-2.596-.18-3.171-.474-.58-.296-1.191-1.563-1.527-2.102-.441-.708-.69-1.454-1.292-2.102 3.256.442 6.098 1.004 9.395 0 2.867-.873 5.95-2.757 9.042-4.542 3.054-1.763 6.617-3.804 9.313-5.36l5.273 3.042z",
  fill: "url(#django_svg__paint6_linear)"
}));

var django_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint0_linear",
  x1: 99.192,
  y1: 26.911,
  x2: 83.832,
  y2: 8.084,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint1_linear",
  x1: 101.139,
  y1: 35.589,
  x2: 95.798,
  y2: 30.008,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint2_linear",
  x1: 170.953,
  y1: 80.335,
  x2: 152.704,
  y2: 61.573,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint3_linear",
  x1: 113.962,
  y1: 47.47,
  x2: 100.167,
  y2: 31.81,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint4_linear",
  x1: 132.358,
  y1: 58.322,
  x2: 116.5,
  y2: 41.322,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint5_linear",
  x1: 156.304,
  y1: 68.805,
  x2: 138.316,
  y2: 47.551,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "django_svg__paint6_linear",
  x1: 99.058,
  y1: 36.833,
  x2: 90.474,
  y2: 24.926,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "django_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "matrix(.86604 .49997 -.86604 .49997 72 20)",
  d: "M0 0h120.086v41.568H0z"
})));

function SvgDjango(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", django_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), django_ref, django_ref2);
}

/* harmony default export */ var django = (SvgDjango);
// CONCATENATED MODULE: ./public/assets/technologies/languages/hp.svg
function hp_extends() { hp_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return hp_extends.apply(this, arguments); }



var hp_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#hp_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M62.025 89.02c-.463-.267-.917-.539-1.364-.813l35.741-9.624 13.123 7.576c2.303 1.33 6.602 1.767 9.553.973L157.839 76.7c6.344-1.708 7.486-5.444 2.535-8.302l-18.205-10.51-57.25 15.414-.005-.004-32.454 8.738c-15.66-14.68-12.474-34.164 9.565-46.887C83.43 22.792 115.877 20.7 141.023 28.87l-33.495 9.02-.002-.002-55.662 14.983 9.888 5.709 47.308-12.726 7.432 4.29-47.309 12.726 9.883 5.708 44.124-11.881c6.346-1.708 7.487-5.444 2.541-8.3l-8.312-4.798 35.777-9.634c.727.384 1.443.777 2.146 1.183 25.769 14.876 25.769 38.995 0 53.871-25.768 14.876-67.548 14.877-93.317 0zm89.159-18.876l-7.429-4.289-38.952 10.474 7.43 4.29 38.951-10.475z",
  fill: "url(#hp_svg__paint0_linear)"
}));

var hp_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "hp_svg__paint0_linear",
  x1: 232.826,
  y1: 45.052,
  x2: 137.036,
  y2: -52.72,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "hp_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "matrix(.86604 .49997 -.86604 .49997 108.684 8)",
  d: "M0 0h108.175v108.175H0z"
})));

function SvgHp(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", hp_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), hp_ref, hp_ref2);
}

/* harmony default export */ var hp = (SvgHp);
// CONCATENATED MODULE: ./public/assets/technologies/languages/jmeter.svg
function jmeter_extends() { jmeter_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return jmeter_extends.apply(this, arguments); }



var jmeter_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#jmeter_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M87.481 28.68c-.976-.142-3.016-.14-5.705.018l-.997 1.86c1.903-.2 3.71-.319 5.371-.354.124-.004.197-.004.197-.004-.065.004-.13 0-.197.004-.546.013-2.251.09-5.269.464 1.2.625 3.187 1.502 4.868 2.187 2.824-1.135 3.075-2.254 3.075-2.254s.667-1.628-1.343-1.921z",
  fill: "url(#jmeter_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M73.561 34.53a.05.05 0 00.03.009l-.175-.084c-.015 0-.03-.008-.044-.008l.19.084z",
  fill: "url(#jmeter_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M68.653 36.054c-.11-.038-.218-.084-.327-.13.109.046.218.092.327.13z",
  fill: "url(#jmeter_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M52.453 36.487c.044-.009.088-.026.132-.034.897-.232 1.764-.456 2.625-.675a317.312 317.312 0 018.378-1.998c.73-.16 1.436-.317 2.13-.465.232-.05.458-.097.684-.144.452-.093.89-.186 1.32-.274.401-.08.788-.161 1.174-.241.131-.026.262-.051.379-.076l.066-.013-.153-.072.102-.185c-.029.009-.065.013-.094.021-.584.11-1.174.224-1.765.347-.335.068-.678.14-1.028.207-.948.195-1.888.393-2.836.605-.955.206-1.91.422-2.85.637-.934.212-1.853.431-2.772.65-.918.22-1.823.44-2.72.664a211.06 211.06 0 00-3.383.861c-.736.19-1.451.376-2.158.566l-.169.299.167.08a98.072 98.072 0 012.771-.76z",
  fill: "url(#jmeter_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M66.028 36.738c-.175-.067-.342-.138-.51-.21.08.038.175.076.262.11.08.037.168.07.248.1z",
  fill: "url(#jmeter_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  opacity: 0.35,
  d: "M66.028 36.738c-.175-.067-.342-.138-.51-.21.08.038.175.076.262.11.08.037.168.07.248.1z",
  fill: "url(#jmeter_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M68.253 35.89c-.007-.004-.007-.004 0 0 .022.013.05.021.073.034.109.046.218.092.327.13-.13-.05-.262-.11-.4-.164z",
  fill: "url(#jmeter_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  opacity: 0.35,
  d: "M68.253 35.89c-.007-.004-.007-.004 0 0 .022.013.05.021.073.034.109.046.218.092.327.13-.13-.05-.262-.11-.4-.164z",
  fill: "url(#jmeter_svg__paint7_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M72.046 31.803c.977-.169 1.94-.33 2.887-.482a111.097 111.097 0 015.846-.771l.99-1.856c-.183.012-.372.021-.57.034-.72.047-1.486.102-2.288.17-.903.077-1.844.166-2.828.264-.903.093-1.844.2-2.799.314-.816.102-1.64.207-2.478.322l-.095.013-1.42 2.491c.925-.165 1.844-.334 2.755-.499z",
  fill: "url(#jmeter_svg__paint8_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M49.267 37.335c-.715.194-1.451.393-2.195.604-.007.004-.022.004-.03.008-.109.03-.21.055-.313.089-.503.14-.948.27-1.969.557.232.362.063.854-.2 1.376.569-.37.89-.842.856-1.368 2.007 1.268 4.359 2.01 6.837 1.655.219-.034.445-.072.671-.118-.97.064-1.727-.104-2.665-.713l-.007-.004.007.004c2.12.465 3.489.523 5.18.294.401-.055.824-.123 1.276-.208-2.602.024-4.458-.618-5.737-1.685l-1.303-.6c-.138.038-.27.071-.408.11z",
  fill: "url(#jmeter_svg__paint9_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M52.447 36.13c.912-.232 1.838-.465 2.771-.701.897-.224 1.801-.444 2.72-.663.919-.22 1.845-.435 2.77-.65.949-.22 1.897-.431 2.852-.639.94-.206 1.895-.405 2.836-.604.343-.072.678-.14 1.028-.207a195.63 195.63 0 011.764-.346c.03-.009.066-.013.095-.022l1.428-2.479c-.058.009-.124.021-.19.026-.889.127-1.793.254-2.697.398-.918.144-1.837.296-2.763.46-.78.14-1.552.284-2.333.432-.153.03-.313.064-.466.093a82.977 82.977 0 00-7.073 1.731c-.423.127-.846.262-1.262.393-.977.312-1.918.646-2.83.996l-1.428 2.504c.707-.19 1.43-.372 2.158-.566.205-.05.409-.101.62-.156z",
  fill: "url(#jmeter_svg__paint10_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M50.973 34.394a36.098 36.098 0 00-3.137 1.375c-.03.016-.073.033-.102.05.279-.682-.228-1.8-.228-1.792-.397 1.237-.99 2.344-2.13 3.111-.56-.214-1.061-.664-1.526-1.244.063.854.44 1.3.615 1.46-.75-.383-1.973-.524-3.277-.629 1.529.37 2.584.776 3.064 1.222-3.821 1.055-7.942 2.283-12.252 3.62.393.127.787.11 1.166.008.773-.245 5.9-1.836 12.923-3.756.197-.055.4-.106.605-.165.058-.017.11-.03.168-.046A578.95 578.95 0 0149.137 37l.532-.14.007-.004 1.428-2.504a1.016 1.016 0 00-.13.042z",
  fill: "url(#jmeter_svg__paint11_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M69.275 32.58l-.379.075c-.386.08-.78.157-1.174.241-.43.09-.867.182-1.32.275-.225.047-.451.093-.685.144a233.135 233.135 0 00-4.9 1.098c-.904.211-1.844.435-2.8.667-.911.224-1.844.452-2.807.697-.853.216-1.728.444-2.625.676-.044.008-.088.025-.131.034-.883.232-1.794.472-2.72.722-.022.004-.044.008-.059.016l1.303.6c-.015-.016-.036-.029-.058-.05 1.579.71 4.492 1.178 6.992 1.12a26.765 26.765 0 003.644-.348c.962-.152 1.961-.359 3.033-.617a58.138 58.138 0 002.924-.79 8.619 8.619 0 01-1.478-.414l-.262-.1c-.087-.034-.167-.072-.262-.11 1.813.465 3.606.38 5.4.033a10.162 10.162 0 01-2.265-.507c-.11-.038-.219-.084-.328-.13-.022-.013-.05-.021-.08-.038.641.15 1.238.251 1.828.297.124.013.248.017.372.02.19.009.379.009.568.008.124-.004.24-.004.365-.008.284-.013.575-.039.867-.081.095-.013.182-.021.27-.038.13-.026.255-.047.371-.072a27.949 27.949 0 001.984-.456c.065-.021.131-.034.197-.055.182-.055.35-.101.488-.148.204-.067.365-.126.474-.173a1.878 1.878 0 01-.255-.029c-.626-.092-1.521-.356-2.17-.612l1.194.554-1.193-.554a.05.05 0 00-.03-.008c-.058-.026-.123-.055-.196-.08.015 0 .03.008.044.008l-4.082-1.893a.083.083 0 01-.059.025z",
  fill: "url(#jmeter_svg__paint12_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M80.881 30.667c-.831.102-1.764.23-2.792.377-.058.009-.11.013-.16.026a179.341 179.341 0 00-5.796.969c-.89.16-1.815.342-2.8.532l4.083 1.894c1.566.33 2.49.35 3.606.244a49.2 49.2 0 003.9-.58 40.381 40.381 0 002.618-.57c.495-.127.933-.253 1.305-.376.32-.11.613-.22.89-.329-1.667-.684-3.653-1.561-4.854-2.187z",
  fill: "url(#jmeter_svg__paint13_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M65.773 36.642c-.087-.034-.167-.071-.262-.11.088.034.175.068.262.11z",
  fill: "url(#jmeter_svg__paint14_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  opacity: 0.35,
  d: "M65.773 36.642c-.087-.034-.167-.071-.262-.11.088.034.175.068.262.11z",
  fill: "url(#jmeter_svg__paint15_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M65.773 36.642c-.087-.034-.167-.071-.262-.11.088.034.175.068.262.11z",
  fill: "url(#jmeter_svg__paint16_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M68.245 35.887c.022.013.051.021.08.038-.022-.013-.05-.021-.08-.038z",
  fill: "url(#jmeter_svg__paint17_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  opacity: 0.35,
  d: "M68.245 35.887c.022.013.051.021.08.038-.022-.013-.05-.021-.08-.038z",
  fill: "url(#jmeter_svg__paint18_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M68.245 35.887c.022.013.051.021.08.038-.022-.013-.05-.021-.08-.038z",
  fill: "url(#jmeter_svg__paint19_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M73.823 34.62l-.534.308-.15-.086.534-.308-.188-.109.127-.073.521.3-.128.074-.182-.106z",
  fill: "url(#jmeter_svg__paint20_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M74.587 35.197l-.539.106-.089-.051.184-.31-.416.24-.145-.083.662-.382.2.115-.19.308.533-.11.2.115-.662.382-.144-.083.417-.24-.011-.007z",
  fill: "url(#jmeter_svg__paint21_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M53.981 53.053a13.952 13.952 0 01-1.517-.991 8.851 8.851 0 01-.964-.803l3.672-2.12c.274.246.576.497.907.754.32.251.709.508 1.162.77.435.25.885.456 1.348.614.473.163.965.256 1.477.277a5.078 5.078 0 001.577-.157 6.438 6.438 0 001.707-.707l20.193-11.658 4.397 2.539-20.05 11.575c-1.196.69-2.4 1.183-3.614 1.48a12.989 12.989 0 01-3.58.397 12.797 12.797 0 01-3.466-.555 15.556 15.556 0 01-3.249-1.415",
  fill: "url(#jmeter_svg__paint22_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M77.83 59.757L89.6 47.706l-.128-.074c-.816.537-1.613 1.051-2.39 1.544-.333.214-.68.43-1.04.649-.35.224-.702.444-1.053.657-.342.22-.678.425-1.01.616-.323.197-.617.373-.883.526L73.42 57.21l-3.929-2.268 20.805-12.01 5.985 3.455-11.428 11.772.085.049 20.675-6.433 5.986 3.455-20.805 12.01-4.1-2.366 9.848-5.685.84-.484a479.63 479.63 0 002.12-1.159c.38-.197.745-.392 1.096-.583.834-.439 1.698-.899 2.589-1.38l-.113-.066-21.201 6.573-4.042-2.333",
  fill: "url(#jmeter_svg__paint23_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M112.702 64.738c-.766-.442-1.674-.655-2.726-.637-1.042.023-2.179.346-3.412.97l5.517 3.186a5.706 5.706 0 001.153-.879c.295-.3.476-.602.543-.903.077-.306.03-.606-.14-.902-.169-.295-.481-.573-.935-.835zM99.85 72.796c-1.134-.655-2.013-1.354-2.636-2.097-.623-.743-.948-1.506-.974-2.287-.016-.788.289-1.586.916-2.396.647-.81 1.658-1.612 3.033-2.406 1.395-.805 2.789-1.408 4.182-1.809 1.394-.4 2.749-.62 4.065-.66a14.18 14.18 0 013.849.399c1.24.31 2.399.776 3.477 1.399 1.049.606 1.834 1.245 2.353 1.917.537.672.791 1.355.761 2.05-.021.7-.326 1.4-.915 2.1-.571.7-1.435 1.384-2.592 2.053l-2.106 1.215-9.673-5.584c-.588.362-1.044.734-1.367 1.117-.304.383-.471.763-.5 1.14-.011.378.116.747.38 1.107.273.366.699.716 1.275 1.049.483.278.96.527 1.433.745.492.218.993.414 1.504.589.511.174 1.041.332 1.59.474.559.136 1.15.264 1.775.384l-3.358 1.939a20.225 20.225 0 01-1.662-.368 20.176 20.176 0 01-1.547-.499c-.52-.19-1.045-.417-1.575-.679a24.233 24.233 0 01-1.688-.892",
  fill: "url(#jmeter_svg__paint24_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M118.244 79.423c.425.246.874.45 1.348.614.482.169.993.327 1.532.474l-3.23 1.865a12.51 12.51 0 01-2.03-.58 13.082 13.082 0 01-2.17-1.007c-.691-.399-1.253-.816-1.687-1.253-.435-.437-.67-.897-.707-1.378-.027-.486.169-.998.587-1.534.437-.536 1.168-1.1 2.193-1.691l7.67-4.428-2.071-1.196 1.836-1.06 3.834.538 4.607-1.218 2.765 1.596-3.386 1.956 3.857 2.227-3.258 1.881-3.858-2.227-7.67 4.428c-.617.356-.921.706-.913 1.05.009.345.259.66.751.943z",
  fill: "url(#jmeter_svg__paint25_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M138.744 79.773c-.766-.442-1.675-.655-2.726-.637-1.042.023-2.179.346-3.412.97l5.517 3.186a5.767 5.767 0 001.153-.879c.294-.3.475-.602.542-.902a1.175 1.175 0 00-.139-.902c-.17-.295-.481-.574-.935-.836zm-12.852 8.058c-1.135-.655-2.014-1.354-2.636-2.097-.623-.743-.948-1.505-.974-2.287-.017-.788.289-1.586.916-2.396.646-.81 1.657-1.612 3.033-2.406 1.395-.805 2.789-1.408 4.182-1.808 1.393-.4 2.748-.62 4.065-.66 1.335-.04 2.618.093 3.849.398 1.24.31 2.398.777 3.476 1.399 1.05.606 1.834 1.245 2.353 1.917.538.672.792 1.355.761 2.05-.021.7-.326 1.4-.915 2.1-.57.7-1.434 1.385-2.591 2.053l-2.107 1.216-9.672-5.585c-.589.362-1.044.734-1.367 1.117-.305.383-.471.763-.501 1.14-.011.378.116.747.38 1.108.274.366.699.715 1.276 1.048.482.278.96.527 1.433.745.492.218.993.415 1.504.59.511.174 1.041.332 1.59.473.558.136 1.15.264 1.774.384l-3.358 1.939a20.333 20.333 0 01-1.661-.368 19.992 19.992 0 01-1.547-.498 15.817 15.817 0 01-1.575-.68 24.077 24.077 0 01-1.688-.892",
  fill: "url(#jmeter_svg__paint26_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M158.066 87.361c.113.066.231.14.354.221.142.082.265.164.369.246.123.082.227.159.312.23.104.07.175.128.212.171l-4.07 2.35a3 3 0 00-.255-.213 6.196 6.196 0 00-.382-.27 3.952 3.952 0 00-.397-.262 6.95 6.95 0 00-.341-.213 8.616 8.616 0 00-1.759-.77 6.837 6.837 0 00-1.903-.293 7.332 7.332 0 00-2.032.273c-.692.192-1.413.504-2.163.937l-8.097 4.674-4.325-2.497 15.909-9.185 3.276 1.892-1.752 1.748.213.123c.635-.104 1.25-.176 1.847-.215.597-.039 1.17-.031 1.719.023a8.221 8.221 0 011.647.327 7.545 7.545 0 011.618.703",
  fill: "url(#jmeter_svg__paint27_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M163.828 90.645l-.962-.555 3.759-2.17-1.142-.66.806-.465 3.242 1.872-.805.465-1.139-.658-3.759 2.17",
  fill: "url(#jmeter_svg__paint28_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M167.497 92.762l2.582-2.644-.028-.016a34.129 34.129 0 01-.984.625 6.297 6.297 0 01-.221.135 10.51 10.51 0 01-.194.116l-2.123 1.225-.862-.497 4.565-2.636 1.313.758-2.508 2.583.019.011 4.536-1.411 1.314.758-4.565 2.635-.9-.52 2.161-1.247.184-.106a56.488 56.488 0 00.466-.254 58.665 58.665 0 00.808-.43l-.025-.015-4.652 1.442-.886-.512",
  fill: "url(#jmeter_svg__paint29_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M86.683 37.152l10.551-2.506.599.346-4.34 6.091-.781-.45 1.362-1.9-3.339-1.927-3.277.793-.775-.447zm9.994-1.835l-5.203 1.244 3.011 1.738 2.192-2.982z",
  fill: "url(#jmeter_svg__paint30_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M94.287 41.54l7.445-4.298 3.074 1.775c1.273.734 1.011 1.948-.146 2.615-1.214.701-3.214.867-4.487.132l-2.413-1.393-2.749 1.588-.724-.419zm4.124-1.544l2.375 1.37c.876.506 2.252.366 3.149-.152.936-.54 1.001-1.37.182-1.843l-2.311-1.335-3.395 1.96z",
  fill: "url(#jmeter_svg__paint31_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M99.15 44.35l10.551-2.506.598.345-4.34 6.091-.793-.458 1.362-1.9L103.195 44l-3.277.794-.768-.444zm9.994-1.836l-5.203 1.244 3.011 1.738 2.192-2.982z",
  fill: "url(#jmeter_svg__paint32_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M116.952 45.978c1.316.76 1.61 1.703 1.116 2.52l-.94-.134c.513-.842-.071-1.566-.871-2.028-1.777-1.026-4.34-.666-5.838.199-1.643.948-1.931 2.504-.357 3.413.838.484 2.238.81 3.671.521l.289.532c-1.591.366-3.527-.037-4.667-.695-2.035-1.175-1.518-3.131.365-4.219 1.789-1.032 4.995-1.4 7.232-.109z",
  fill: "url(#jmeter_svg__paint33_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M126.665 51.636l-7.445 4.299-.731-.422 3.483-2.01-4.397-2.539-3.483 2.01-.718-.414 7.445-4.298.719.414-3.312 1.912 4.397 2.539 3.312-1.912.73.421z",
  fill: "url(#jmeter_svg__paint34_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M126.529 59.403l-.651.375-4.983-2.876 7.446-4.299 4.888 2.823-.651.375-4.17-2.407-2.686 1.55 3.635 2.099-.62.358-3.635-2.099-2.837 1.639 4.264 2.462z",
  fill: "url(#jmeter_svg__paint35_linear)"
}));

var jmeter_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint18_linear",
  x1: 68.328,
  y1: 35.924,
  x2: 68.294,
  y2: 35.95,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint0_linear",
  x1: 92.265,
  y1: 29.28,
  x2: 86.205,
  y2: 23.416,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint2_linear",
  x1: 68.672,
  y1: 36.046,
  x2: 68.391,
  y2: 36.127,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint3_linear",
  x1: 74.137,
  y1: 29.848,
  x2: 69.168,
  y2: 22.974,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint4_linear",
  x1: 66.055,
  y1: 36.727,
  x2: 65.657,
  y2: 36.874,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint5_linear",
  x1: 66.055,
  y1: 36.727,
  x2: 65.657,
  y2: 36.874,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint6_linear",
  x1: 68.674,
  y1: 36.045,
  x2: 68.356,
  y2: 36.161,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint7_linear",
  x1: 68.674,
  y1: 36.045,
  x2: 68.356,
  y2: 36.161,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint8_linear",
  x1: 85.948,
  y1: 27.612,
  x2: 80.963,
  y2: 21.315,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint9_linear",
  x1: 58.855,
  y1: 38.331,
  x2: 51.788,
  y2: 31.3,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint10_linear",
  x1: 77.566,
  y1: 27.576,
  x2: 71.588,
  y2: 19.339,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint11_linear",
  x1: 57.701,
  y1: 32.262,
  x2: 54.059,
  y2: 26.864,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint12_linear",
  x1: 80.937,
  y1: 32.424,
  x2: 70.259,
  y2: 20.012,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint13_linear",
  x1: 88.366,
  y1: 31.404,
  x2: 79.506,
  y2: 22.665,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint14_linear",
  x1: 65.787,
  y1: 36.636,
  x2: 65.589,
  y2: 36.715,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint15_linear",
  x1: 65.787,
  y1: 36.636,
  x2: 65.589,
  y2: 36.715,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint16_linear",
  x1: 65.787,
  y1: 36.636,
  x2: 65.589,
  y2: 36.715,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint17_linear",
  x1: 68.328,
  y1: 35.924,
  x2: 68.294,
  y2: 35.95,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint1_linear",
  x1: 73.601,
  y1: 34.534,
  x2: 73.445,
  y2: 34.603,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint19_linear",
  x1: 68.328,
  y1: 35.924,
  x2: 68.294,
  y2: 35.95,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint20_linear",
  x1: 74.351,
  y1: 34.531,
  x2: 73.858,
  y2: 33.966,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint21_linear",
  x1: 75.205,
  y1: 35.025,
  x2: 74.41,
  y2: 34.267,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint22_linear",
  x1: 96.712,
  y1: 36.587,
  x2: 88.958,
  y2: 25.47,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint23_linear",
  x1: 118.475,
  y1: 51.434,
  x2: 96.419,
  y2: 29.22,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint24_linear",
  x1: 128.029,
  y1: 63.865,
  x2: 114.394,
  y2: 48.766,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint25_linear",
  x1: 143.153,
  y1: 67.879,
  x2: 134.281,
  y2: 56.229,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint26_linear",
  x1: 154.07,
  y1: 78.9,
  x2: 140.435,
  y2: 63.801,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint27_linear",
  x1: 164.749,
  y1: 85.17,
  x2: 156.314,
  y2: 74.455,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint28_linear",
  x1: 171.03,
  y1: 87.828,
  x2: 168.053,
  y2: 84.272,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint29_linear",
  x1: 176.414,
  y1: 90.936,
  x2: 171.575,
  y2: 86.062,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint30_linear",
  x1: 103.393,
  y1: 35.423,
  x2: 96.619,
  y2: 28.176,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint31_linear",
  x1: 109.442,
  y1: 38.914,
  x2: 104.614,
  y2: 33.136,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint32_linear",
  x1: 115.86,
  y1: 42.62,
  x2: 109.086,
  y2: 35.373,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint33_linear",
  x1: 122.322,
  y1: 46.262,
  x2: 116.106,
  y2: 39.37,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint34_linear",
  x1: 129.113,
  y1: 50.271,
  x2: 123.582,
  y2: 43.922,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "jmeter_svg__paint35_linear",
  x1: 135.766,
  y1: 54.111,
  x2: 131.269,
  y2: 48.619,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "jmeter_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "matrix(.86604 .49997 -.86604 .49997 71.09 19)",
  d: "M0 0h132.71v45.137H0z"
})));

function SvgJmeter(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", jmeter_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), jmeter_ref, jmeter_ref2);
}

/* harmony default export */ var jmeter = (SvgJmeter);
// CONCATENATED MODULE: ./public/assets/technologies/languages/linux.svg
function linux_extends() { linux_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return linux_extends.apply(this, arguments); }



var linux_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#linux_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M126.788 45.22c-.25.145-.503-.001-.503-.001l-.252-.146c-.253-.146-.002-.29-.004-.58 0 0-.002-.291.249-.436.25-.144.25-.144.503.001l.254.437c.002.29.004.58-.247.725zm-2.039-4.07c1.253-.723 1.5-1.448.742-1.886 0 0-.25.145-.503 0l-.501.288.757.438c-.501.289-.499.58-1 .869l.505.291zm10.089 4.378c.505.292.257.727-.242 1.306l.505.292c-.002-.29.248-.435.499-.58.25-.144.501-.289.499-.58-.002-.29-.004-.58-.257-.726 0 0-.503-.001-.755-.147-.251.145.002.29-.249.435zm-11.582-2.058c-.253-.146-.253-.146-.002-.29.25-.145.501-.29 1.004-.289a2.243 2.243 0 001.008.293c.252.146.002.29.002.29-.251.145-.754.144-1.76.142l-.252-.146zm-2.527-1.748c-.509-.872-.01-1.452 1.243-2.175.752-.434 1.253-.723 2.259-.721.753-.144 1.509.003 2.014.295.505.291.507.582.51 1.162-.499.58-.998 1.16-1.749 1.594l-.251.144-.251.145.253.146.251-.145c.252.146.753-.144 1.755-.722.752-.434 1.503-.868 1.75-1.593.247-.726.243-1.306-.767-1.89-.757-.437-2.016-.585-3.02-.297-1.507.288-2.36.663-3.612 1.386-1.002.579-1.651 1.361-1.643 2.523.503.001.755.147 1.258.148zm-3.766 38.612c.252.146.352.088.578-.042.551-.318.95-.84.919-1.697-.006-.871-.792-1.875-2.107-2.865-.227-.16-.479-.306-.479-.306-.203-.174-.203-.174-.455-.32-.228-.16-.683-.48-.885-.656 3.087-.907 5.394-1.948 7.198-2.989 2.506-1.446 3.754-2.75 4.248-4.201.493-1.45.235-2.468-.777-3.341-.503-.001-.503-.001-.754.143.762 1.018 1.022 2.326.276 3.63-.996 1.45-2.247 2.464-4.001 3.476-1.403.81-3.735 1.865-7.4 2.815-1.411-.351-3.348-.4-5.558.002-.226.13-.351.203-.099.349 0 0 .478.015 1.157-.085.931.046 1.609-.054 2.54-.009 1.183.192 1.914.353 2.671.79 1.262.729 2.349 1.559 2.756 2.199.684.77.838 1.265.69 1.642a9.762 9.762 0 00-.295 1.044c-.326.189-.223.421-.223.421zm13.1-34.38c-.002-.291.499-.58 1-.87 1.002-.578 1.503-.868 2.76-1.01 1.006.002 1.761.15 2.266.44.758.438.761 1.019.765 1.6-.499.58-.747 1.014-1.499 1.448-1.253.723-2.509.866-3.77.427 0 0 .002.29.255.436.505.292.507.582.761 1.018 1.756-.722 3.01-1.154 4.263-1.878 1.503-.867 2.253-1.592 2.499-2.317-.005-.871-.512-1.453-1.522-2.036-.758-.437-1.765-.73-3.024-.878-1.256.142-2.01.286-2.762.72-1.252.723-2.002 1.447-2.499 2.317.252.146.254.436.507.582zm-.979 4.062c-5.537-.593-9.064-1.472-11.084-2.638-1.767-1.02-2.783-2.474-3.045-4.072-.248.435-.497.87-.495 1.16l.012 1.743c.007 1.161.769 2.18 2.031 2.908 1.767 1.02 4.789 1.608 9.068 2.053l3.775.444c1.006.002 2.012.005 2.764-.43.25-.144.501-.289.248-.434.249-.435-.262-1.598-2.035-3.49-1.27-1.89-2.536-3.2-3.546-3.782-.758-.438-2.521-.877-5.291-1.319-2.516-.296-4.529-.3-5.531.278 0 0 .002.29-.246.726.262 1.597 1.025 2.906 2.54 3.78 2.02 1.166 5.547 2.045 11.334 2.494l-.501.289c.252.146.002.29.002.29zM84.283 82.585c-.874 1.67-.054 3.237 1.966 4.403.505.292 1.085.54 1.74.745.605.233 1.285.423 1.738.453.428.045.856.09 1.309.12.68.19.805.117 1.183.19l7.975.353c1.809.121 3.518.3 5.387.68 1.611.235 3.022.587 3.752.748.958.322 1.763.44 2.669.5.629-.071 1.357-.2 1.958-.548.727-.419.773-1.029.669-1.552a3.003 3.003 0 00-.663-1.366c-.33-.393-.434-.916-.515-1.743.147-.668.544-1.48 1.469-2.306l1.2-.984c.424-.537.926-.826.949-1.13.075-.044.1-.058-.153-.204-.252-.146-.983-.307-1.661-.207a63.023 63.023 0 00-2.918-.065c-.753.144-1.962-.033-2.892-.079-1.184-.191-2.167-.498-2.672-.79-2.019-1.166-2.478-2.067-2.158-3.126.297-.754.971-1.435 1.771-2.189-.079-.537-.106-.813-.611-1.104-1.262-.73-3.07-.269-5.701 1.25l-.827.477-2.906 1.678-2.23 1.287-1.077.622-.751.434c-.226.13-.98.274-1.756.722-1.03.303-1.911.812-2.908 1.388l-3.286 1.314-.043.024-.007.005zM49.007 60.68c2.008 1.552 3.98 3.533 5.922 5.939 1.953 2.4 3.245 3.81 3.927 4.204 1.767 1.02 4.008 1.418 6.72 1.25.74-.134 1.31-.464 1.967-.843 2.367-1.367 3.922-3.926 4.677-7.684l.563-2.308c.122-.479.42-1.146.841-2.03.447-.87.72-1.582.867-2.104.248-.522.294-1.16.188-1.887-.08-.712-.462-1.366-1.094-1.963-1.261-.496-2.344-.716-3.174-.645-.83.07-1.557.258-2.159.547-.602.26-1.104.375-1.532.33-.403-.059-.957-.234-1.663-.497a11.672 11.672 0 00-.707-.38l-.681-.393c-1.338-.772-2.398-1.21-3.128-1.343-1.358.055-2.513.343-3.39.85-.4.231-.976.68-1.726 1.346-.725.652-1.375 1.144-1.902 1.448-1.027.593-2.357 1.01-4.015 1.24-1.709.257-2.839.53-3.41.86-.72.707.263 2.064 2.917 4.059l-.008.004zm31.189-8.298c1.728-.998 4.087-1.835 7.276-2.597 3.164-.777 5.576-1.12 7.461-1.189.2-.173.074-.246-.128-.363l-.002-.29c-1.483.011-4.12.513-7.686 1.347-3.316.69-5.95 1.57-7.479 2.452-1.127.65-1.827 1.375-2.174 2.159-.27.797-.135 2.264.457 4.37l.948 2.544c.397 3.064.209 4.922-.793 5.5-.527.304-1.305.462-2.639.358-1.106.055-2.088-.165-2.67-.5-.05-.029-.125-.015-.25.058-.025.014-.274.45-.72 1.32-.368 1.436 1.202 3.153 4.232 4.902 5.554 3.206 12.101 4.382 19.893 3.674 1.252-.724 2.03-1.172 2.102-1.506l.927-.535c1.629-.94 3.109-1.503 4.416-1.674 1.306-.171 2.187-.097 2.944.34.505.292.835.684.964 1.193 2.182-.968 3.86-1.937 5.363-2.805 2.28-1.316 4.159-2.4 5.408-3.705 1.251-1.013 1.999-2.028 2.496-2.898l.74-2.176c.246-.726.745-1.305.992-2.03.75-.725 1.249-1.304 2.502-2.028.495-1.16 1.243-2.175 1.738-3.335.748-1.015 1.495-2.03 1.992-2.9l-4.025-.299c-4.279-.445-7.05-1.177-8.818-2.197-1.514-.875-2.526-1.748-2.281-2.764l-.263-1.598c-.751.434-2.006.867-3.513 1.154l-4.597.817c-2.461.605-3.841.965-4.669 1.355-.602.23-1.179.476-1.23.447l-5.65 1.076c-5.804.99-10.321 2.402-13.202 4.066-.577.332-1.127.709-1.628 1.114-.36-1.104.163-2.047 1.566-2.857zm-5.624 24.118c-3.282-1.895-6.248-3.098-8.89-3.613l.076-.043c-2.766.139-4.956-.23-6.925-1.366-1.238-.714-2.706-2.111-4.38-4.177-1.748-2.05-3.405-3.806-4.994-5.25-.144-.15-.513-.46-1.13-.95a53.673 53.673 0 01-1.601-1.32c-.418-.371-.853-.82-1.28-1.332-.432-.478-.68-.919-.745-1.318-.033-.384.152-.688.55-.918.4-.231.915-.43 1.543-.593.437-.066.89-.123 1.316-.166.453-.057.855-.114 1.206-.2a8.086 8.086 0 001.055-.201c.327-.072.653-.144.98-.274.3-.116.601-.231.852-.376.25-.144.901-.636 2.027-1.52 1.1-.868 1.826-1.404 2.177-1.606 1.102-.637 2.232-.997 3.414-1.038 1.181-.041 2.037.077 2.593.398l2.903 1.676c.227.13.706.263 1.537.395.578-.13 1.055-.23 1.456-.345.427-.1.703-.202.854-.23.2-.058.401-.116.577-.159.277-.043.603-.086.98-.1.049-.26.274-.507.674-.738.276-.16.527-.304.727-.362.902-.52 2.61-1.01 5.197-1.454l2.462-.402c2.085-.358 3.642-.616 5.049-.961 1.43-.33 3.389-.936 5.898-1.802 2.157-.78 4.87-1.238 8.14-1.376l4.147-.208c2.816-.11 4.928-.338 6.409-.64 1.482-.3 2.987-.878 3.989-1.457.502-.29 1.879-1.23 4.106-2.837a90.866 90.866 0 016.887-4.412c1.754-1.013 3.158-1.649 4.739-2.182a19.492 19.492 0 015.274-1.005c1.76-.141 3.772-.137 5.788.449 2.016.585 4.034 1.46 6.053 2.626.758.438 1.515.875 2.022 1.457.758.437 1.517 1.165 2.278 2.183.509.872 1.018 1.744 1.024 2.615.258 1.017-.237 2.178-.733 3.338-.998 1.16-1.996 2.319-3.748 3.622-1 .868-2.253 1.592-3.754 2.75-1.503.868-2.253 1.592-3.005 2.026-.499.58-1.501 1.158-2.501 2.027-.75.724-1.499 1.449-1.746 2.174-.497.87-.994 1.74-1.239 2.756-.495 1.16-.738 2.466-1.232 3.917-.233 2.758-1.221 5.37-2.967 7.543-1.243 2.176-3.743 4.493-7.225 6.503-1.729.998-3.66 1.822-5.793 2.47.505.292.557.553.458.901-.098.349-.597.928-1.522 1.754l-1.601 1.216c-.299.464-.573.913-.266 1.611.054.552.183 1.06.639 1.671.255.436.662 1.076.716 1.628.003.58-.27 1.03-.821 1.348a4.906 4.906 0 01-2.687.677c-1.006-.002-1.862-.092-2.845-.399-.755-.147-2.266-.44-4.487-.907-2.004-.3-4.166-.51-6.493-.624l-4.657-.227a33.468 33.468 0 01-4.882-.388 10.03 10.03 0 01-3.453-1.213l-1.567-1.136c-1.494-1.47-1.754-2.777-.983-4.096-3.553-2.613-6.595-4.647-8.615-5.813",
  fill: "url(#linux_svg__paint0_linear)"
}));

var linux_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "linux_svg__paint0_linear",
  x1: 203.75,
  y1: 40.06,
  x2: 140.702,
  y2: -29.567,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "linux_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "scale(1.22477 .70706) rotate(45 21 123.128)",
  d: "M0 0h77.545v90.257H0z"
})));

function SvgLinux(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", linux_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), linux_ref, linux_ref2);
}

/* harmony default export */ var linux = (SvgLinux);
// CONCATENATED MODULE: ./public/assets/technologies/languages/python.svg
function python_extends() { python_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return python_extends.apply(this, arguments); }



var python_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#python_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M75.594 51.545c5.17-2.985 13.581-2.985 18.751 0l28.979 16.73c2.35 1.356 6.174 1.356 8.523 0l24.036-13.877c7.05-4.07 7.05-10.691 0-14.76l-11.933-6.89c-7.049-4.07-18.52-4.07-25.569 0l-6.989 4.035 18.751 10.825c1.412.816 1.412 2.137 0 2.953-1.413.815-3.702.815-5.114 0L94.175 32.748c-7.05-4.07-18.52-4.07-25.57 0l-11.932 6.89c-7.05 4.069-7.05 10.69 0 14.76l6.989 4.035 11.932-6.888zm55.401-11.318c-1.412-.815-1.412-2.136 0-2.952 1.412-.815 3.702-.815 5.114 0 1.412.816 1.412 2.137 0 2.953-1.412.815-3.702.815-5.114 0z",
  fill: "url(#python_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M155.883 68.373l-6.989-4.034-11.933 6.888c-5.169 2.985-13.581 2.985-18.751 0l-28.979-16.73c-2.35-1.356-6.173-1.356-8.523 0L56.673 68.374c-7.05 4.07-7.05 10.692 0 14.762l11.932 6.888c7.05 4.07 18.52 4.07 25.57 0l6.989-4.034-18.751-10.825c-1.413-.816-1.413-2.137 0-2.953 1.412-.815 3.701-.815 5.114 0l30.854 17.812c7.049 4.07 18.52 4.07 25.569 0l11.933-6.888c7.05-4.07 7.05-10.692 0-14.762zM81.56 82.544c1.413.816 1.413 2.137 0 2.953-1.412.815-3.701.815-5.114 0-1.412-.816-1.412-2.137 0-2.953 1.413-.815 3.702-.815 5.114 0z",
  fill: "url(#python_svg__paint1_linear)"
}));

var python_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "python_svg__paint0_linear",
  x1: 189.277,
  y1: 35.63,
  x2: 125.233,
  y2: -29.738,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "python_svg__paint1_linear",
  x1: 189.277,
  y1: 64.366,
  x2: 125.233,
  y2: -1.002,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "python_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "scale(1.22477 .70706) rotate(45 24.607 112.524)",
  d: "M0 0h100.778v100.778H0z"
})));

function SvgPython(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", python_extends({
    width: 217,
    height: 132,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), python_ref, python_ref2);
}

/* harmony default export */ var python = (SvgPython);
// CONCATENATED MODULE: ./public/assets/technologies/languages/ruby.svg
function ruby_extends() { ruby_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return ruby_extends.apply(this, arguments); }



var ruby_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#ruby_svg__clip0)",
  fillRule: "evenodd",
  clipRule: "evenodd"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M95.437 75.154L65 45.563l4.504 38.315 66.924 2.951-40.99-11.675z",
  fill: "url(#ruby_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M69.55 83.941l39.372-3.293-20.518-8.97L69.55 83.94z",
  fill: "url(#ruby_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M69.55 83.986l3.28-24.127-7.745-14.168 4.464 38.295z",
  fill: "url(#ruby_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M65.077 45.726l34.203 6.027-4.913-13.263-29.29 7.236z",
  fill: "url(#ruby_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M88.29 71.677l37.629-5.546-25.77-15.871-11.858 21.417z",
  fill: "url(#ruby_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M125.389 85.573l21.225-15.003-23.395-4.177 2.17 19.18z",
  fill: "url(#ruby_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M154.182 78.554l-8.447-8.823 8.56-5.567-.113 14.39z",
  fill: "url(#ruby_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M82.895 35l11.677 3.696 13.911-2.99L82.895 35z",
  fill: "url(#ruby_svg__paint7_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M108.922 35.508l-14.781 3.008 5.08 13.072 24.025 14.924 23.173 4.212 8.111-6.632-7.309-11.275c-5.723-3.552-17.047-10.445-17.26-10.694-.217-.245-14.314-4.551-21.039-6.615z",
  fill: "url(#ruby_svg__paint8_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M135.476 46.07c13.223 7.7 21.035 17.624 14.378 21.433-6.656 3.807-22.833-.23-36.051-7.929-13.218-7.698-21.46-17.5-14.804-21.307 6.652-3.809 23.259.105 36.477 7.804z",
  fill: "url(#ruby_svg__paint9_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M65.09 45.725l34.312 5.98-11.052 19.86c-11.664-7.18-21.524-15.167-23.26-25.84z",
  fill: "url(#ruby_svg__paint10_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M123.314 66.537l-34.982 5.099c10.925 5.998 22.67 11.382 37.198 14.018l-2.216-19.117z",
  fill: "url(#ruby_svg__paint11_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M125.353 85.603c10.667 2.04 25.969 2.511 28.81-7.11l-7.553-7.895-21.257 15.005z",
  fill: "url(#ruby_svg__paint12_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M83.049 35c-17.605.282-17.868 7.615-18.022 10.738l29.187-7.214L83.049 35z",
  fill: "url(#ruby_svg__paint13_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M123.26 66.566c-4.908 4.61-14.764 13.9-14.945 14.087-.285.293 10.844 4.005 17.133 4.848l-2.188-18.935z",
  fill: "url(#ruby_svg__paint14_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M99.402 51.697l-26.717 7.995c4.44 4.727 9.851 8.428 15.647 11.818l11.07-19.813z",
  fill: "url(#ruby_svg__paint15_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M94.177 38.492L70.813 37.36c-5.056 2.138-5.496 5.078-5.102 8.162 9.62-2.231 28.856-6.689 28.466-7.029z",
  fill: "url(#ruby_svg__paint16_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M146.551 70.55l-3.861 15.882c6.222-.847 10.236-3.45 11.492-7.887l-7.631-7.994z",
  fill: "url(#ruby_svg__paint17_linear)"
}));

var ruby_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint9_linear",
  x1: 170.258,
  y1: 69.183,
  x2: 164.059,
  y2: 27.693,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint0_linear",
  x1: 159.517,
  y1: 87.121,
  x2: 151.567,
  y2: 33.885,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint2_linear",
  x1: 75.333,
  y1: 84.257,
  x2: 50.823,
  y2: 65.079,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint3_linear",
  x1: 110.336,
  y1: 51.847,
  x2: 108.6,
  y2: 34.53,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint4_linear",
  x1: 138.082,
  y1: 71.828,
  x2: 134.015,
  y2: 44.182,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint5_linear",
  x1: 154.177,
  y1: 85.709,
  x2: 149.047,
  y2: 61.5,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint6_linear",
  x1: 157.062,
  y1: 78.656,
  x2: 150.125,
  y2: 62.692,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint7_linear",
  x1: 116.755,
  y1: 38.722,
  x2: 116.573,
  y2: 33.855,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint8_linear",
  x1: 174.051,
  y1: 70.973,
  x2: 167.206,
  y2: 25.561,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint1_linear",
  x1: 121.649,
  y1: 84.028,
  x2: 120.356,
  y2: 67.959,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint10_linear",
  x1: 110.493,
  y1: 71.748,
  x2: 104.102,
  y2: 38.913,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint11_linear",
  x1: 137.554,
  y1: 85.79,
  x2: 134.263,
  y2: 61.015,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint12_linear",
  x1: 163.476,
  y1: 86.92,
  x2: 160.433,
  y2: 65.988,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint13_linear",
  x1: 103.648,
  y1: 45.815,
  x2: 102.314,
  y2: 31.779,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint14_linear",
  x1: 130.988,
  y1: 85.635,
  x2: 124.395,
  y2: 62.546,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint15_linear",
  x1: 108.038,
  y1: 71.651,
  x2: 103.207,
  y2: 46.447,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint16_linear",
  x1: 103.422,
  y1: 45.579,
  x2: 102.632,
  y2: 34.873,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "ruby_svg__paint17_linear",
  x1: 157.896,
  y1: 86.545,
  x2: 151.261,
  y2: 67.97,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "ruby_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "rotate(90 59.765 94.765)",
  d: "M0 0h51.83v89.53H0z"
})));

function SvgRuby(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", ruby_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), ruby_ref, ruby_ref2);
}

/* harmony default export */ var ruby = (SvgRuby);
// CONCATENATED MODULE: ./public/assets/technologies/languages/spark.svg
function spark_extends() { spark_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return spark_extends.apply(this, arguments); }



var spark_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#spark_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M149.165 81.594c.095-.151.147-.225.188-.3 1.091-2.027 2.187-4.052 3.262-6.082.108-.204.26-.274.626-.29 4.171-.189 8.342-.388 12.512-.584a1.33 1.33 0 00.458-.087c-.707-.239-1.411-.478-2.119-.716-2.94-.985-5.879-1.972-8.825-2.952-.274-.091-.309-.185-.237-.35.886-2.036 1.761-4.072 2.638-6.109.045-.105.083-.212.033-.352-.694.278-1.39.554-2.082.834-2.445.985-4.889 1.97-7.333 2.957-.264.106-.543.203-.787.322-.233.113-.422.095-.677.019-3.599-1.084-7.203-2.162-10.807-3.24a2.033 2.033 0 00-.571-.094l4.065 5.446c-.149-.01-.253-.012-.353-.024-2.306-.285-4.61-.573-6.916-.853-.276-.033-.424-.1-.527-.258-.918-1.4-1.857-2.795-2.782-4.193-.416-.628-.646-1.276-.528-1.952.267-1.528 2.16-2.35 4.758-2.018.851.108 1.688.304 2.468.533 3.556 1.043 7.092 2.11 10.629 3.176.296.09.502.091.788-.026 2.436-.992 4.884-1.976 7.332-2.958 1.31-.524 2.554-1.12 4.11-1.395a12.943 12.943 0 011.852-.203c2.254-.068 3.791.766 3.79 2.07 0 .44-.122.89-.301 1.32a295.6 295.6 0 01-2.435 5.661c-.098.223-.017.326.321.439 3.806 1.267 7.602 2.544 11.4 3.819.783.263 1.47.59 1.987 1.026 1.124.949.675 1.907-1.132 2.41-.823.228-1.706.313-2.606.355-4.563.215-9.125.433-13.688.64-.373.016-.522.098-.628.296-1.112 2.09-2.241 4.178-3.363 6.267-.269.5-.629.971-1.274 1.351-1.467.864-3.762.94-5.449.215-.945-.406-1.534-.95-2.067-1.533-1.3-1.422-2.612-2.84-3.93-4.257-.122-.131-.121-.21.037-.336 1.111-.892 2.2-1.793 3.297-2.691.03-.025.081-.041.167-.085l4.699 4.782",
  fill: "url(#spark_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M126.152 90.643c-1.694-.982-3.383-1.966-5.087-2.941-.224-.13-.286-.24-.22-.422 1.054-2.928 2.092-5.857 3.134-8.787.033-.093.058-.186.108-.347l-11.052 4.898-4.443-2.565c.485-.22.95-.436 1.421-.645l11.252-4.989c3.584-1.589 7.167-3.178 10.757-4.763a.918.918 0 01.416-.065c2.527.307 5.05.621 7.574.936.037.004.07.021.171.054l-11.957 5.303.017.053 15.342-.459-.921.754c-.834.681-1.677 1.359-2.498 2.046-.165.138-.344.18-.62.183-3.187.043-6.374.092-9.56.14-.139.002-.278.007-.44.011-.03.075-.064.143-.084.213-1.084 3.716-2.167 7.432-3.247 11.15-.02.066-.003.138-.004.207l-.059.035",
  fill: "url(#spark_svg__paint1_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M83.26 54.595c.279-.24.74-.561 1.06-.924 1.555-1.756.46-3.844-2.547-4.924-3.298-1.184-7.314-1.073-10.323.345-2.225 1.048-3.085 2.8-1.794 4.389 1.04 1.28 2.854 2.043 5.183 2.379 3.089.445 5.856.056 8.421-1.265zm-20.288-1.85c-1.12.494-2.178.96-3.233 1.427-1.402.622-2.804 1.243-4.2 1.87-.163.072-.27.084-.425-.006-1.215-.708-2.437-1.412-3.655-2.117-.028-.016-.04-.041-.088-.09.659-.295 1.317-.593 1.98-.887 2.325-1.031 4.65-2.063 6.979-3.092 2.662-1.176 5.271-2.397 8.007-3.514 4.843-1.977 11.91-2.011 16.91-.15 2.9 1.08 4.943 2.535 5.682 4.512.736 1.971-.016 3.78-2.19 5.348-3.072 2.217-7.212 3.14-12.038 3.094-3.168-.03-6.086-.571-8.643-1.661-2.636-1.123-4.406-2.586-4.987-4.481-.022-.07-.054-.139-.099-.253",
  fill: "url(#spark_svg__paint2_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M80.631 40.253l-7.004-.59c.124-.319.269-.623.358-.933.23-.796.116-1.555-.85-2.197-.803-.533-1.827-.795-3.052-.767-1.096.026-1.975.454-2.128 1.11-.167.72-.248 1.45-.3 2.178-.085 1.207-.083 2.415-.18 3.622-.089 1.097-.483 2.158-1.62 3.072-1.351 1.087-3.213 1.79-5.399 2.169-4.043.701-7.796.297-11.213-1.125-1.508-.628-2.761-1.4-3.655-2.346-1.185-1.256-1.253-2.569-.448-3.902.285-.471.662-.924 1.011-1.404l6.683 1.167c-.07.099-.135.177-.182.259-.249.429-.572.85-.722 1.29-.444 1.305.612 2.39 2.77 2.863a9.96 9.96 0 001.757.215c1.812.062 3.178-.52 3.54-1.548.209-.592.251-1.205.306-1.81.133-1.448.175-2.9.359-4.345.126-.996.663-1.933 1.732-2.748 1.166-.89 2.715-1.472 4.559-1.724 4.585-.625 8.603-.008 11.846 2.005 1.848 1.147 2.478 2.548 2.172 4.093-.09.458-.22.912-.34 1.396",
  fill: "url(#spark_svg__paint3_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M97.917 67.8c-1.93.857-3.763 1.673-5.603 2.482a.754.754 0 01-.372.047c-4.847-1.026-8.994-3.667-8.622-7.022.2-1.803 1.557-3.295 3.842-4.508 5.289-2.808 13.926-2.939 19.575-.393 3.3 1.487 5.083 3.434 4.789 5.902-.201 1.68-1.432 3.116-3.576 4.246-1.416.747-2.977 1.403-4.494 2.085-2.148.965-4.317 1.915-6.477 2.871-.077.034-.158.065-.26.106l-3.962-2.287c.492-.223.97-.443 1.453-.658 2.492-1.107 5.008-2.195 7.468-3.325 1.532-.704 2.798-1.549 3.444-2.647.685-1.167.28-2.228-1.037-3.17-2.728-1.947-7.753-2.398-11.408-1.039-2.414.898-3.638 2.386-3.11 3.904.513 1.48 2.158 2.433 4.498 3.017 1.187.297 2.444.407 3.852.39",
  fill: "url(#spark_svg__paint4_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M126.762 70.411l-4.653 2.061c-.837-.483-1.654-.96-2.482-1.43-.67-.382-1.708-.484-2.529-.24-.322.095-.611.229-.9.356a15044.78 15044.78 0 00-10.716 4.75l-2.711 1.207-4.124-2.38c.684-.307 1.344-.604 2.007-.898 2.315-1.027 4.629-2.053 6.946-3.077 2.005-.886 3.988-1.79 6.028-2.648 2.734-1.151 7.184-1.006 9.671.31 1.187.628 2.287 1.31 3.463 1.99",
  fill: "url(#spark_svg__paint5_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M131.04 93.434l1.742-1.006-.01-.005-2.425.611-.218-.126 1.059-1.4-.01-.006-1.743 1.006-.344-.199 2.087-1.204.531.306-.963 1.275 2.199-.562.526.304-2.087 1.205-.344-.199zm-1.219-2.79l-1.808 1.043-.344-.199 1.808-1.043-.653-.377.279-.162 1.65.953-.279.161-.653-.377",
  fill: "url(#spark_svg__paint6_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M89.086 41.75l1.023.59 1.308-1.034-2.33.444zm.182 1.26l-1.701-.981-1.6.3-1.205-.696 7.627-1.335 1.161.67-4.064 3.392-1.117-.645.9-.704",
  fill: "url(#spark_svg__paint7_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M98.263 44.845l-.606-.35-1.35.545.605.35c.365.21.898.24 1.351-.022.3-.173.27-.367 0-.523zm-.466-1.425l1.592.92c.832.48.919 1.103.064 1.597-1.074.62-2.557.6-3.637-.024l-.686-.397-2.142.865-1.036-.598 5.845-2.363",
  fill: "url(#spark_svg__paint8_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M101.418 48.869l1.022.59 1.309-1.033-2.331.443zm.182 1.26l-1.701-.981-1.6.3-1.205-.696 7.626-1.335 1.161.67-4.063 3.392-1.117-.645.899-.704",
  fill: "url(#spark_svg__paint9_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M107.441 54.523c-.511-.11-.993-.277-1.38-.5-1.314-.76-1.152-1.806.083-2.519 1.578-.91 4.237-.912 5.814-.001.394.227.657.489.788.75l-1.337.604c.022-.266-.16-.565-.598-.818-.905-.523-2.527-.514-3.477.035-.723.417-.834 1.004-.118 1.417.438.253 1.029.4 1.548.43l-1.323.602",
  fill: "url(#spark_svg__paint10_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M115.604 57.127l-2.161-1.248-2.354.954-1.036-.599 5.845-2.363 1.037.599-2.316.933 2.161 1.247 2.316-.933 1.036.599-5.844 2.363-1.037-.599 2.353-.953z",
  fill: "url(#spark_svg__paint11_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M117.404 60.477l5.844-2.364 2.753 1.59-1.176.476-1.716-.99-1.14.456 1.577.91-1.176.477-1.577-.91-1.177.476 1.716.99-1.176.477-2.752-1.588",
  fill: "url(#spark_svg__paint12_linear)"
}));

var spark_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint7_linear",
  x1: 87.913,
  y1: 45.306,
  x2: 86.115,
  y2: 40.161,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint0_linear",
  x1: 143.219,
  y1: 94.814,
  x2: 131.58,
  y2: 56.796,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint2_linear",
  x1: 64.441,
  y1: 70.321,
  x2: 56.142,
  y2: 47.642,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint3_linear",
  x1: 51.089,
  y1: 57.457,
  x2: 43.08,
  y2: 37.255,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint4_linear",
  x1: 91.563,
  y1: 78.179,
  x2: 84.634,
  y2: 57.737,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint5_linear",
  x1: 103.273,
  y1: 84.056,
  x2: 98.339,
  y2: 72.502,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint6_linear",
  x1: 130.74,
  y1: 94.04,
  x2: 130.019,
  y2: 89.848,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint1_linear",
  x1: 118.511,
  y1: 95.197,
  x2: 110.838,
  y2: 75.704,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint8_linear",
  x1: 94.257,
  y1: 48.968,
  x2: 92.613,
  y2: 44.679,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint9_linear",
  x1: 100.245,
  y1: 52.425,
  x2: 98.446,
  y2: 47.28,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint10_linear",
  x1: 106.241,
  y1: 56.041,
  x2: 104.557,
  y2: 51.674,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint11_linear",
  x1: 113.593,
  y1: 60.133,
  x2: 111.743,
  y2: 54.605,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "spark_svg__paint12_linear",
  x1: 119.451,
  y1: 63.513,
  x2: 117.866,
  y2: 59.49,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#F1F1F1"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff",
  stopOpacity: 0.4
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "spark_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  transform: "scale(1.22477 .70706) rotate(45 7.889 96.305)",
  d: "M0 0h111.428v58.004H0z"
})));

function SvgSpark(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", spark_extends({
    width: 217,
    height: 134,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), spark_ref, spark_ref2);
}

/* harmony default export */ var spark = (SvgSpark);
// EXTERNAL MODULE: ./components/IdIcon/IdIcon.module.scss
var IdIcon_module = __webpack_require__("pq5z");
var IdIcon_module_default = /*#__PURE__*/__webpack_require__.n(IdIcon_module);

// CONCATENATED MODULE: ./components/IdIcon/IdIcon.js
var __jsx = external_react_default.a.createElement;

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function IdIcon_extends() { IdIcon_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return IdIcon_extends.apply(this, arguments); }






























const WorkflowIconList = {
  team: __jsx(team, {
    className: IdIcon_module_default.a.workflow
  }),
  validated: __jsx(validated, {
    className: IdIcon_module_default.a.workflow
  }),
  complete: __jsx(complete, {
    className: IdIcon_module_default.a.workflow
  }),
  tools: __jsx(tools, {
    className: IdIcon_module_default.a.workflow
  }),
  ['customer-control']: __jsx(validated, {
    className: IdIcon_module_default.a.workflow
  }),
  ['changes']: __jsx(changes, {
    className: IdIcon_module_default.a.workflow
  }),
  ['tasks']: __jsx(tasks, {
    className: IdIcon_module_default.a.workflow
  }),
  ['targets']: __jsx(targets, {
    className: IdIcon_module_default.a.workflow
  }),
  ['changes-gear']: __jsx(changes_gear, {
    className: IdIcon_module_default.a.workflow
  })
};
const TechnologiesIconList = {
  ['web-frontend']: __jsx(front, null),
  ['web-backend']: __jsx(back, null),
  mobile: __jsx(mobile, null),
  ['dev-ops']: __jsx(devops, null),
  ['machine-learning']: __jsx(ai, null),
  qa: __jsx(qa, null),
  ['big-data']: __jsx(bigdata, null)
}; // TODO add more icons

const ProgrammingLanguagesIconList = {
  react: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  ember: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  vue: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  svetle: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  angular: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  android: __jsx(android, {
    className: IdIcon_module_default.a.langIcon
  }),
  ios: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  java: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  ['c-sharp']: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  swift: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  python: __jsx(python, {
    className: IdIcon_module_default.a.langIcon
  }),
  ['c-plus-plus']: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  hp: __jsx(hp, {
    className: IdIcon_module_default.a.langIcon
  }),
  jira: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  jmeter: __jsx(jmeter, {
    className: IdIcon_module_default.a.langIcon
  }),
  qa: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  selenium: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  ruby: __jsx(ruby, {
    className: IdIcon_module_default.a.langIcon
  }),
  django: __jsx(django, {
    className: IdIcon_module_default.a.langIcon
  }),
  php: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  laravel: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  ['amazon-aws']: __jsx(amazon_aws, {
    className: IdIcon_module_default.a.langIcon
  }),
  chef: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  linux: __jsx(linux, {
    className: IdIcon_module_default.a.langIcon
  }),
  puppet: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  ansible: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  spark: __jsx(spark, {
    className: IdIcon_module_default.a.langIcon
  }),
  kafka: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  }),
  hadoop: __jsx(java, {
    className: IdIcon_module_default.a.langIcon
  })
};
const proxyHandler = {
  get(target, prop) {
    if (prop in target) {
      return target[prop];
    } else {
      return null;
    }
  }

};

function IdIcon({
  IconList = {},
  name = ''
}) {
  const value = name.toLowerCase();
  return IconList[value];
}

function WorkflowIcon(props) {
  const IconList = new Proxy(WorkflowIconList, proxyHandler);
  return __jsx(IdIcon, IdIcon_extends({
    IconList: IconList
  }, props));
}
function TechnologiesIcon(props) {
  const IconList = new Proxy(TechnologiesIconList, proxyHandler);
  return __jsx(IdIcon, IdIcon_extends({
    IconList: IconList
  }, props));
}
function ProgrammingLanguagesIcon(_ref) {
  let {
    isSelected = false
  } = _ref,
      props = _objectWithoutProperties(_ref, ["isSelected"]);

  const IconList = new Proxy(ProgrammingLanguagesIconList, proxyHandler);
  return __jsx("div", {
    className: IdIcon_module_default.a.programmingLanguage
  }, __jsx("img", {
    className: IdIcon_module_default.a.hexagonItem,
    src: "/assets/technologies/languages/hexagon.svg",
    alt: "Hexagon"
  }), __jsx("div", {
    className: IdIcon_module_default.a.langContainer
  }, __jsx("img", {
    className: external_classnames_default()(IdIcon_module_default.a.hexagonBg, {
      [IdIcon_module_default.a.hexagonBgActive]: isSelected
    }),
    src: "/assets/technologies/languages/hexagon-bg.svg",
    alt: "Hexagon background"
  }), __jsx(IdIcon, IdIcon_extends({
    IconList: IconList
  }, props))));
}
/* harmony default export */ var IdIcon_IdIcon = (IdIcon);

/***/ }),

/***/ "Y0jz":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__("K2gz");
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// EXTERNAL MODULE: ./routes/Technologies/tecnologiesData.json
var tecnologiesData = __webpack_require__("jnZq");

// EXTERNAL MODULE: ./components/Page/Page.js + 9 modules
var Page = __webpack_require__("YHOl");

// EXTERNAL MODULE: ./components/Form/Form.js + 1 modules
var Form = __webpack_require__("hK8T");

// EXTERNAL MODULE: ./components/Button/Button.js
var Button = __webpack_require__("8wsC");

// EXTERNAL MODULE: ./components/IdIcon/IdIcon.js + 25 modules
var IdIcon = __webpack_require__("PjcW");

// EXTERNAL MODULE: ./components/Footer/Footer.js
var Footer = __webpack_require__("HXcA");

// EXTERNAL MODULE: ./routes/Technologies/Technologies.module.scss
var Technologies_module = __webpack_require__("JSqF");
var Technologies_module_default = /*#__PURE__*/__webpack_require__.n(Technologies_module);

// CONCATENATED MODULE: ./routes/Technologies/Technologies.js

var __jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











const defaultTechnologyName = 'web-frontend';
class Technologies_Technologies extends external_react_["Component"] {
  static async getInitialProps({
    query
  }) {
    console.log('1', query);
    return _objectSpread(_objectSpread({}, {
      query: {
        technologyName: defaultTechnologyName
      }
    }), {}, {
      query
    });
  }

  constructor(props) {
    super(props);

    _defineProperty(this, "changeLang", value => {
      this.setState({
        currentLang: value
      });
    });

    const {
      technologyName
    } = props.query;
    this.state = {
      currentLang: tecnologiesData.technologiesTypes[technologyName || defaultTechnologyName].defaultValue
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      technologyName
    } = prevProps.query;
    const {
      technologyName: newTechnologyName
    } = this.props.query;
    if (technologyName !== newTechnologyName) this.setState({
      currentLang: tecnologiesData.technologiesTypes[newTechnologyName].defaultValue
    });
  }

  render() {
    const {
      query: {
        technologyName
      }
    } = this.props;
    const {
      currentLang
    } = this.state;
    const technologyData = tecnologiesData.technologiesTypes[technologyName || defaultTechnologyName];
    const programmingLanguageData = tecnologiesData.programmingLanguagesList[currentLang];
    const programmingLanguagesList = technologyData.list;
    return __jsx(Page["a" /* default */], null, __jsx(head_default.a, null, __jsx("title", null, "Hivex - ", technologyData.title)), __jsx("div", {
      className: Technologies_module_default.a.container
    }, __jsx("article", {
      className: Technologies_module_default.a.article
    }, __jsx("div", {
      className: Technologies_module_default.a.titleBlock
    }, __jsx("h1", {
      className: Technologies_module_default.a.titleBlockTitle
    }, technologyData.title), __jsx("p", {
      className: Technologies_module_default.a.titleBlockDescription
    }, technologyData.description)), __jsx("div", {
      className: Technologies_module_default.a.titleImage
    }, __jsx(IdIcon["b" /* TechnologiesIcon */], {
      name: technologyName
    }))), __jsx("div", {
      className: Technologies_module_default.a.picker
    }, __jsx("div", {
      className: Technologies_module_default.a.listContainer
    }, __jsx("ul", {
      className: Technologies_module_default.a.visualList
    }, programmingLanguagesList.map(item => {
      const isSelected = currentLang === item.key;
      return __jsx("li", {
        key: item.key,
        className: external_classnames_default()(Technologies_module_default.a.visualListItem, {
          [Technologies_module_default.a.visualListItemActive]: isSelected
        }),
        onClick: () => this.changeLang(item.key)
      }, __jsx(IdIcon["a" /* ProgrammingLanguagesIcon */], {
        isSelected: isSelected,
        name: item.key
      }));
    })), __jsx("ul", {
      className: Technologies_module_default.a.list
    }, programmingLanguagesList.map(item => __jsx("li", {
      key: item.key,
      className: external_classnames_default()(Technologies_module_default.a.listItem, {
        [Technologies_module_default.a.listItemActive]: currentLang === item.key
      })
    }, __jsx("span", {
      onClick: () => this.changeLang(item.key),
      className: Technologies_module_default.a.listItemValue
    }, item.title), __jsx("span", {
      className: Technologies_module_default.a.listItemLine
    }))))), __jsx("article", {
      className: Technologies_module_default.a.languageInfo
    }, __jsx("h2", {
      className: Technologies_module_default.a.languageInfoTitle
    }, programmingLanguageData.title), __jsx("p", {
      className: Technologies_module_default.a.languageInfoDescription
    }, programmingLanguageData.description), __jsx(Button["a" /* default */], {
      className: Technologies_module_default.a.startWorkingWithUs
    }, "Start working with us"))), __jsx(Form["a" /* default */], {
      formClassName: Technologies_module_default.a.form
    }), __jsx(Footer["a" /* default */], null)));
  }

}
// CONCATENATED MODULE: ./pages/technologies.js

/* harmony default export */ var technologies = __webpack_exports__["default"] = (Technologies_Technologies);

/***/ }),

/***/ "YHOl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ Page; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__("K2gz");
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// EXTERNAL MODULE: ./components/CookiesConfirmation/CookiesConfirmation.module.scss
var CookiesConfirmation_module = __webpack_require__("Lueu");
var CookiesConfirmation_module_default = /*#__PURE__*/__webpack_require__.n(CookiesConfirmation_module);

// CONCATENATED MODULE: ./components/CookiesConfirmation/CookiesConfirmation.js
var __jsx = external_react_default.a.createElement;



const isCookiesAgreed = 'isCookiesAgreed';

function CookiesConfirmation(props) {
  const {
    0: isCookiesShowed,
    1: setIsCookiesShowed
  } = Object(external_react_["useState"])(false);
  Object(external_react_["useEffect"])(() => {
    const storageAgreementValue = JSON.parse(localStorage.getItem(isCookiesAgreed));

    if (storageAgreementValue === null || storageAgreementValue === false) {
      setIsCookiesShowed(true);
    }
  }, []);

  function confirmCookies() {
    // ToDo: add actual cookies confirmation
    localStorage.setItem(isCookiesAgreed, true);
    setIsCookiesShowed(false);
  }

  return __jsx("div", {
    className: external_classnames_default()(CookiesConfirmation_module_default.a.container, {
      [CookiesConfirmation_module_default.a.hidden]: !isCookiesShowed
    })
  }, __jsx("div", {
    className: CookiesConfirmation_module_default.a.info
  }, __jsx("span", {
    className: CookiesConfirmation_module_default.a.title
  }, "Cookies"), __jsx("p", {
    className: CookiesConfirmation_module_default.a.description
  }, "By continuing to use the site hivex.group you agree to the use of cookies.", __jsx("br", null), "More information can be found in the Cookie Policy")), __jsx("button", {
    className: CookiesConfirmation_module_default.a.agreeBtn,
    onClick: confirmCookies
  }, "Agree"));
}

/* harmony default export */ var CookiesConfirmation_CookiesConfirmation = (CookiesConfirmation);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");

// EXTERNAL MODULE: external "react-transition-group"
var external_react_transition_group_ = __webpack_require__("CSOn");

// EXTERNAL MODULE: ./routes.js
var routes = __webpack_require__("8cHP");

// EXTERNAL MODULE: ./components/NavLink/NavLink.module.scss
var NavLink_module = __webpack_require__("kPE/");
var NavLink_module_default = /*#__PURE__*/__webpack_require__.n(NavLink_module);

// CONCATENATED MODULE: ./components/NavLink/NavLink.js
var NavLink_jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }






function NavLink(props) {
  const {
    route,
    children,
    activeClassname = NavLink_module_default.a.NavLinkActive
  } = props,
        rest = _objectWithoutProperties(props, ["route", "children", "activeClassname"]);

  const router = Object(router_["useRouter"])();
  const isRouteActive = route === undefined ? false : router.asPath === route;
  return NavLink_jsx(routes["Link"], _extends({
    route: route
  }, rest), NavLink_jsx("a", {
    className: external_classnames_default()(NavLink_module_default.a.NavLink, {
      [NavLink_module_default.a.NavLinkActive]: isRouteActive
    })
  }, children));
}
// CONCATENATED MODULE: ./public/assets/dropdown-arrow.svg
function dropdown_arrow_extends() { dropdown_arrow_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return dropdown_arrow_extends.apply(this, arguments); }



var _ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M1 1l4 4 4-4",
  stroke: "#2B2B2B",
  strokeLinecap: "round",
  strokeLinejoin: "round"
});

function SvgDropdownArrow(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", dropdown_arrow_extends({
    width: 10,
    height: 6,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _ref);
}

/* harmony default export */ var dropdown_arrow = (SvgDropdownArrow);
// EXTERNAL MODULE: ./components/Dropdown/Dropdown.module.scss
var Dropdown_module = __webpack_require__("jY/B");
var Dropdown_module_default = /*#__PURE__*/__webpack_require__.n(Dropdown_module);

// CONCATENATED MODULE: ./components/Dropdown/Dropdown.js
var Dropdown_jsx = external_react_default.a.createElement;







function Dropdown(props) {
  const {
    name,
    subroutes,
    className = ''
  } = props;
  const {
    0: isOpen,
    1: setIsOpen
  } = Object(external_react_["useState"])(false);

  function toggleIsOpen() {
    setIsOpen(prevState => !prevState);
  }

  return Dropdown_jsx("div", {
    className: external_classnames_default()(Dropdown_module_default.a.container, className)
  }, Dropdown_jsx("div", {
    className: external_classnames_default()(Dropdown_module_default.a.name, {
      [Dropdown_module_default.a.dropdownNameShowed]: isOpen
    }),
    onClick: toggleIsOpen
  }, Dropdown_jsx("span", {
    className: Dropdown_module_default.a.title
  }, name), Dropdown_jsx(dropdown_arrow, {
    className: Dropdown_module_default.a.arrow
  })), Dropdown_jsx(external_react_transition_group_["CSSTransition"], {
    className: Dropdown_module_default.a.subroutesContainer,
    in: isOpen,
    classNames: {
      enterDone: Dropdown_module_default.a.dropdownOpen,
      exitDone: Dropdown_module_default.a.dropdownClosed
    },
    timeout: 200
  }, Dropdown_jsx("div", null, Dropdown_jsx("ul", {
    className: Dropdown_module_default.a.dropdownSubroutes,
    onMouseLeave: toggleIsOpen
  }, subroutes.map(subroute => Dropdown_jsx("li", {
    className: Dropdown_module_default.a.dropdownSubroutesItem,
    key: subroute.route
  }, Dropdown_jsx(NavLink, {
    route: subroute.route
  }, Dropdown_jsx("a", null, subroute.name))))))));
}

/* harmony default export */ var Dropdown_Dropdown = (Dropdown);
// EXTERNAL MODULE: ./components/SocialLinks/SocialLinks.js + 3 modules
var SocialLinks = __webpack_require__("E1mO");

// EXTERNAL MODULE: ./components/Button/Button.js
var Button = __webpack_require__("8wsC");

// EXTERNAL MODULE: ./constants/mainConstants.js
var mainConstants = __webpack_require__("cMTN");

// CONCATENATED MODULE: ./public/assets/menu-open.svg
function menu_open_extends() { menu_open_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return menu_open_extends.apply(this, arguments); }



var menu_open_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M23 1H0M23 13H0",
  stroke: "#2B2B2B",
  strokeWidth: 2
});

var _ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M23 7H4.93",
  stroke: "#2B2B2B",
  strokeWidth: 3
});

function SvgMenuOpen(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", menu_open_extends({
    width: 23,
    height: 14,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), menu_open_ref, _ref2);
}

/* harmony default export */ var menu_open = (SvgMenuOpen);
// CONCATENATED MODULE: ./public/assets/menu-close.svg
function menu_close_extends() { menu_close_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return menu_close_extends.apply(this, arguments); }



var menu_close_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M13 13l16 16M13 29l16-16",
  stroke: "#2A2A2A",
  strokeWidth: 3
});

var menu_close_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M19.25 1.588a3.5 3.5 0 013.5 0l14.187 8.19a3.5 3.5 0 011.75 3.031v16.382a3.5 3.5 0 01-1.75 3.03L22.75 40.413a3.5 3.5 0 01-3.5 0l-14.187-8.19a3.5 3.5 0 01-1.75-3.031V12.809a3.5 3.5 0 011.75-3.03L19.25 1.587z",
  stroke: "#2B2B2B"
});

function SvgMenuClose(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", menu_close_extends({
    width: 42,
    height: 42,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), menu_close_ref, menu_close_ref2);
}

/* harmony default export */ var menu_close = (SvgMenuClose);
// EXTERNAL MODULE: ./components/MobileMenu/MobileMenu.module.scss
var MobileMenu_module = __webpack_require__("yxEx");
var MobileMenu_module_default = /*#__PURE__*/__webpack_require__.n(MobileMenu_module);

// CONCATENATED MODULE: ./components/MobileMenu/MobileMenu.js
var MobileMenu_jsx = external_react_default.a.createElement;












function MobileMenu(props) {
  const {
    setIsMenuOpen,
    isMenuOpen
  } = props;

  function toggleMenu() {
    setIsMenuOpen(prevState => !prevState);
  }

  function moveTo(path) {
    fullpage_api.moveTo(path);
    setIsMenuOpen(false);
  }

  return MobileMenu_jsx(external_react_default.a.Fragment, null, MobileMenu_jsx("div", {
    className: external_classnames_default()(MobileMenu_module_default.a.control, MobileMenu_module_default.a.mobileWrap)
  }, MobileMenu_jsx("button", {
    className: MobileMenu_module_default.a.toggleMenuContainer
  }, isMenuOpen ? MobileMenu_jsx(menu_close, {
    onClick: toggleMenu
  }) : MobileMenu_jsx(menu_open, {
    onClick: toggleMenu
  }))), MobileMenu_jsx(external_react_transition_group_["CSSTransition"], {
    className: external_classnames_default()(MobileMenu_module_default.a.container, MobileMenu_module_default.a.mobileWrap),
    classNames: {
      enterDone: MobileMenu_module_default.a.menuOpen,
      exitDone: MobileMenu_module_default.a.menuClosed
    },
    in: isMenuOpen,
    timeout: 0
  }, MobileMenu_jsx("div", null, MobileMenu_jsx("nav", {
    className: MobileMenu_module_default.a.nav
  }, MobileMenu_jsx("ul", {
    className: MobileMenu_module_default.a.navIn
  }, MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(NavLink, {
    route: "/"
  }, MobileMenu_jsx("a", null, "Home"))), MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(NavLink, {
    route: "/work-with-us"
  }, MobileMenu_jsx("a", null, "Cooperation with us"))), MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(Dropdown_Dropdown, {
    name: "Technologies",
    className: MobileMenu_module_default.a.dropdown,
    subroutes: mainConstants["b" /* TechnologiesSubroutes */]
  })), MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(NavLink, {
    route: "/company"
  }, MobileMenu_jsx("a", null, "Company"))))), MobileMenu_jsx(SocialLinks["a" /* default */], null), MobileMenu_jsx(Button["a" /* default */], {
    className: MobileMenu_module_default.a.btn,
    onClick: () => moveTo('contactUs')
  }, "Start work with us"))), MobileMenu_jsx(external_react_transition_group_["CSSTransition"], {
    className: external_classnames_default()(MobileMenu_module_default.a.grayWrapper, MobileMenu_module_default.a.mobileWrap),
    classNames: {
      enterDone: MobileMenu_module_default.a.wrapperOpen,
      exitDone: MobileMenu_module_default.a.wrapperClosed
    },
    in: isMenuOpen,
    timeout: 0
  }, MobileMenu_jsx("div", {
    onClick: toggleMenu
  })));
}

/* harmony default export */ var MobileMenu_MobileMenu = (MobileMenu);
// CONCATENATED MODULE: ./public/assets/logo.svg
function logo_extends() { logo_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return logo_extends.apply(this, arguments); }



var logo_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#logo_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M44.436 11.953h-3.692V27.02h3.692v-6.176h6.79v6.176h3.692V11.953h-3.691v5.75h-6.791v-5.75zM61.736 11.953h-3.692V27.02h3.692V11.953zM67.476 11.953h-4.204l5.713 15.067h4.823l5.767-15.067H75.37l-3.961 11.42-3.934-11.42zM104.663 27.02h4.338l-5.309-7.746 4.959-7.321h-4.366l-3.099 4.978-3.152-4.978h-4.339l4.958 7.347-5.308 7.72h4.365l3.476-5.297 3.477 5.297zM92.212 11.953H81.11v3.141h11.102v-3.141zM90.245 17.836H81.11v3.142h9.135v-3.142zM92.212 23.88H81.11v3.14h11.102v-3.14z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M22.69 17.597H11.48v3.3h11.21v-3.3z",
  fill: "url(#logo_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M26.489 5.35v24.838h-3.8v-9.29H11.48v14.88L17.084 39l17.085-9.77V9.743l-7.68-4.392z",
  fill: "#0255FB"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M11.48 17.597h11.21V3.195L17.083 0 0 9.743V29.23l7.707 4.393V8.89h3.772v8.706z",
  fill: "#0255FB"
}));

var logo_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "logo_svg__paint0_linear",
  x1: 11.48,
  y1: 19.253,
  x2: 22.698,
  y2: 19.253,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#929497"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.274,
  stopColor: "#B9BABC"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.589,
  stopColor: "#DFDFE0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.842,
  stopColor: "#F6F7F7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "logo_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  d: "M0 0h109v39H0z"
})));

function SvgLogo(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", logo_extends({
    width: 109,
    height: 39,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), logo_ref, logo_ref2);
}

/* harmony default export */ var logo = (SvgLogo);
// EXTERNAL MODULE: ./components/Header/Header.module.scss
var Header_module = __webpack_require__("yzvi");
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);

// CONCATENATED MODULE: ./components/Header/Header.js
var Header_jsx = external_react_default.a.createElement;









function Header(props) {
  const {
    0: isMenuOpen,
    1: setIsMenuOpen
  } = Object(external_react_["useState"])(false);
  const {
    route
  } = Object(router_["useRouter"])();

  function openMenu() {
    setIsMenuOpen(true);
  }

  const isMainPage = route === '/';
  return Header_jsx("header", {
    className: external_classnames_default()(Header_module_default.a.container, {
      [Header_module_default.a.fixedToTop]: isMainPage
    })
  }, Header_jsx("div", {
    className: Header_module_default.a.containerIn
  }, Header_jsx("div", {
    className: Header_module_default.a.left
  }, Header_jsx("a", {
    className: Header_module_default.a.logo,
    href: "/"
  }, Header_jsx(logo, null))), Header_jsx("nav", {
    className: Header_module_default.a.nav
  }, Header_jsx("ul", {
    className: Header_module_default.a.navIn
  }, Header_jsx("li", {
    className: Header_module_default.a.navItem
  }, Header_jsx(NavLink, {
    route: "/"
  }, Header_jsx("a", null, "Home"))), Header_jsx("li", {
    className: Header_module_default.a.navItem
  }, Header_jsx(NavLink, {
    route: "/interaction-models/dedicated-team"
  }, Header_jsx("a", null, "Cooperate with us"))), Header_jsx("li", {
    className: Header_module_default.a.navItem
  }, Header_jsx(NavLink, {
    route: "/work-with-us"
  }, Header_jsx("a", null, "Company")))))), Header_jsx(MobileMenu_MobileMenu, {
    isMenuOpen: isMenuOpen,
    setIsMenuOpen: setIsMenuOpen
  }));
}
// EXTERNAL MODULE: ./components/Page/Page.module.scss
var Page_module = __webpack_require__("d1WD");
var Page_module_default = /*#__PURE__*/__webpack_require__.n(Page_module);

// CONCATENATED MODULE: ./components/Page/Page.js
var Page_jsx = external_react_default.a.createElement;





function SvgBackground() {
  return Page_jsx("div", {
    className: "svg-animated-wrap"
  }, Page_jsx("svg", {
    className: "svg-animated-rb",
    width: "1008",
    height: "832",
    viewBox: "0 0 1008 832",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, Page_jsx("g", {
    opacity: "0.2",
    filter: "url(#filter0_f)"
  }, Page_jsx("path", {
    d: "M585.369 198.306C709.163 322.101 925.395 431.558 801.6 555.353C677.805 679.148 589.558 509.463 465.764 385.668C341.969 261.873 6.13289 368.648 129.928 244.853C253.723 121.058 461.574 74.5115 585.369 198.306Z",
    fill: "url(#paint0_radial)"
  })), Page_jsx("defs", null, Page_jsx("filter", {
    id: "filter0_f",
    x: "-31.7008",
    y: "-141.59",
    width: "1039.16",
    height: "1039.16",
    filterUnits: "userSpaceOnUse",
    colorInterpolationFilters: "sRGB"
  }, Page_jsx("feFlood", {
    floodOpacity: 0,
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "62",
    result: "effect1_foregroundBlur"
  })), Page_jsx("radialGradient", {
    id: "paint0_radial",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(440.519 463.823) rotate(45) scale(316.996 316.996)"
  }, Page_jsx("stop", {
    stopColor: "#0255FB"
  }, Page_jsx("animate", {
    attributeName: "stop-color",
    values: "#0255FB; #7A5FFF; #FB02B5; #0255FB",
    dur: "4s",
    repeatCount: "indefinite"
  }))))), Page_jsx("svg", {
    className: "svg-animated-lt",
    width: "758",
    height: "356",
    viewBox: "0 0 558 356",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, Page_jsx("g", {
    opacity: "0.6",
    filter: "url(#filter0_f)"
  }, Page_jsx("path", {
    d: "M438.056 23.1961C411.673 90.1082 60.5684 115.55 202.686 171.586C344.804 227.623 609.047 113.122 635.43 46.2099C661.813 -20.7023 567.992 -120.372 425.874 -176.408C283.756 -232.444 464.439 -43.7161 438.056 23.1961Z",
    fill: "url(#paint0_radial)"
  })), Page_jsx("defs", null, Page_jsx("filter", {
    id: "filter0_f",
    x: "0.220581",
    y: "-355.386",
    width: "808.374",
    height: "710.667",
    filterUnits: "userSpaceOnUse",
    "color-interpolation-filters": "sRGB"
  }, Page_jsx("feFlood", {
    "flood-opacity": "0",
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "84.3607",
    result: "effect1_foregroundBlur"
  })), Page_jsx("radialGradient", {
    id: "filter0_f",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(378.103 -55.2526) rotate(111.519) scale(164.392 349.16)"
  }, Page_jsx("stop", {
    "stop-color": "#FB02B5"
  }), Page_jsx("stop", {
    offset: "0.765625",
    "stop-color": "#0255FB"
  })))), Page_jsx("svg", {
    className: "svg-animated-lb",
    width: "508",
    height: "456",
    viewBox: "0 0 508 456",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, Page_jsx("g", {
    opacity: "0.7",
    filter: "url(#filter0_f)"
  }, Page_jsx("path", {
    d: "M348.023 248.622C383.869 282.291 426.676 255.133 393.059 290.923C359.443 326.713 303.132 328.433 267.286 294.764C231.441 261.095 236.744 266.23 270.361 230.44C303.977 194.65 312.177 214.953 348.023 248.622Z",
    fill: "url(#paint0_radial)"
  })), Page_jsx("g", {
    opacity: "0.7",
    filter: "url(#filter1_f)"
  }, Page_jsx("path", {
    d: "M238.208 115.535C274.218 149.358 252.976 186.474 219.378 222.244C185.78 258.014 152.549 278.893 116.539 245.07C80.5287 211.246 140.538 215.519 174.136 179.749C207.734 143.979 202.197 81.7111 238.208 115.535Z",
    fill: "url(#paint1_radial)"
  })), Page_jsx("defs", null, Page_jsx("filter", {
    id: "filter0_f",
    x: "136.928",
    y: "85.562",
    width: "370.679",
    height: "369.771",
    filterUnits: "userSpaceOnUse",
    colorInterpolationFilters: "sRGB"
  }, Page_jsx("feFlood", {
    floodOpacity: 0,
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "49.8033",
    result: "effect1_foregroundBlur"
  })), Page_jsx("filter", {
    id: "filter1_f",
    x: "0.393448",
    y: "0.393448",
    width: "380.667",
    height: "384.903",
    filterUnits: "userSpaceOnUse",
    colorInterpolationFilters: "sRGB"
  }, Page_jsx("feFlood", {
    floodOpacity: 0,
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "49.8033",
    result: "effect1_foregroundBlur"
  })), Page_jsx("radialGradient", {
    id: "paint0_radial",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(328.155 229.96) rotate(43.2064) scale(112.402 112.227)"
  }, Page_jsx("stop", {
    stopColor: "#0255FB"
  }), Page_jsx("stop", {
    offset: "1",
    stopColor: "#02D4FB"
  })), Page_jsx("radialGradient", {
    id: "paint1_radial",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(177.373 180.302) rotate(43.2064) scale(112.917 112.164)"
  }, Page_jsx("stop", {
    stopColor: "#0255FB"
  }), Page_jsx("stop", {
    offset: "1",
    stopColor: "#02D4FB"
  })))));
}

function Page({
  children
}) {
  return Page_jsx(external_react_default.a.Fragment, null, Page_jsx(Header, null), Page_jsx(CookiesConfirmation_CookiesConfirmation, null), Page_jsx("div", {
    className: Page_module_default.a.container
  }, children), Page_jsx(SvgBackground, null));
}

/***/ }),

/***/ "bJNL":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Form_wrapHeader__32Xjn",
	"wrap": "Form_wrap__3kOpw",
	"wrapInLeft": "Form_wrapInLeft__363mE",
	"wrapInCenter": "Form_wrapInCenter__2scJv",
	"hidden": "Form_hidden__2V7PF",
	"container": "Form_container__2ID_0",
	"textBlockTitle": "Form_textBlockTitle__Wwewf",
	"textBlockDescription": "Form_textBlockDescription___ih_y",
	"form": "Form_form__us8qB",
	"col": "Form_col__3zSMW",
	"row": "Form_row__2Hpkq",
	"confirmBtn": "Form_confirmBtn__m5iG2"
};


/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cMTN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TechnologiesSubroutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InteractionModelTypes; });
const TechnologiesSubroutes = [{
  name: 'Web-Frontend',
  route: '/technologies/web-frontend'
}, {
  name: 'Web-Backend',
  route: '/technologies/web-backend'
}, {
  name: 'Mobile',
  route: '/technologies/mobile'
}, {
  name: 'DevOps',
  route: '/technologies/dev-ops'
}, {
  name: 'Machine Learning & AI',
  route: '/technologies/machine-learning'
}, {
  name: 'Quality Assurance',
  route: '/technologies/qa'
}, {
  name: 'Big data',
  route: '/technologies/big-data'
}];
const InteractionModelTypes = [{
  link: '/interaction-models/dedicated-team',
  value: 'Dedicated team'
}, {
  link: '/interaction-models/hourly-payment',
  value: 'Hourly payment'
}];

/***/ }),

/***/ "d1WD":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Page_wrapHeader__SK-tE",
	"wrap": "Page_wrap__3PV08",
	"container": "Page_container__3nCVR",
	"wrapInLeft": "Page_wrapInLeft__2gHgy",
	"wrapInCenter": "Page_wrapInCenter__nDA6O",
	"hidden": "Page_hidden__3bvai"
};


/***/ }),

/***/ "hK8T":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ Form; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__("K2gz");
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// EXTERNAL MODULE: ./components/Input/Input.module.scss
var Input_module = __webpack_require__("oo+y");
var Input_module_default = /*#__PURE__*/__webpack_require__.n(Input_module);

// CONCATENATED MODULE: ./components/Input/Input.js
var __jsx = external_react_default.a.createElement;


function Input({
  id,
  placeholder,
  required,
  type,
  rows = 5
}) {
  const inputId = `input_id_${id}`;
  const inputPlaceholder = `${placeholder} ${required ? '*' : ''}`;

  const InputComponent = () => type === 'textarea' ? __jsx("textarea", {
    required: required,
    id: inputId,
    rows: rows,
    className: Input_module_default.a.field,
    name: id,
    placeholder: inputPlaceholder
  }) : __jsx("input", {
    required: required,
    type: type,
    id: inputId,
    className: Input_module_default.a.field,
    name: id,
    placeholder: inputPlaceholder
  });

  return __jsx("div", {
    className: Input_module_default.a.wrapper
  }, __jsx(InputComponent, null), __jsx("label", {
    htmlFor: inputId,
    className: Input_module_default.a.label
  }, __jsx("span", null, inputPlaceholder)), __jsx("div", {
    className: Input_module_default.a.underline
  }));
}
// EXTERNAL MODULE: ./components/Button/Button.js
var Button = __webpack_require__("8wsC");

// EXTERNAL MODULE: ./components/Form/Form.module.scss
var Form_module = __webpack_require__("bJNL");
var Form_module_default = /*#__PURE__*/__webpack_require__.n(Form_module);

// CONCATENATED MODULE: ./components/Form/Form.js

var Form_jsx = external_react_default.a.createElement;




function Form({
  formClassName = ''
}) {
  return Form_jsx("div", {
    className: Form_module_default.a.container
  }, Form_jsx("article", {
    className: Form_module_default.a.textBlock
  }, Form_jsx("h2", {
    className: Form_module_default.a.textBlockTitle
  }, "Leave a request"), Form_jsx("p", {
    className: Form_module_default.a.textBlockDescription
  }, "Tell us about the project and put your questions - we will answer ASAP")), Form_jsx("form", {
    className: external_classnames_default()(Form_module_default.a.form, formClassName)
  }, Form_jsx("div", {
    className: Form_module_default.a.col
  }, Form_jsx("div", {
    className: Form_module_default.a.row
  }, Form_jsx("div", {
    className: Form_module_default.a.col
  }, Form_jsx(Input, {
    placeholder: "Name",
    id: "name",
    required: true,
    type: "text"
  })), Form_jsx("div", {
    className: Form_module_default.a.col
  }, Form_jsx(Input, {
    placeholder: "Email",
    id: "email",
    required: true,
    type: "email"
  }))), Form_jsx("div", {
    className: Form_module_default.a.row
  }, Form_jsx(Input, {
    placeholder: "Subject",
    id: "subject",
    required: true,
    type: "text"
  }))), Form_jsx("div", {
    className: Form_module_default.a.col
  }, Form_jsx(Input, {
    placeholder: "Project description",
    type: "textarea",
    rows: 5,
    id: "description",
    required: true
  })), Form_jsx(Button["a" /* default */], {
    className: Form_module_default.a.confirmBtn
  }, "Send")));
}

/***/ }),

/***/ "jY/B":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Dropdown_wrapHeader__3WtZY",
	"wrap": "Dropdown_wrap__cjTQs",
	"wrapInLeft": "Dropdown_wrapInLeft__nf79s",
	"wrapInCenter": "Dropdown_wrapInCenter__1jHGs",
	"hidden": "Dropdown_hidden__OmYdM",
	"container": "Dropdown_container__2v6C_",
	"name": "Dropdown_name__2x8wH",
	"title": "Dropdown_title__1lh20",
	"arrow": "Dropdown_arrow__3bX6Z",
	"dropdownNameShowed": "Dropdown_dropdownNameShowed__1wLYX",
	"subroutesContainer": "Dropdown_subroutesContainer__3Nib8",
	"dropdownSubroutes": "Dropdown_dropdownSubroutes__2EdTy",
	"dropdownSubroutesItem": "Dropdown_dropdownSubroutesItem__ShZOY",
	"dropdownClosed": "Dropdown_dropdownClosed__3bVYz",
	"dropdownOpen": "Dropdown_dropdownOpen__36wL4"
};


/***/ }),

/***/ "jnZq":
/***/ (function(module) {

module.exports = JSON.parse("{\"technologiesTypes\":{\"web-frontend\":{\"title\":\"Web-Frontend\",\"description\":\"We develop fully functional web applications, available online to millions of users in any country of the world. Our team works with various databases and APIs, implements scaling and integration with other services. By mixing technology stacks effectively, Brights Ukrainian web development company devises solutions that streamline your business operations and deliver value to your customers. We have been in the development market for 8 years! Guiding you through all the web development stages we provide you the best customer-oriented technologies for your custom web application development.\",\"defaultValue\":\"vue\",\"list\":[{\"key\":\"vue\",\"title\":\"Vue.js\"},{\"key\":\"ember\",\"title\":\"Ember\"},{\"key\":\"react\",\"title\":\"React js\"},{\"key\":\"svetle\",\"title\":\"Svetle\"},{\"key\":\"angular\",\"title\":\"Angular js\"}]},\"web-backend\":{\"title\":\"Web-Backend\",\"description\":\"Hivex exclusive quality assurance and testing services give you an amazing opportunity to significantly reduce all risks connected with the life cycle of the whole software development\",\"defaultValue\":\"ruby\",\"list\":[{\"key\":\"ruby\",\"title\":\"Ruby\"},{\"key\":\"ember\",\"title\":\"Ember\"},{\"key\":\"django\",\"title\":\"Django\"},{\"key\":\"php\",\"title\":\"PHP\"},{\"key\":\"laravel\",\"title\":\"Laravel\"}]},\"mobile\":{\"title\":\"Mobile\",\"description\":\"Our software development department can easily create any iOS or Android mobile apps. For the creation of such apps, we use only high-tech instruments. The same tools are used by the best developers of apps in the world. According to the desire of our customer, later on we can add any new features to the app, providing you comprehensive support. It’s worth mentioning that we autonomously work in our own private repository.\",\"defaultValue\":\"android\",\"list\":[{\"key\":\"android\",\"title\":\"Android\"},{\"key\":\"ios\",\"title\":\"Ios\"},{\"key\":\"java\",\"title\":\"Java\"},{\"key\":\"c-sharp\",\"title\":\"C#\"},{\"key\":\"swift\",\"title\":\"Swift\"}]},\"dev-ops\":{\"title\":\"DevOps\",\"description\":\"Apart from everything else, Hivex offers you a great variety of DevOps services and solutions. DevOps is a set of practices aimed at improving the efficiency of developmental and operational software. Thanks to this set, you’ll be able to timely and efficiently respond to changing market conditions. Our team will be glad to help you with any network operations.\",\"defaultValue\":\"amazon-aws\",\"list\":[{\"key\":\"amazon-aws\",\"title\":\"Amazon AWS\"},{\"key\":\"chef\",\"title\":\"Chef\"},{\"key\":\"linux\",\"title\":\"Linux\"},{\"key\":\"puppet\",\"title\":\"Puppet\"},{\"key\":\"ansible\",\"title\":\"Ansible\"}]},\"machine-learning\":{\"title\":\"Machine Learning & AI\",\"description\":\"Hivex company is a developer of a module that was created in order to provide you with crowd-sourcing measurements from smartphones. This company can provide you with the highest class specialists from Central and Eastern Europe that can easily solve any algorithms and statistical modeling tasks connected with machine learning or AI.\",\"defaultValue\":\"python\",\"list\":[{\"key\":\"python\",\"title\":\"Python\"},{\"key\":\"c-plus-plus\",\"title\":\"C++\"},{\"key\":\"java\",\"title\":\"Java\"},{\"key\":\"c-sharp\",\"title\":\"C#\"},{\"key\":\"swift\",\"title\":\"Swift\"}]},\"qa\":{\"title\":\"Quality Assurance\",\"description\":\"Hivex exclusive quality assurance and testing services give you an amazing opportunity to significantly reduce all risks connected with the life cycle of the whole software development\",\"defaultValue\":\"hp\",\"list\":[{\"key\":\"hp\",\"title\":\"HP\"},{\"key\":\"jira\",\"title\":\"Jira\"},{\"key\":\"jmeter\",\"title\":\"JMeter\"},{\"key\":\"qa\",\"title\":\"QA\"},{\"key\":\"selenium\",\"title\":\"Selenium\"}]},\"big-data\":{\"title\":\"Big data\",\"description\":\"Apart from everything else, Hivex offers you a great variety of DevOps services and solutions. DevOps is a set of practices aimed at improving the efficiency of developmental and operational software. Thanks to this set, you’ll be able to timely and efficiently respond to changing market conditions. Our team will be glad to help you with any network operations.\",\"defaultValue\":\"python\",\"list\":[{\"key\":\"python\",\"title\":\"Python\"},{\"key\":\"java\",\"title\":\"Java\"},{\"key\":\"spark\",\"title\":\"Spark\"},{\"key\":\"kafka\",\"title\":\"Kafka\"},{\"key\":\"hadoop\",\"title\":\"Angular js\"}]}},\"programmingLanguagesList\":{\"react\":{\"title\":\"React js development\",\"description\":\"React js is a famous open-source JavaScript library, designed to develop the user interface. This technology is upheld by Facebook, Instagram, and other developers from different publics and companies. React js lets us develop single-page and mobile applications. Our React js development company has a big portfolio with various projects, based on React js development technology.\"},\"ember\":{\"title\":\"Ember\",\"description\":\"[EMBER TEXT HERE]\"},\"vue\":{\"title\":\"Vue.js\",\"description\":\"[VUE TEXT HERE]\"},\"svetle\":{\"title\":\"Svetle\",\"description\":\"[SVETLE TEXT HERE]\"},\"angular\":{\"title\":\"Angular js\",\"description\":\"[ANGULAR TEXT HERE]\"},\"java\":{\"title\":\"Java development\",\"description\":\"Java is a strictly typified object-oriented language that can be controlled in different ways, one of which is via a virtual machine. It was developed by Sun MicroSystems Company. This language gives you an opportunity for software reuse for software development which is also pretty convenient.\"},\"android\":{\"title\":\"Android\",\"description\":\"Android is a mobile operating system developed by Google. This is a modified version of the Linux kernel source. Mainly used for touch screen mobile devices. Our Android app specialists will create a high quality mobile app development for any purpose.\"},\"ios\":{\"title\":\"IOS\",\"description\":\"iOS is a mobile operating system manufactured by Apple Inc. iOS is used on iPhone, iPad, iPod Touch and Apple TV. With our iOS app development experts, you can find a reliable partner for your business.\"},\"c-sharp\":{\"title\":\"С#\",\"description\":\"C# is used to build native apps for billions of Android, iPhone, iPad, Mac and Windows devices around the world. By adding generic code to that with any .NET application so you can build applications more efficiently than ever on all platforms.\"},\"swift\":{\"title\":\"Swift\",\"description\":\"Swift is a fast, efficient, real-time responsive programming language that can be easily embedded into off-the-shelf Objective-C code. Now our team can not only write more reliable and secure codes, but also save time and create applications with advanced features.\"},\"python\":{\"title\":\"Python\",\"description\":\"Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. Its high-level built in data structures, combined with dynamic typing and dynamic binding, make it very attractive for Rapid Application Development.\"},\"c-plus-plus\":{\"title\":\"C++\",\"description\":\"C ++ is a high level programming language. C ++ is one of the most popular programming languages for graphical applications, such as those that run on Windows and Macintosh environments.\"},\"hp\":{\"title\":\"hp\",\"description\":\"The R&D organization's quality assurance practice is industry leading. From the concept of the product itself to develop, validate, and deploy the product, you can be confident that your product has a vision that was \\\"laid down\\\" early in the lifecycle.\"},\"jira\":{\"title\":\"Jira\",\"description\":\"Jira is a proprietary issue support product that enables Atlassian, bug tracking and flexible project management.\"},\"jmeter\":{\"title\":\"Apache JMeter\",\"description\":\"This product is one of the Apache projects that can be used as a load-testing instrument. It focuses on different web applications, particularly on testing and analyzing of service performance.\"},\"qa\":{\"title\":\"QA\",\"description\":\"This is a complex quality assurance process that encompasses all stages of a company's software product development. QA includes the study of processes and the definition of all conditions and circumstances that may affect the quality of development and the final product.\"},\"selenium\":{\"title\":\"Selenium\",\"description\":\"Selenium WebDriver is a collection of open source APIs which are used to automate the testing of a web application. ... It also supports different programming languages such as C #, Java, Perl, PHP and Ruby for writing test scripts.\"},\"ruby\":{\"title\":\"RUBY\",\"description\":\"Ruby on Rails is used to write server-side web application logic using the open source Ruby programming language. The main goal of Ruby on Rails is to develop back-end components, web service applications, and support front-end developers.\"},\"django\":{\"title\":\"DJANGO\",\"description\":\"A product of Django Software Foundation. That’s a free framework for web-applications which uses the MTV architectural pattern. This framework is Python-based and absolutely free.\"},\"php\":{\"title\":\"PHP\",\"description\":\"PHP is a widely used open source general-purpose scripting language especially for web development and can be embedded in HTML. Our web development company PHP uses this popular language to create dynamic websites.\"},\"laravel\":{\"title\":\"Laravel\",\"description\":\"Laravel is a free, open-source PHP web framework, created and intended for the development of web applications following the model – view – controller (MVC).\"},\"linux\":{\"title\":\"LINUX\",\"description\":\"Linux is one of the most popular Unix-like operating systems in the world for PCs, servers, mobile devices, etc. based on Linux kernel. Such big computer platforms as SPARC, ARM, and X86 support Linux as well.\"},\"amazon-aws\":{\"title\":\"Amazon AWS\",\"description\":\"Ruby on Rails is used to write server-side web application logic using the open source Ruby programming language. The main goal of Ruby on Rails is to develop back-end components, web service applications, and support front-end developers.\"},\"chef\":{\"title\":\"Chef\",\"description\":\"Chef is a configuration management system written in Ruby (front-end) and Erlang (back-end) using a domain-specific description language. Used to simplify tasks and support systems for integration servers in cloud platforms such as Rackspace and Amazon EC2.\"},\"puppet\":{\"title\":\"Puppet\",\"description\":\"Puppet is a cross-platform client-server application that allows you to centrally manage the configuration of operating systems and programs installed on multiple computers. Written in the Ruby programming language.\"},\"ansible\":{\"title\":\"Ansible\",\"description\":\"Ansible is a configuration management system written in the Python programming language using a declarative markup language to describe configurations. Used to automate software configuration and deployment.\"},\"spark\":{\"title\":\"Spark\",\"description\":\"One more product of Apache. It’s an open-source cluster-computer framework. Spark was exclusively created in order to work with large amounts of data and analytics. Spark works with various sources such as HDFS, Cassandra, and OpenStack Swift.\"},\"kafka\":{\"title\":\"Kafka\",\"description\":\"Kafka is a data stream used to feed Hadoop BigData lakes. Kafka brokers support massive message streams for low-latency follow-up analysis in Hadoop or Spark. Also, Kafka Streams (a subproject) can be used for real-time analytics.\"},\"hadoop\":{\"title\":\"Hadoop\",\"description\":\"Apache Hadoop is one of the foundational technologies for big data, and it is written in Java. Hadoop is a free and open source suite of utilities, libraries, and frameworks managed by the Apache Software Foundation.\"}}}");

/***/ }),

/***/ "kPE/":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "NavLink_wrapHeader__O8y3w",
	"wrap": "NavLink_wrap__2YDsA",
	"wrapInLeft": "NavLink_wrapInLeft__1auL8",
	"wrapInCenter": "NavLink_wrapInCenter__104gq",
	"hidden": "NavLink_hidden__WDNC5",
	"NavLink": "NavLink_NavLink__18jpR",
	"NavLinkActive": "NavLink_NavLinkActive__1fXFl"
};


/***/ }),

/***/ "kvya":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "SocialLinks_wrapHeader__3dMQv",
	"wrap": "SocialLinks_wrap__16FE5",
	"wrapInLeft": "SocialLinks_wrapInLeft__261iX",
	"wrapInCenter": "SocialLinks_wrapInCenter__2zq-h",
	"hidden": "SocialLinks_hidden__2PcE6",
	"container": "SocialLinks_container__xipb6",
	"item": "SocialLinks_item__308gW",
	"link": "SocialLinks_link__3G8X9"
};


/***/ }),

/***/ "kzqI":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Footer_wrapHeader__kqZCB",
	"wrap": "Footer_wrap__372cF",
	"wrapInLeft": "Footer_wrapInLeft__16p1G",
	"wrapInCenter": "Footer_wrapInCenter__2jYMB",
	"hidden": "Footer_hidden__1f_3V",
	"container": "Footer_container__l2D4g",
	"info": "Footer_info__3Ojhd",
	"infoHead": "Footer_infoHead__17VVW",
	"infoMapView": "Footer_infoMapView__3wk9y",
	"infoCopyRight": "Footer_infoCopyRight__uAq8I",
	"navIn": "Footer_navIn__Yf_fX",
	"navItem": "Footer_navItem__3nSJx",
	"columnHead": "Footer_columnHead__1ist4",
	"cooperationEmailUs": "Footer_cooperationEmailUs__3cnjN",
	"cooperation": "Footer_cooperation__3rRQW",
	"cooperationItem": "Footer_cooperationItem__2qUCS"
};


/***/ }),

/***/ "oo+y":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Input_wrapHeader__1mGrd",
	"wrap": "Input_wrap__1SJqH",
	"wrapInLeft": "Input_wrapInLeft__3ZpAY",
	"wrapInCenter": "Input_wrapInCenter__3pPKW",
	"hidden": "Input_hidden__yRY67",
	"wrapper": "Input_wrapper__x8uCu",
	"field": "Input_field__1uPUd",
	"label": "Input_label__1lLYJ",
	"underline": "Input_underline__3U5pB"
};


/***/ }),

/***/ "pq5z":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"workflow": "IdIcon_workflow__1da3u",
	"programmingLanguage": "IdIcon_programmingLanguage__2LsTU",
	"hexagonBg": "IdIcon_hexagonBg__3cfKN",
	"hexagonItem": "IdIcon_hexagonItem__91yu4",
	"langContainer": "IdIcon_langContainer__2JTpv",
	"hexagonBgActive": "IdIcon_hexagonBgActive__1mkHM",
	"langIcon": "IdIcon_langIcon__QoKXw"
};


/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "yxEx":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "MobileMenu_wrapHeader__2cMaN",
	"wrap": "MobileMenu_wrap__29YsL",
	"wrapInLeft": "MobileMenu_wrapInLeft__2Qswu",
	"wrapInCenter": "MobileMenu_wrapInCenter__2xvWJ",
	"hidden": "MobileMenu_hidden__3Y4b1",
	"mobileWrap": "MobileMenu_mobileWrap__1Tdg8",
	"container": "MobileMenu_container__1qe4-",
	"control": "MobileMenu_control__3vIul",
	"grayWrapper": "MobileMenu_grayWrapper__1b9hA",
	"nav": "MobileMenu_nav__9t5Hq",
	"navIn": "MobileMenu_navIn__2eEGQ",
	"navItem": "MobileMenu_navItem__nbeh4",
	"btn": "MobileMenu_btn__3a6zW",
	"toggleMenuContainer": "MobileMenu_toggleMenuContainer__3jr1X",
	"dropdown": "MobileMenu_dropdown__30B6O",
	"menuOpen": "MobileMenu_menuOpen__737-b",
	"menuClosed": "MobileMenu_menuClosed__1t8ar",
	"wrapperOpen": "MobileMenu_wrapperOpen__1b_C0",
	"wrapperClosed": "MobileMenu_wrapperClosed__2ZRBW"
};


/***/ }),

/***/ "yzvi":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Header_wrapHeader__3bEbQ",
	"wrap": "Header_wrap__3c1CF",
	"containerIn": "Header_containerIn__21gIb",
	"wrapInLeft": "Header_wrapInLeft__2Nb_B",
	"wrapInCenter": "Header_wrapInCenter__1pi4U",
	"hidden": "Header_hidden__y5jGu",
	"container": "Header_container__3cXlC",
	"fixedToTop": "Header_fixedToTop__1j-Fa",
	"left": "Header_left__jkv05",
	"nav": "Header_nav__2SG38",
	"navIn": "Header_navIn__1iiLj",
	"navItem": "Header_navItem__3Fat9"
};


/***/ })

/******/ });