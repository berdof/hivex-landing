module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/waN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__("K2gz");
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__("xnum");
var head_default = /*#__PURE__*/__webpack_require__.n(head_);

// EXTERNAL MODULE: ./routes/Cases/casesData.json
var casesData = __webpack_require__("sgxf");

// EXTERNAL MODULE: ./components/Page/Page.js + 9 modules
var Page = __webpack_require__("YHOl");

// EXTERNAL MODULE: ./components/ReadMoreButton/ReadMoreButton.js
var ReadMoreButton = __webpack_require__("bmoy");

// EXTERNAL MODULE: ./components/Footer/Footer.js
var Footer = __webpack_require__("HXcA");

// EXTERNAL MODULE: ./routes/Cases/Cases.module.scss
var Cases_module = __webpack_require__("tUy9");
var Cases_module_default = /*#__PURE__*/__webpack_require__.n(Cases_module);

// CONCATENATED MODULE: ./routes/Cases/Cases.js
var __jsx = external_react_default.a.createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }










function LinkToResource({
  title,
  link,
  className = ''
}) {
  return __jsx("a", {
    className: external_classnames_default()(Cases_module_default.a.linkToResource, className),
    href: link
  }, title);
}

class Cases_Cases extends external_react_["Component"] {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "renderList", () => {
      return __jsx("div", {
        className: Cases_module_default.a.caseListContainer
      }, __jsx("span", {
        className: Cases_module_default.a.caseListTitle
      }, "Check out the following projects"), __jsx("ul", {
        className: Cases_module_default.a.caseList
      }, this.filteredCases.map(item => __jsx("li", {
        className: Cases_module_default.a.caseListItem
      }, __jsx("div", {
        className: Cases_module_default.a.caseListItemImageWrapper
      }, __jsx("img", {
        className: Cases_module_default.a.caseListItemImage,
        src: item.titleImage,
        alt: "Case image"
      })), __jsx("div", {
        className: Cases_module_default.a.caseListItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.caseListItemTitle
      }, item.title), __jsx("p", {
        className: Cases_module_default.a.caseListItemDescription
      }, item.projectDescription), __jsx(ReadMoreButton["a" /* default */], {
        link: `/cases/${item.key}`
      }))))));
    });

    _defineProperty(this, "renderCasePage", () => {
      const {
        query: {
          caseName
        }
      } = this.props;
      const {
        currentCaseData
      } = this;
      return __jsx(external_react_default.a.Fragment, null, __jsx("div", {
        className: Cases_module_default.a.head
      }, __jsx("div", {
        className: external_classnames_default()(Cases_module_default.a.headInfo, Cases_module_default.a.hideOnMobile)
      }, __jsx("span", {
        className: Cases_module_default.a.year
      }, currentCaseData.year), __jsx("div", {
        className: Cases_module_default.a.infoItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemTitle
      }, "Model:"), __jsx("span", {
        className: Cases_module_default.a.infoItemValue
      }, currentCaseData.model)), __jsx("div", {
        className: Cases_module_default.a.infoItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemTitle
      }, "Term:"), __jsx("span", {
        className: Cases_module_default.a.infoItemValue
      }, currentCaseData.term)), __jsx("div", {
        className: Cases_module_default.a.infoItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemTitle
      }, "Technologies:"), __jsx("ul", {
        className: Cases_module_default.a.infoItemList
      }, currentCaseData.technologies.map(item => __jsx("li", {
        key: item.id
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemValue
      }, item.title)))))), __jsx("div", {
        className: external_classnames_default()(Cases_module_default.a.mobileHeadInfo, Cases_module_default.a.hideOnDesktop)
      }, __jsx("div", {
        className: Cases_module_default.a.mobileHeadInfoTitle
      }, __jsx("h1", {
        className: Cases_module_default.a.caseTitle
      }, currentCaseData.title), __jsx(LinkToResource, {
        title: currentCaseData.linkTitle,
        link: currentCaseData.linkToResource
      })), __jsx("div", {
        className: Cases_module_default.a.mobileHeadInfoData
      }, __jsx("div", {
        className: Cases_module_default.a.mobileHeadInfoDataBlock
      }, __jsx("div", {
        className: Cases_module_default.a.infoItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemTitle
      }, "Model:"), __jsx("span", {
        className: Cases_module_default.a.infoItemValue
      }, currentCaseData.model)), __jsx("div", {
        className: Cases_module_default.a.infoItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemTitle
      }, "Term:"), __jsx("span", {
        className: Cases_module_default.a.infoItemValue
      }, currentCaseData.term)), __jsx("div", {
        className: Cases_module_default.a.infoItemContainer
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemTitle
      }, "Technologies:"), __jsx("ul", {
        className: Cases_module_default.a.infoItemList
      }, currentCaseData.technologies.map(item => __jsx("li", {
        key: item.id
      }, __jsx("span", {
        className: Cases_module_default.a.infoItemValue
      }, item.title)))))), __jsx("div", {
        className: Cases_module_default.a.mobileHeadInfoDataBlock
      }, __jsx("span", {
        className: Cases_module_default.a.year
      }, currentCaseData.year)))), __jsx("article", {
        className: Cases_module_default.a.headDescription
      }, __jsx("h1", {
        className: external_classnames_default()(Cases_module_default.a.caseTitle, Cases_module_default.a.hideOnMobile)
      }, currentCaseData.title), __jsx("span", {
        className: Cases_module_default.a.projectDescription
      }, currentCaseData.projectDescription), __jsx(LinkToResource, {
        title: currentCaseData.linkTitle,
        link: currentCaseData.linkToResource,
        className: Cases_module_default.a.hideOnMobile
      }), __jsx("p", {
        className: Cases_module_default.a.caseDescription
      }, "Case description: ", currentCaseData.caseDescription))), __jsx("div", {
        className: Cases_module_default.a.caseImagesList
      }, currentCaseData.images.map(item => __jsx("img", {
        className: Cases_module_default.a.caseImage,
        key: item.id,
        src: item.src,
        alt: "Case image"
      }))), __jsx("div", {
        className: Cases_module_default.a.challengeContainer
      }, __jsx("span", {
        className: Cases_module_default.a.challengeTitle
      }, "Challenge"), __jsx("p", {
        className: Cases_module_default.a.challengeDescription
      }, currentCaseData.challenge)), __jsx("ul", {
        className: Cases_module_default.a.numbersContainer
      }, currentCaseData.temporaryNumbers.map(item => __jsx("li", {
        className: Cases_module_default.a.numbersItem,
        key: item.id
      }, __jsx("span", {
        className: Cases_module_default.a.numbersItemTitle
      }, item.count), __jsx("span", {
        className: Cases_module_default.a.numbersItemDescription
      }, item.title)))));
    });
  }

  static async getInitialProps({
    query
  }) {
    return {
      query
    };
  }

  get currentCaseData() {
    const {
      query: {
        caseName
      }
    } = this.props;
    return casesData.find(item => item.key === caseName);
  }

  get filteredCases() {
    const {
      query: {
        caseName
      }
    } = this.props;
    return casesData.filter(item => item.key !== caseName);
  }

  get pageTitle() {
    const {
      query: {
        caseName
      }
    } = this.props;
    return caseName || 'Portfolio';
  }

  render() {
    const {
      query: {
        caseName
      }
    } = this.props;
    return __jsx(Page["a" /* default */], null, __jsx(head_default.a, null, __jsx("title", null, "Hivex - ", this.pageTitle)), __jsx("div", {
      className: Cases_module_default.a.container
    }, caseName && this.renderCasePage(),  false && false, __jsx(Footer["a" /* default */], {
      className: Cases_module_default.a.footer
    })));
  }

}

/* harmony default export */ var routes_Cases_Cases = (Cases_Cases);
// CONCATENATED MODULE: ./pages/cases.js

/* harmony default export */ var cases = __webpack_exports__["default"] = (routes_Cases_Cases);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("/waN");


/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "61VP":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "ReadMoreButton_wrapHeader__2j8cE",
	"wrap": "ReadMoreButton_wrap__32lxb",
	"wrapInLeft": "ReadMoreButton_wrapInLeft__jqLhh",
	"wrapInCenter": "ReadMoreButton_wrapInCenter__-4NWt",
	"hidden": "ReadMoreButton_hidden__3BI2R",
	"container": "ReadMoreButton_container__2Oi3u",
	"image": "ReadMoreButton_image__3roxo"
};


/***/ }),

/***/ "73Hj":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Button_wrapHeader__1JYsI",
	"wrap": "Button_wrap__1NxA6",
	"wrapInLeft": "Button_wrapInLeft__2YmlP",
	"wrapInCenter": "Button_wrapInCenter__2cX9s",
	"hidden": "Button_hidden__19hY1",
	"btn": "Button_btn__3A27M",
	"btnMain": "Button_btnMain__1v6TM",
	"btnSecondary": "Button_btnSecondary__19dlM"
};


/***/ }),

/***/ "8cHP":
/***/ (function(module, exports, __webpack_require__) {

const routes = __webpack_require__("90Kz"); // Name   Page      Pattern


module.exports = routes() // ----   ----      -----
.add('index').add('work-with-us').add('technologies', '/technologies/:technologyName').add('interaction-models', '/interaction-models/:modelType').add('cases', '/cases/:caseName?');

/***/ }),

/***/ "8wsC":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Button; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Button_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("73Hj");
/* harmony import */ var _Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Button_module_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



function Button(props) {
  const {
    className = '',
    children,
    secondary = false,
    onClick = () => {},
    disabled = false
  } = props;
  return __jsx("button", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.btn, className, {
      [_Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.btnSecondary]: secondary,
      [_Button_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.btnMain]: !secondary
    }),
    disabled: disabled,
    onClick: onClick
  }, children);
}

/***/ }),

/***/ "90Kz":
/***/ (function(module, exports) {

module.exports = require("next-routes");

/***/ }),

/***/ "CSOn":
/***/ (function(module, exports) {

module.exports = require("react-transition-group");

/***/ }),

/***/ "E1mO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./public/assets/facebook.svg
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }



var _ref = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16 .273c-8.836 0-16 7.164-16 16s7.164 16 16 16 16-7.164 16-16-7.164-16-16-16zm0 1c8.271 0 15 6.73 15 15 0 8.271-6.729 15-15 15s-15-6.729-15-15c0-8.27 6.729-15 15-15zm-5.333 12.334h2.666V11.35c0-2.022 1.064-3.078 3.462-3.078H20v3.334h-1.923c-.616 0-.744.252-.744.89v1.11H20l-.24 2.666h-2.427v8h-4v-8h-2.666v-2.666z",
  fill: "#2B2B2B"
});

function SvgFacebook(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", _extends({
    width: 32,
    height: 33,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _ref);
}

/* harmony default export */ var facebook = (SvgFacebook);
// CONCATENATED MODULE: ./public/assets/twitter.svg
function twitter_extends() { twitter_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return twitter_extends.apply(this, arguments); }



var twitter_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16 .273c-8.836 0-16 7.164-16 16s7.164 16 16 16 16-7.164 16-16-7.164-16-16-16zm0 1c8.271 0 15 6.73 15 15 0 8.271-6.729 15-15 15s-15-6.729-15-15c0-8.27 6.729-15 15-15zm6.781 11.222a6.569 6.569 0 001.886-.518 6.616 6.616 0 01-1.639 1.7c.208 4.616-3.235 9.764-9.33 9.764a9.284 9.284 0 01-5.031-1.474 6.606 6.606 0 004.862-1.362 3.287 3.287 0 01-3.066-2.278 3.29 3.29 0 001.482-.056c-1.578-.318-2.669-1.74-2.633-3.26.443.245.948.393 1.485.41a3.287 3.287 0 01-1.016-4.382 9.323 9.323 0 006.766 3.43 3.286 3.286 0 015.593-2.994 6.568 6.568 0 002.085-.796 3.297 3.297 0 01-1.444 1.816z",
  fill: "#2B2B2B"
});

function SvgTwitter(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", twitter_extends({
    width: 32,
    height: 33,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), twitter_ref);
}

/* harmony default export */ var twitter = (SvgTwitter);
// CONCATENATED MODULE: ./public/assets/instagram.svg
function instagram_extends() { instagram_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return instagram_extends.apply(this, arguments); }



var instagram_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16 .273c-8.836 0-16 7.164-16 16s7.164 16 16 16 16-7.164 16-16-7.164-16-16-16zm0 1c8.271 0 15 6.73 15 15 0 8.271-6.729 15-15 15s-15-6.729-15-15c0-8.27 6.729-15 15-15zm-4.55 7A3.45 3.45 0 008 11.723v9.101a3.45 3.45 0 003.45 3.45h9.1a3.45 3.45 0 003.45-3.45v-9.102a3.45 3.45 0 00-3.45-3.449h-9.1zM16 19.723a3.45 3.45 0 100-6.899 3.45 3.45 0 000 6.899zm5.665-8.126a1 1 0 11-2 0 1 1 0 012 0z",
  fill: "#2B2B2B"
});

function SvgInstagram(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", instagram_extends({
    width: 32,
    height: 33,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), instagram_ref);
}

/* harmony default export */ var instagram = (SvgInstagram);
// EXTERNAL MODULE: ./components/SocialLinks/SocialLinks.module.scss
var SocialLinks_module = __webpack_require__("kvya");
var SocialLinks_module_default = /*#__PURE__*/__webpack_require__.n(SocialLinks_module);

// CONCATENATED MODULE: ./components/SocialLinks/SocialLinks.js
var __jsx = external_react_default.a.createElement;






function SocialLinks(props) {
  return __jsx("ul", {
    className: SocialLinks_module_default.a.container
  }, __jsx("li", {
    className: SocialLinks_module_default.a.item
  }, __jsx("a", {
    className: SocialLinks_module_default.a.link,
    href: "/"
  }, __jsx(facebook, null))), __jsx("li", {
    className: SocialLinks_module_default.a.item
  }, __jsx("a", {
    className: SocialLinks_module_default.a.link,
    href: "/"
  }, __jsx(twitter, null))), __jsx("li", {
    className: SocialLinks_module_default.a.item
  }, __jsx("a", {
    className: SocialLinks_module_default.a.link,
    href: "/"
  }, __jsx(instagram, null))));
}

/* harmony default export */ var SocialLinks_SocialLinks = __webpack_exports__["a"] = (SocialLinks);

/***/ }),

/***/ "HXcA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Footer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_SocialLinks_SocialLinks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("E1mO");
/* harmony import */ var _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("kzqI");
/* harmony import */ var _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_Footer_module_scss__WEBPACK_IMPORTED_MODULE_3__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



function Footer({
  className = ''
}) {
  return __jsx("footer", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.container, className)
  }, __jsx("div", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.info
  }, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoHead
  }, "Estonia, Tallin, Kesklinna linnaosa, Tuukri tn 19-315"),  false && false,  false && false, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.infoCopyRight
  }, "Copyright \xA9 ", new Date().getFullYear(), ". Hivex")), __jsx("nav", null, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.columnHead
  }, "Company"), __jsx("ul", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navIn
  }, __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "howWeWork"
  }, __jsx("a", {
    href: "#howWeWork"
  }, "How we are working")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "technologies"
  }, __jsx("a", {
    href: "#technologies"
  }, "Technologies")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "portfolio"
  }, __jsx("a", {
    href: "#portfolio"
  }, "Portfolio")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "reviews"
  }, __jsx("a", {
    href: "#reviews"
  }, "Reviews")), __jsx("li", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.navItem,
    "data-menuanchor": "contactUs"
  }, __jsx("a", {
    href: "#contactUs"
  }, "Contact us")))), __jsx("div", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.cooperation
  }, __jsx("span", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.columnHead
  }, "Cooperation"), __jsx("a", {
    className: _Footer_module_scss__WEBPACK_IMPORTED_MODULE_3___default.a.cooperationEmailUs,
    href: "mailto:hello@hivex.group"
  }, "hello@hivex.group")));
}

/***/ }),

/***/ "K2gz":
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),

/***/ "Lueu":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "CookiesConfirmation_wrapHeader__3KL9K",
	"wrap": "CookiesConfirmation_wrap__1yCFG",
	"wrapInLeft": "CookiesConfirmation_wrapInLeft__XALEJ",
	"wrapInCenter": "CookiesConfirmation_wrapInCenter__2YbBH",
	"hidden": "CookiesConfirmation_hidden__3yaH_",
	"container": "CookiesConfirmation_container__1BxNj",
	"info": "CookiesConfirmation_info__3cOhH",
	"title": "CookiesConfirmation_title__3-EA8",
	"description": "CookiesConfirmation_description__2FsRZ",
	"agreeBtn": "CookiesConfirmation_agreeBtn__1NTZ6"
};


/***/ }),

/***/ "YHOl":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ Page; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__("K2gz");
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);

// EXTERNAL MODULE: ./components/CookiesConfirmation/CookiesConfirmation.module.scss
var CookiesConfirmation_module = __webpack_require__("Lueu");
var CookiesConfirmation_module_default = /*#__PURE__*/__webpack_require__.n(CookiesConfirmation_module);

// CONCATENATED MODULE: ./components/CookiesConfirmation/CookiesConfirmation.js
var __jsx = external_react_default.a.createElement;



const isCookiesAgreed = 'isCookiesAgreed';

function CookiesConfirmation(props) {
  const {
    0: isCookiesShowed,
    1: setIsCookiesShowed
  } = Object(external_react_["useState"])(false);
  Object(external_react_["useEffect"])(() => {
    const storageAgreementValue = JSON.parse(localStorage.getItem(isCookiesAgreed));

    if (storageAgreementValue === null || storageAgreementValue === false) {
      setIsCookiesShowed(true);
    }
  }, []);

  function confirmCookies() {
    // ToDo: add actual cookies confirmation
    localStorage.setItem(isCookiesAgreed, true);
    setIsCookiesShowed(false);
  }

  return __jsx("div", {
    className: external_classnames_default()(CookiesConfirmation_module_default.a.container, {
      [CookiesConfirmation_module_default.a.hidden]: !isCookiesShowed
    })
  }, __jsx("div", {
    className: CookiesConfirmation_module_default.a.info
  }, __jsx("span", {
    className: CookiesConfirmation_module_default.a.title
  }, "Cookies"), __jsx("p", {
    className: CookiesConfirmation_module_default.a.description
  }, "By continuing to use the site hivex.group you agree to the use of cookies.", __jsx("br", null), "More information can be found in the Cookie Policy")), __jsx("button", {
    className: CookiesConfirmation_module_default.a.agreeBtn,
    onClick: confirmCookies
  }, "Agree"));
}

/* harmony default export */ var CookiesConfirmation_CookiesConfirmation = (CookiesConfirmation);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");

// EXTERNAL MODULE: external "react-transition-group"
var external_react_transition_group_ = __webpack_require__("CSOn");

// EXTERNAL MODULE: ./routes.js
var routes = __webpack_require__("8cHP");

// EXTERNAL MODULE: ./components/NavLink/NavLink.module.scss
var NavLink_module = __webpack_require__("kPE/");
var NavLink_module_default = /*#__PURE__*/__webpack_require__.n(NavLink_module);

// CONCATENATED MODULE: ./components/NavLink/NavLink.js
var NavLink_jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }






function NavLink(props) {
  const {
    route,
    children,
    activeClassname = NavLink_module_default.a.NavLinkActive
  } = props,
        rest = _objectWithoutProperties(props, ["route", "children", "activeClassname"]);

  const router = Object(router_["useRouter"])();
  const isRouteActive = route === undefined ? false : router.asPath === route;
  return NavLink_jsx(routes["Link"], _extends({
    route: route
  }, rest), NavLink_jsx("a", {
    className: external_classnames_default()(NavLink_module_default.a.NavLink, {
      [NavLink_module_default.a.NavLinkActive]: isRouteActive
    })
  }, children));
}
// CONCATENATED MODULE: ./public/assets/dropdown-arrow.svg
function dropdown_arrow_extends() { dropdown_arrow_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return dropdown_arrow_extends.apply(this, arguments); }



var _ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M1 1l4 4 4-4",
  stroke: "#2B2B2B",
  strokeLinecap: "round",
  strokeLinejoin: "round"
});

function SvgDropdownArrow(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", dropdown_arrow_extends({
    width: 10,
    height: 6,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), _ref);
}

/* harmony default export */ var dropdown_arrow = (SvgDropdownArrow);
// EXTERNAL MODULE: ./components/Dropdown/Dropdown.module.scss
var Dropdown_module = __webpack_require__("jY/B");
var Dropdown_module_default = /*#__PURE__*/__webpack_require__.n(Dropdown_module);

// CONCATENATED MODULE: ./components/Dropdown/Dropdown.js
var Dropdown_jsx = external_react_default.a.createElement;







function Dropdown(props) {
  const {
    name,
    subroutes,
    className = ''
  } = props;
  const {
    0: isOpen,
    1: setIsOpen
  } = Object(external_react_["useState"])(false);

  function toggleIsOpen() {
    setIsOpen(prevState => !prevState);
  }

  return Dropdown_jsx("div", {
    className: external_classnames_default()(Dropdown_module_default.a.container, className)
  }, Dropdown_jsx("div", {
    className: external_classnames_default()(Dropdown_module_default.a.name, {
      [Dropdown_module_default.a.dropdownNameShowed]: isOpen
    }),
    onClick: toggleIsOpen
  }, Dropdown_jsx("span", {
    className: Dropdown_module_default.a.title
  }, name), Dropdown_jsx(dropdown_arrow, {
    className: Dropdown_module_default.a.arrow
  })), Dropdown_jsx(external_react_transition_group_["CSSTransition"], {
    className: Dropdown_module_default.a.subroutesContainer,
    in: isOpen,
    classNames: {
      enterDone: Dropdown_module_default.a.dropdownOpen,
      exitDone: Dropdown_module_default.a.dropdownClosed
    },
    timeout: 200
  }, Dropdown_jsx("div", null, Dropdown_jsx("ul", {
    className: Dropdown_module_default.a.dropdownSubroutes,
    onMouseLeave: toggleIsOpen
  }, subroutes.map(subroute => Dropdown_jsx("li", {
    className: Dropdown_module_default.a.dropdownSubroutesItem,
    key: subroute.route
  }, Dropdown_jsx(NavLink, {
    route: subroute.route
  }, Dropdown_jsx("a", null, subroute.name))))))));
}

/* harmony default export */ var Dropdown_Dropdown = (Dropdown);
// EXTERNAL MODULE: ./components/SocialLinks/SocialLinks.js + 3 modules
var SocialLinks = __webpack_require__("E1mO");

// EXTERNAL MODULE: ./components/Button/Button.js
var Button = __webpack_require__("8wsC");

// EXTERNAL MODULE: ./constants/mainConstants.js
var mainConstants = __webpack_require__("cMTN");

// CONCATENATED MODULE: ./public/assets/menu-open.svg
function menu_open_extends() { menu_open_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return menu_open_extends.apply(this, arguments); }



var menu_open_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M23 1H0M23 13H0",
  stroke: "#2B2B2B",
  strokeWidth: 2
});

var _ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M23 7H4.93",
  stroke: "#2B2B2B",
  strokeWidth: 3
});

function SvgMenuOpen(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", menu_open_extends({
    width: 23,
    height: 14,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), menu_open_ref, _ref2);
}

/* harmony default export */ var menu_open = (SvgMenuOpen);
// CONCATENATED MODULE: ./public/assets/menu-close.svg
function menu_close_extends() { menu_close_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return menu_close_extends.apply(this, arguments); }



var menu_close_ref = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M13 13l16 16M13 29l16-16",
  stroke: "#2A2A2A",
  strokeWidth: 3
});

var menu_close_ref2 = /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M19.25 1.588a3.5 3.5 0 013.5 0l14.187 8.19a3.5 3.5 0 011.75 3.031v16.382a3.5 3.5 0 01-1.75 3.03L22.75 40.413a3.5 3.5 0 01-3.5 0l-14.187-8.19a3.5 3.5 0 01-1.75-3.031V12.809a3.5 3.5 0 011.75-3.03L19.25 1.587z",
  stroke: "#2B2B2B"
});

function SvgMenuClose(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", menu_close_extends({
    width: 42,
    height: 42,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), menu_close_ref, menu_close_ref2);
}

/* harmony default export */ var menu_close = (SvgMenuClose);
// EXTERNAL MODULE: ./components/MobileMenu/MobileMenu.module.scss
var MobileMenu_module = __webpack_require__("yxEx");
var MobileMenu_module_default = /*#__PURE__*/__webpack_require__.n(MobileMenu_module);

// CONCATENATED MODULE: ./components/MobileMenu/MobileMenu.js
var MobileMenu_jsx = external_react_default.a.createElement;












function MobileMenu(props) {
  const {
    setIsMenuOpen,
    isMenuOpen
  } = props;

  function toggleMenu() {
    setIsMenuOpen(prevState => !prevState);
  }

  function moveTo(path) {
    fullpage_api.moveTo(path);
    setIsMenuOpen(false);
  }

  return MobileMenu_jsx(external_react_default.a.Fragment, null, MobileMenu_jsx("div", {
    className: external_classnames_default()(MobileMenu_module_default.a.control, MobileMenu_module_default.a.mobileWrap)
  }, MobileMenu_jsx("button", {
    className: MobileMenu_module_default.a.toggleMenuContainer
  }, isMenuOpen ? MobileMenu_jsx(menu_close, {
    onClick: toggleMenu
  }) : MobileMenu_jsx(menu_open, {
    onClick: toggleMenu
  }))), MobileMenu_jsx(external_react_transition_group_["CSSTransition"], {
    className: external_classnames_default()(MobileMenu_module_default.a.container, MobileMenu_module_default.a.mobileWrap),
    classNames: {
      enterDone: MobileMenu_module_default.a.menuOpen,
      exitDone: MobileMenu_module_default.a.menuClosed
    },
    in: isMenuOpen,
    timeout: 0
  }, MobileMenu_jsx("div", null, MobileMenu_jsx("nav", {
    className: MobileMenu_module_default.a.nav
  }, MobileMenu_jsx("ul", {
    className: MobileMenu_module_default.a.navIn
  }, MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(NavLink, {
    route: "/"
  }, MobileMenu_jsx("a", null, "Home"))), MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(NavLink, {
    route: "/work-with-us"
  }, MobileMenu_jsx("a", null, "Cooperation with us"))), MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(Dropdown_Dropdown, {
    name: "Technologies",
    className: MobileMenu_module_default.a.dropdown,
    subroutes: mainConstants["b" /* TechnologiesSubroutes */]
  })), MobileMenu_jsx("li", {
    className: MobileMenu_module_default.a.navItem
  }, MobileMenu_jsx(NavLink, {
    route: "/company"
  }, MobileMenu_jsx("a", null, "Company"))))), MobileMenu_jsx(SocialLinks["a" /* default */], null), MobileMenu_jsx(Button["a" /* default */], {
    className: MobileMenu_module_default.a.btn,
    onClick: () => moveTo('contactUs')
  }, "Start work with us"))), MobileMenu_jsx(external_react_transition_group_["CSSTransition"], {
    className: external_classnames_default()(MobileMenu_module_default.a.grayWrapper, MobileMenu_module_default.a.mobileWrap),
    classNames: {
      enterDone: MobileMenu_module_default.a.wrapperOpen,
      exitDone: MobileMenu_module_default.a.wrapperClosed
    },
    in: isMenuOpen,
    timeout: 0
  }, MobileMenu_jsx("div", {
    onClick: toggleMenu
  })));
}

/* harmony default export */ var MobileMenu_MobileMenu = (MobileMenu);
// CONCATENATED MODULE: ./public/assets/logo.svg
function logo_extends() { logo_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return logo_extends.apply(this, arguments); }



var logo_ref = /*#__PURE__*/external_react_["createElement"]("g", {
  clipPath: "url(#logo_svg__clip0)"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M44.436 11.953h-3.692V27.02h3.692v-6.176h6.79v6.176h3.692V11.953h-3.691v5.75h-6.791v-5.75zM61.736 11.953h-3.692V27.02h3.692V11.953zM67.476 11.953h-4.204l5.713 15.067h4.823l5.767-15.067H75.37l-3.961 11.42-3.934-11.42zM104.663 27.02h4.338l-5.309-7.746 4.959-7.321h-4.366l-3.099 4.978-3.152-4.978h-4.339l4.958 7.347-5.308 7.72h4.365l3.476-5.297 3.477 5.297zM92.212 11.953H81.11v3.141h11.102v-3.141zM90.245 17.836H81.11v3.142h9.135v-3.142zM92.212 23.88H81.11v3.14h11.102v-3.14z",
  fill: "#000"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M22.69 17.597H11.48v3.3h11.21v-3.3z",
  fill: "url(#logo_svg__paint0_linear)"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M26.489 5.35v24.838h-3.8v-9.29H11.48v14.88L17.084 39l17.085-9.77V9.743l-7.68-4.392z",
  fill: "#0255FB"
}), /*#__PURE__*/external_react_["createElement"]("path", {
  d: "M11.48 17.597h11.21V3.195L17.083 0 0 9.743V29.23l7.707 4.393V8.89h3.772v8.706z",
  fill: "#0255FB"
}));

var logo_ref2 = /*#__PURE__*/external_react_["createElement"]("defs", null, /*#__PURE__*/external_react_["createElement"]("linearGradient", {
  id: "logo_svg__paint0_linear",
  x1: 11.48,
  y1: 19.253,
  x2: 22.698,
  y2: 19.253,
  gradientUnits: "userSpaceOnUse"
}, /*#__PURE__*/external_react_["createElement"]("stop", {
  stopColor: "#929497"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.274,
  stopColor: "#B9BABC"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.589,
  stopColor: "#DFDFE0"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 0.842,
  stopColor: "#F6F7F7"
}), /*#__PURE__*/external_react_["createElement"]("stop", {
  offset: 1,
  stopColor: "#fff"
})), /*#__PURE__*/external_react_["createElement"]("clipPath", {
  id: "logo_svg__clip0"
}, /*#__PURE__*/external_react_["createElement"]("path", {
  fill: "#fff",
  d: "M0 0h109v39H0z"
})));

function SvgLogo(props) {
  return /*#__PURE__*/external_react_["createElement"]("svg", logo_extends({
    width: 109,
    height: 39,
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, props), logo_ref, logo_ref2);
}

/* harmony default export */ var logo = (SvgLogo);
// EXTERNAL MODULE: ./components/Header/Header.module.scss
var Header_module = __webpack_require__("yzvi");
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);

// CONCATENATED MODULE: ./components/Header/Header.js
var Header_jsx = external_react_default.a.createElement;









function Header(props) {
  const {
    0: isMenuOpen,
    1: setIsMenuOpen
  } = Object(external_react_["useState"])(false);
  const {
    route
  } = Object(router_["useRouter"])();

  function openMenu() {
    setIsMenuOpen(true);
  }

  const isMainPage = route === '/';
  return Header_jsx("header", {
    className: external_classnames_default()(Header_module_default.a.container, {
      [Header_module_default.a.fixedToTop]: isMainPage
    })
  }, Header_jsx("div", {
    className: Header_module_default.a.containerIn
  }, Header_jsx("div", {
    className: Header_module_default.a.left
  }, Header_jsx("a", {
    className: Header_module_default.a.logo,
    href: "/"
  }, Header_jsx(logo, null))), Header_jsx("nav", {
    className: Header_module_default.a.nav
  }, Header_jsx("ul", {
    className: Header_module_default.a.navIn
  }, Header_jsx("li", {
    className: Header_module_default.a.navItem
  }, Header_jsx(NavLink, {
    route: "/"
  }, Header_jsx("a", null, "Home"))), Header_jsx("li", {
    className: Header_module_default.a.navItem
  }, Header_jsx(NavLink, {
    route: "/interaction-models/dedicated-team"
  }, Header_jsx("a", null, "Cooperate with us"))), Header_jsx("li", {
    className: Header_module_default.a.navItem
  }, Header_jsx(NavLink, {
    route: "/work-with-us"
  }, Header_jsx("a", null, "Company")))))), Header_jsx(MobileMenu_MobileMenu, {
    isMenuOpen: isMenuOpen,
    setIsMenuOpen: setIsMenuOpen
  }));
}
// EXTERNAL MODULE: ./components/Page/Page.module.scss
var Page_module = __webpack_require__("d1WD");
var Page_module_default = /*#__PURE__*/__webpack_require__.n(Page_module);

// CONCATENATED MODULE: ./components/Page/Page.js
var Page_jsx = external_react_default.a.createElement;





function SvgBackground() {
  return Page_jsx("div", {
    className: "svg-animated-wrap"
  }, Page_jsx("svg", {
    className: "svg-animated-rb",
    width: "1008",
    height: "832",
    viewBox: "0 0 1008 832",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, Page_jsx("g", {
    opacity: "0.2",
    filter: "url(#filter0_f)"
  }, Page_jsx("path", {
    d: "M585.369 198.306C709.163 322.101 925.395 431.558 801.6 555.353C677.805 679.148 589.558 509.463 465.764 385.668C341.969 261.873 6.13289 368.648 129.928 244.853C253.723 121.058 461.574 74.5115 585.369 198.306Z",
    fill: "url(#paint0_radial)"
  })), Page_jsx("defs", null, Page_jsx("filter", {
    id: "filter0_f",
    x: "-31.7008",
    y: "-141.59",
    width: "1039.16",
    height: "1039.16",
    filterUnits: "userSpaceOnUse",
    colorInterpolationFilters: "sRGB"
  }, Page_jsx("feFlood", {
    floodOpacity: 0,
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "62",
    result: "effect1_foregroundBlur"
  })), Page_jsx("radialGradient", {
    id: "paint0_radial",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(440.519 463.823) rotate(45) scale(316.996 316.996)"
  }, Page_jsx("stop", {
    stopColor: "#0255FB"
  }, Page_jsx("animate", {
    attributeName: "stop-color",
    values: "#0255FB; #7A5FFF; #FB02B5; #0255FB",
    dur: "4s",
    repeatCount: "indefinite"
  }))))), Page_jsx("svg", {
    className: "svg-animated-lt",
    width: "758",
    height: "356",
    viewBox: "0 0 558 356",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, Page_jsx("g", {
    opacity: "0.6",
    filter: "url(#filter0_f)"
  }, Page_jsx("path", {
    d: "M438.056 23.1961C411.673 90.1082 60.5684 115.55 202.686 171.586C344.804 227.623 609.047 113.122 635.43 46.2099C661.813 -20.7023 567.992 -120.372 425.874 -176.408C283.756 -232.444 464.439 -43.7161 438.056 23.1961Z",
    fill: "url(#paint0_radial)"
  })), Page_jsx("defs", null, Page_jsx("filter", {
    id: "filter0_f",
    x: "0.220581",
    y: "-355.386",
    width: "808.374",
    height: "710.667",
    filterUnits: "userSpaceOnUse",
    "color-interpolation-filters": "sRGB"
  }, Page_jsx("feFlood", {
    "flood-opacity": "0",
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "84.3607",
    result: "effect1_foregroundBlur"
  })), Page_jsx("radialGradient", {
    id: "filter0_f",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(378.103 -55.2526) rotate(111.519) scale(164.392 349.16)"
  }, Page_jsx("stop", {
    "stop-color": "#FB02B5"
  }), Page_jsx("stop", {
    offset: "0.765625",
    "stop-color": "#0255FB"
  })))), Page_jsx("svg", {
    className: "svg-animated-lb",
    width: "508",
    height: "456",
    viewBox: "0 0 508 456",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }, Page_jsx("g", {
    opacity: "0.7",
    filter: "url(#filter0_f)"
  }, Page_jsx("path", {
    d: "M348.023 248.622C383.869 282.291 426.676 255.133 393.059 290.923C359.443 326.713 303.132 328.433 267.286 294.764C231.441 261.095 236.744 266.23 270.361 230.44C303.977 194.65 312.177 214.953 348.023 248.622Z",
    fill: "url(#paint0_radial)"
  })), Page_jsx("g", {
    opacity: "0.7",
    filter: "url(#filter1_f)"
  }, Page_jsx("path", {
    d: "M238.208 115.535C274.218 149.358 252.976 186.474 219.378 222.244C185.78 258.014 152.549 278.893 116.539 245.07C80.5287 211.246 140.538 215.519 174.136 179.749C207.734 143.979 202.197 81.7111 238.208 115.535Z",
    fill: "url(#paint1_radial)"
  })), Page_jsx("defs", null, Page_jsx("filter", {
    id: "filter0_f",
    x: "136.928",
    y: "85.562",
    width: "370.679",
    height: "369.771",
    filterUnits: "userSpaceOnUse",
    colorInterpolationFilters: "sRGB"
  }, Page_jsx("feFlood", {
    floodOpacity: 0,
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "49.8033",
    result: "effect1_foregroundBlur"
  })), Page_jsx("filter", {
    id: "filter1_f",
    x: "0.393448",
    y: "0.393448",
    width: "380.667",
    height: "384.903",
    filterUnits: "userSpaceOnUse",
    colorInterpolationFilters: "sRGB"
  }, Page_jsx("feFlood", {
    floodOpacity: 0,
    result: "BackgroundImageFix"
  }), Page_jsx("feBlend", {
    mode: "normal",
    in: "SourceGraphic",
    in2: "BackgroundImageFix",
    result: "shape"
  }), Page_jsx("feGaussianBlur", {
    stdDeviation: "49.8033",
    result: "effect1_foregroundBlur"
  })), Page_jsx("radialGradient", {
    id: "paint0_radial",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(328.155 229.96) rotate(43.2064) scale(112.402 112.227)"
  }, Page_jsx("stop", {
    stopColor: "#0255FB"
  }), Page_jsx("stop", {
    offset: "1",
    stopColor: "#02D4FB"
  })), Page_jsx("radialGradient", {
    id: "paint1_radial",
    cx: "0",
    cy: "0",
    r: "1",
    gradientUnits: "userSpaceOnUse",
    gradientTransform: "translate(177.373 180.302) rotate(43.2064) scale(112.917 112.164)"
  }, Page_jsx("stop", {
    stopColor: "#0255FB"
  }), Page_jsx("stop", {
    offset: "1",
    stopColor: "#02D4FB"
  })))));
}

function Page({
  children
}) {
  return Page_jsx(external_react_default.a.Fragment, null, Page_jsx(Header, null), Page_jsx(CookiesConfirmation_CookiesConfirmation, null), Page_jsx("div", {
    className: Page_module_default.a.container
  }, children), Page_jsx(SvgBackground, null));
}

/***/ }),

/***/ "bmoy":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ReadMoreButton_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("61VP");
/* harmony import */ var _ReadMoreButton_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_ReadMoreButton_module_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




function ReadMoreButton({
  link = '/',
  className = ''
}) {
  return __jsx("a", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_ReadMoreButton_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.container, className),
    href: link
  }, "Read more", ' ', __jsx("img", {
    className: _ReadMoreButton_module_scss__WEBPACK_IMPORTED_MODULE_2___default.a.image,
    src: "/assets/arrow-long.svg",
    alt: ""
  }));
}

/* harmony default export */ __webpack_exports__["a"] = (ReadMoreButton);

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cMTN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TechnologiesSubroutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InteractionModelTypes; });
const TechnologiesSubroutes = [{
  name: 'Web-Frontend',
  route: '/technologies/web-frontend'
}, {
  name: 'Web-Backend',
  route: '/technologies/web-backend'
}, {
  name: 'Mobile',
  route: '/technologies/mobile'
}, {
  name: 'DevOps',
  route: '/technologies/dev-ops'
}, {
  name: 'Machine Learning & AI',
  route: '/technologies/machine-learning'
}, {
  name: 'Quality Assurance',
  route: '/technologies/qa'
}, {
  name: 'Big data',
  route: '/technologies/big-data'
}];
const InteractionModelTypes = [{
  link: '/interaction-models/dedicated-team',
  value: 'Dedicated team'
}, {
  link: '/interaction-models/hourly-payment',
  value: 'Hourly payment'
}];

/***/ }),

/***/ "d1WD":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Page_wrapHeader__SK-tE",
	"wrap": "Page_wrap__3PV08",
	"container": "Page_container__3nCVR",
	"wrapInLeft": "Page_wrapInLeft__2gHgy",
	"wrapInCenter": "Page_wrapInCenter__nDA6O",
	"hidden": "Page_hidden__3bvai"
};


/***/ }),

/***/ "jY/B":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Dropdown_wrapHeader__3WtZY",
	"wrap": "Dropdown_wrap__cjTQs",
	"wrapInLeft": "Dropdown_wrapInLeft__nf79s",
	"wrapInCenter": "Dropdown_wrapInCenter__1jHGs",
	"hidden": "Dropdown_hidden__OmYdM",
	"container": "Dropdown_container__2v6C_",
	"name": "Dropdown_name__2x8wH",
	"title": "Dropdown_title__1lh20",
	"arrow": "Dropdown_arrow__3bX6Z",
	"dropdownNameShowed": "Dropdown_dropdownNameShowed__1wLYX",
	"subroutesContainer": "Dropdown_subroutesContainer__3Nib8",
	"dropdownSubroutes": "Dropdown_dropdownSubroutes__2EdTy",
	"dropdownSubroutesItem": "Dropdown_dropdownSubroutesItem__ShZOY",
	"dropdownClosed": "Dropdown_dropdownClosed__3bVYz",
	"dropdownOpen": "Dropdown_dropdownOpen__36wL4"
};


/***/ }),

/***/ "kPE/":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "NavLink_wrapHeader__O8y3w",
	"wrap": "NavLink_wrap__2YDsA",
	"wrapInLeft": "NavLink_wrapInLeft__1auL8",
	"wrapInCenter": "NavLink_wrapInCenter__104gq",
	"hidden": "NavLink_hidden__WDNC5",
	"NavLink": "NavLink_NavLink__18jpR",
	"NavLinkActive": "NavLink_NavLinkActive__1fXFl"
};


/***/ }),

/***/ "kvya":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "SocialLinks_wrapHeader__3dMQv",
	"wrap": "SocialLinks_wrap__16FE5",
	"wrapInLeft": "SocialLinks_wrapInLeft__261iX",
	"wrapInCenter": "SocialLinks_wrapInCenter__2zq-h",
	"hidden": "SocialLinks_hidden__2PcE6",
	"container": "SocialLinks_container__xipb6",
	"item": "SocialLinks_item__308gW",
	"link": "SocialLinks_link__3G8X9"
};


/***/ }),

/***/ "kzqI":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Footer_wrapHeader__kqZCB",
	"wrap": "Footer_wrap__372cF",
	"wrapInLeft": "Footer_wrapInLeft__16p1G",
	"wrapInCenter": "Footer_wrapInCenter__2jYMB",
	"hidden": "Footer_hidden__1f_3V",
	"container": "Footer_container__l2D4g",
	"info": "Footer_info__3Ojhd",
	"infoHead": "Footer_infoHead__17VVW",
	"infoMapView": "Footer_infoMapView__3wk9y",
	"infoCopyRight": "Footer_infoCopyRight__uAq8I",
	"navIn": "Footer_navIn__Yf_fX",
	"navItem": "Footer_navItem__3nSJx",
	"columnHead": "Footer_columnHead__1ist4",
	"cooperationEmailUs": "Footer_cooperationEmailUs__3cnjN",
	"cooperation": "Footer_cooperation__3rRQW",
	"cooperationItem": "Footer_cooperationItem__2qUCS"
};


/***/ }),

/***/ "sgxf":
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":0,\"titleImage\":\"/assets/cases/titleImage.png\",\"key\":\"asomaker\",\"title\":\"ASOmaker\",\"projectDescription\":\"Conversion product pages related to the constructor of insurance products\",\"linkTitle\":\"asomaker.com\",\"linkToResource\":\"https://asomaker.com/\",\"caseDescription\":\"Rich and diverse experience as well as further development of different engagement forms allow to assess the significance of the development forms. The task of the organization, particularly the further development of different engagement forms results in the process of implementation and modernization of positions occupied by the participants in relation to targets. \",\"year\":2020,\"model\":\"Dedicated team\",\"term\":\"12 months\",\"challenge\":\"We've been developing asomaker.com - a project with the increased level of complexity due to the following features:\\n- technical complexity: this single-page application id based on the latest technological solutions such as React JS, MOBX, gulp, webpack and other;\\n- an integration complexity: there were plenty of dependencies from the client's application (it was needed to interact a lot with the related entities)\\n- management complexities: developers faced the problem of a frequently changing requirements (a common occurrence in new products, as many hypotheses are tested, then rejected and everything needs to be redone, starting from the beginning )\",\"technologies\":[{\"id\":0,\"title\":\"Vue.js\"},{\"id\":1,\"title\":\"Ember\"},{\"id\":2,\"title\":\"React js\"},{\"id\":3,\"title\":\"Svetle\"},{\"id\":4,\"title\":\"Angular js\"}],\"images\":[{\"id\":0,\"src\":\"/assets/cases/firstSlide.png\"},{\"id\":1,\"src\":\"/assets/cases/secondSlide.png\"},{\"id\":2,\"src\":\"/assets/cases/thirdSlide.png\"}],\"temporaryNumbers\":[{\"id\":0,\"title\":\"some numbers about the project\",\"count\":\"214\"},{\"id\":1,\"title\":\"some numbers about the project\",\"count\":\"86+\"},{\"id\":2,\"title\":\"some numbers about the project\",\"count\":\"214\"},{\"id\":3,\"title\":\"some numbers about the project\",\"count\":\"174\"}]},{\"id\":1,\"titleImage\":\"/assets/cases/titleImage.png\",\"key\":\"asomaker-1\",\"title\":\"Second ASOmaker\",\"projectDescription\":\"Conversion product pages related to the constructor of insurance products\",\"linkTitle\":\"asomaker.com\",\"linkToResource\":\"https://asomaker.com/\",\"caseDescription\":\"Rich and diverse experience as well as further development of different engagement forms allow to assess the significance of the development forms. The task of the organization, particularly the further development of different engagement forms results in the process of implementation and modernization of positions occupied by the participants in relation to targets. \",\"year\":2020,\"model\":\"Dedicated team\",\"term\":\"12 months\",\"challenge\":\"We've been developing asomaker.com - a project with the increased level of complexity due to the following features:\\n- technical complexity: this single-page application id based on the latest technological solutions such as React JS, MOBX, gulp, webpack and other;\\n- an integration complexity: there were plenty of dependencies from the client's application (it was needed to interact a lot with the related entities)\\n- management complexities: developers faced the problem of a frequently changing requirements (a common occurrence in new products, as many hypotheses are tested, then rejected and everything needs to be redone, starting from the beginning )\",\"technologies\":[{\"id\":0,\"title\":\"Vue.js\"},{\"id\":1,\"title\":\"Ember\"},{\"id\":2,\"title\":\"React js\"},{\"id\":3,\"title\":\"Svetle\"},{\"id\":4,\"title\":\"Angular js\"}],\"images\":[{\"id\":0,\"src\":\"/assets/cases/firstSlide.png\"},{\"id\":1,\"src\":\"/assets/cases/secondSlide.png\"},{\"id\":2,\"src\":\"/assets/cases/thirdSlide.png\"}],\"temporaryNumbers\":[{\"id\":0,\"title\":\"some numbers about the project\",\"count\":\"214\"},{\"id\":1,\"title\":\"some numbers about the project\",\"count\":\"86+\"},{\"id\":2,\"title\":\"some numbers about the project\",\"count\":\"214\"},{\"id\":3,\"title\":\"some numbers about the project\",\"count\":\"174\"}]},{\"id\":2,\"titleImage\":\"/assets/cases/titleImage.png\",\"key\":\"asomaker-2\",\"title\":\"Third ASOmaker?\",\"projectDescription\":\"Conversion product pages related to the constructor of insurance products\",\"linkTitle\":\"asomaker.com\",\"linkToResource\":\"https://asomaker.com/\",\"caseDescription\":\"Rich and diverse experience as well as further development of different engagement forms allow to assess the significance of the development forms. The task of the organization, particularly the further development of different engagement forms results in the process of implementation and modernization of positions occupied by the participants in relation to targets. \",\"year\":2020,\"model\":\"Dedicated team\",\"term\":\"12 months\",\"challenge\":\"We've been developing asomaker.com - a project with the increased level of complexity due to the following features:\\n- technical complexity: this single-page application id based on the latest technological solutions such as React JS, MOBX, gulp, webpack and other;\\n- an integration complexity: there were plenty of dependencies from the client's application (it was needed to interact a lot with the related entities)\\n- management complexities: developers faced the problem of a frequently changing requirements (a common occurrence in new products, as many hypotheses are tested, then rejected and everything needs to be redone, starting from the beginning )\",\"technologies\":[{\"id\":0,\"title\":\"Vue.js\"},{\"id\":1,\"title\":\"Ember\"},{\"id\":2,\"title\":\"React js\"},{\"id\":3,\"title\":\"Svetle\"},{\"id\":4,\"title\":\"Angular js\"}],\"images\":[{\"id\":0,\"src\":\"/assets/cases/firstSlide.png\"},{\"id\":1,\"src\":\"/assets/cases/secondSlide.png\"},{\"id\":2,\"src\":\"/assets/cases/thirdSlide.png\"}],\"temporaryNumbers\":[{\"id\":0,\"title\":\"some numbers about the project\",\"count\":\"214\"},{\"id\":1,\"title\":\"some numbers about the project\",\"count\":\"86+\"},{\"id\":2,\"title\":\"some numbers about the project\",\"count\":\"214\"},{\"id\":3,\"title\":\"some numbers about the project\",\"count\":\"174\"}]}]");

/***/ }),

/***/ "tUy9":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Cases_wrapHeader__3oDOw",
	"wrap": "Cases_wrap__3K-Yw",
	"wrapInLeft": "Cases_wrapInLeft__3Eapv",
	"wrapInCenter": "Cases_wrapInCenter__1ZWuR",
	"hidden": "Cases_hidden__2rq2B",
	"hideOnMobile": "Cases_hideOnMobile__2CF_H",
	"hideOnDesktop": "Cases_hideOnDesktop__1CNSu",
	"container": "Cases_container__1wfgd",
	"head": "Cases_head__2DqWm",
	"headInfo": "Cases_headInfo__tVDF4",
	"mobileHeadInfo": "Cases_mobileHeadInfo___NWgd",
	"mobileHeadInfoTitle": "Cases_mobileHeadInfoTitle__Xx6kt",
	"mobileHeadInfoData": "Cases_mobileHeadInfoData__37syD",
	"mobileHeadInfoDataBlock": "Cases_mobileHeadInfoDataBlock__36KAm",
	"headDescription": "Cases_headDescription__1nXd1",
	"caseTitle": "Cases_caseTitle__F8WpS",
	"projectDescription": "Cases_projectDescription__3coiL",
	"linkToResource": "Cases_linkToResource__1xI6e",
	"caseDescription": "Cases_caseDescription__x0Sjp",
	"year": "Cases_year__3-eWj",
	"infoItemContainer": "Cases_infoItemContainer__2fBGX",
	"infoItemTitle": "Cases_infoItemTitle__3CVpk",
	"infoItemValue": "Cases_infoItemValue__3wfAV",
	"infoItemList": "Cases_infoItemList__3K1xr",
	"caseImagesList": "Cases_caseImagesList__f9As_",
	"caseImage": "Cases_caseImage__1LsrF",
	"challengeContainer": "Cases_challengeContainer__3VYAp",
	"challengeTitle": "Cases_challengeTitle__3cFlb",
	"challengeDescription": "Cases_challengeDescription__174Dq",
	"numbersContainer": "Cases_numbersContainer__2tEH5",
	"numbersItem": "Cases_numbersItem__1hqOZ",
	"numbersItemTitle": "Cases_numbersItemTitle__3vYDy",
	"numbersItemDescription": "Cases_numbersItemDescription__3a1OJ",
	"caseList": "Cases_caseList__3R8lV",
	"caseListContainer": "Cases_caseListContainer__2Kgcp",
	"caseListTitle": "Cases_caseListTitle__1Fhv9",
	"caseListItem": "Cases_caseListItem__1-AlE",
	"caseListItemContainer": "Cases_caseListItemContainer__2nJ-k",
	"caseListItemImageWrapper": "Cases_caseListItemImageWrapper___74mq",
	"caseListItemImage": "Cases_caseListItemImage__1lTNv",
	"caseListItemTitle": "Cases_caseListItemTitle__1ijdU",
	"caseListItemDescription": "Cases_caseListItemDescription__27JJE",
	"footer": "Cases_footer__2IjZ7"
};


/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "yxEx":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "MobileMenu_wrapHeader__2cMaN",
	"wrap": "MobileMenu_wrap__29YsL",
	"wrapInLeft": "MobileMenu_wrapInLeft__2Qswu",
	"wrapInCenter": "MobileMenu_wrapInCenter__2xvWJ",
	"hidden": "MobileMenu_hidden__3Y4b1",
	"mobileWrap": "MobileMenu_mobileWrap__1Tdg8",
	"container": "MobileMenu_container__1qe4-",
	"control": "MobileMenu_control__3vIul",
	"grayWrapper": "MobileMenu_grayWrapper__1b9hA",
	"nav": "MobileMenu_nav__9t5Hq",
	"navIn": "MobileMenu_navIn__2eEGQ",
	"navItem": "MobileMenu_navItem__nbeh4",
	"btn": "MobileMenu_btn__3a6zW",
	"toggleMenuContainer": "MobileMenu_toggleMenuContainer__3jr1X",
	"dropdown": "MobileMenu_dropdown__30B6O",
	"menuOpen": "MobileMenu_menuOpen__737-b",
	"menuClosed": "MobileMenu_menuClosed__1t8ar",
	"wrapperOpen": "MobileMenu_wrapperOpen__1b_C0",
	"wrapperClosed": "MobileMenu_wrapperClosed__2ZRBW"
};


/***/ }),

/***/ "yzvi":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"wrapHeader": "Header_wrapHeader__3bEbQ",
	"wrap": "Header_wrap__3c1CF",
	"containerIn": "Header_containerIn__21gIb",
	"wrapInLeft": "Header_wrapInLeft__2Nb_B",
	"wrapInCenter": "Header_wrapInCenter__1pi4U",
	"hidden": "Header_hidden__y5jGu",
	"container": "Header_container__3cXlC",
	"fixedToTop": "Header_fixedToTop__1j-Fa",
	"left": "Header_left__jkv05",
	"nav": "Header_nav__2SG38",
	"navIn": "Header_navIn__1iiLj",
	"navItem": "Header_navItem__3Fat9"
};


/***/ })

/******/ });