Vlad WebDev to Everyone (11:57 AM)
setTimeout(() => {
  console.log(from setTimeout);
});
Promise.resolve().then(() => {console.log('promise resolve')});
Vlad WebDev to Everyone (11:59 AM)
const runWhileLoopForNSeconds = ms => {
  const start = Date.now();
  while (Date.now() - start < ms * 1000) {}
}

const main = () => {
  console.log('A')
  setTimeout(function exec() {
    console.log('B')
  }, 0)
  runWhileLoopForNSeconds(3)
  console.log('C')
}

main();
Vlad WebDev to Everyone (12:04 PM)
var data1;
let data2;
const data3;

console.info(data1, data2, data3)
function sayHi() {
  console.log(name);
  console.log(age);
  var name = "Lydia";
  let age = 21;
}

sayHi();
Vlad WebDev to Everyone (12:07 PM)
class EventEmitter {
  // ... реализовать функционал
}

const func1 = (data) => {
  console.log(`func1 data:`, data);
}

const func2 = (data) => {
  console.log(`func2 data:`, data);
}

const ee = new EventEmitter();

ee.on(`event`, func1);
ee.on(`event`, func2);
ee.emit(`event`, `hello`);// вывод в консоль func1 data: hello и func2 data: hello

ee.emit(`event`, `world`);// вывод в консоль объекта для func1 и func2
ee.off(`event`, func1);
ee.emit(`event`, `hello`);// выводится только func2 data: hello
Vlad WebDev to Everyone (12:19 PM)
https://www.typescriptlang.org/play?#code/MYewdgzgLgBADgUwE4XDAvDA3jGAoXXMAQwFsEAuGAcgGEALAV2AGsYA5EJJASwmoA0BQsQDmlGADYAHEIC+Abjx4A9CpgAVAPIARLVUDsIIAYQQEIggDhBA-CBmDZwAIggPhBATCCnAMiB4AZozDAoPNOKgtACMAKwA1YgAbRgQAChBQqmIwAE8BeCQQOHYyCWSUgEok1OxhXCQEKEYkMBgEkIBtOEzs3IBdJTllAODwqJjYxBRwdOoScmoChSA
  Vlad WebDev to Everyone (12:19 PM)
const person = {
  name: 'Chuck Norris',
  age: 68,
};

// TODO: затипизировать
function getObjValue(obj: any, propName: any): any {
  return obj[propName];
}

getObjValue(person, 'name');
Vlad WebDev to Everyone (12:26 PM)
const students: Array<{ name: string, age: number }> = [
  { name: 'Kyle', age: 17 },
  { name: 'John', age: 17 },
  { name: 'Alex', age: 18 },
  { name: 'Alex', age: 24 },
];

const teachers: Array<{ name: string, spec: string }> = [
  { name: 'Anna', spec: 'math' },
  { name: 'Olga', spec: 'lang' },
  { name: 'Anna', spec: 'chem' },
];

// TODO: реализовать и затипизировать
function find(collection: any, propName: any, value: any) {
  // ...
}

const resultStd = find(students, 'age', 17);
const resultTch = find(teachers, 'spec', 'math');

console.info('resultStd', resultStd);
console.info('resultTch', resultTch);
Vlad WebDev to Everyone (12:37 PM)
class Queue {
  // TODO: реализация
}

// -----------------------------------------------

const q = new Queue();

const first = (cb) => {
  setTimeout(() => cb('async first in action'), 500)
}
const second = (cb) => {
  setTimeout(() => cb('async second in action'), 1000)
}
const third = () => {
  console.info('sync third in action')
}
const last = (cb) => {
  setTimeout(() => cb('async last in action'), 1000)
}

// -----------------------------------------------

q.add(first, data => {
  console.info(data);
});
q.add(second, data => {
  console.info(data);
});
q.add(third);
setTimeout(() => {
  q.add(last, data => {
    console.info(data);
  });
}, 600)
setTimeout(() => {
  q.add(third);
}, 5000)
setTimeout(() => {
  q.add(first, data => {
    console.info(data);
  });
}, 6000)
