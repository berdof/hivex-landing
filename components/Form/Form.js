import classnames from 'classnames';

import Input from 'components/Input/Input';
import Button from 'components/Button/Button';

import styles from './Form.module.scss';

export default function Form({ formClassName = '' }) {
  return (
    <div className={styles.container}>
      <article className={styles.textBlock}>
        <h2 className={styles.textBlockTitle}>Leave a request</h2>
        <p className={styles.textBlockDescription}>
          Tell us about the project and put your questions - we will answer ASAP
        </p>
      </article>
      <form className={classnames(styles.form, formClassName)}>
        <div className={styles.col}>
          <div className={styles.row}>
            <div className={styles.col}>
              <Input placeholder="Name" id="name" required={true} type="text" />
            </div>
            <div className={styles.col}>
              <Input
                placeholder="Email"
                id="email"
                required={true}
                type="email"
              />
            </div>
          </div>
          <div className={styles.row}>
            <Input
              placeholder="Subject"
              id="subject"
              required={true}
              type="text"
            />
          </div>
        </div>
        <div className={styles.col}>
          <Input
            placeholder="Project description"
            type="textarea"
            rows={5}
            id="description"
            required={true}
          />
        </div>
        <Button className={styles.confirmBtn}>Send</Button>
      </form>
    </div>
  );
}
