import React from 'react';
import { useRouter } from 'next/router';
import classnames from 'classnames';

import { Link } from 'routes';

import styles from './Tabs.module.scss';

function Tab(props) {
  const { tab } = props;
  const router = useRouter();

  const isTabActive =
    tab.link === undefined ? false : router.asPath === tab.link;

  return (
    <Link route={tab.link}>
      <a
        className={classnames(styles.link, {
          [styles.linkActive]: isTabActive,
        })}
      >
        <div className={styles.tabContainer}>
          <span className={styles.tabValue}>{tab.value}</span>
          <div
            className={classnames(styles.tabUnderline, {
              [styles.tabUnderlineShowed]: isTabActive,
            })}
          />
        </div>
      </a>
    </Link>
  );
}

export default Tab;
