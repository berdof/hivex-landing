import React from 'react';
import classnames from 'classnames';

import styles from './Tabs.module.scss';
import Tab from './Tab';

function Tabs(props) {
  const { tabs = [], className = '' } = props;
  return (
    <div className={classnames(styles.container, className)}>
      {tabs.map((item) => (
        <Tab key={item.link} tab={item} />
      ))}
    </div>
  );
}

export default Tabs;
