import React from 'react';
import { useRouter } from 'next/router';
import { Link } from 'routes';
import classnames from 'classnames';

import styles from './NavLink.module.scss';

export default function NavLink(props) {
  const {
    route,
    children,
    activeClassname = styles.NavLinkActive,
    ...rest
  } = props;
  const router = useRouter();
  const isRouteActive = route === undefined ? false : router.asPath === route;

  return (
    <Link route={route} {...rest}>
      <a
        className={classnames(styles.NavLink, {
          [styles.NavLinkActive]: isRouteActive,
        })}
      >
        {children}
      </a>
    </Link>
  );
}
