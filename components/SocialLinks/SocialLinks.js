import React from 'react';
import FacebookIcon from 'public/assets/facebook.svg';
import TwitterIcon from 'public/assets/twitter.svg';
import InstagramIcon from 'public/assets/instagram.svg';

import styles from './SocialLinks.module.scss';

function SocialLinks(props) {
  return (
    <ul className={styles.container}>
      <li className={styles.item}>
        <a className={styles.link} href="/">
          <FacebookIcon/>
        </a>
      </li>
      <li className={styles.item}>
        <a className={styles.link} href="/">
          <TwitterIcon/>
        </a>
      </li>
      <li className={styles.item}>
        <a className={styles.link} href="/">
          <InstagramIcon/>
        </a>
      </li>
    </ul>
  );
}

export default SocialLinks;
