import React from 'react';
import classnames from 'classnames';
import styles from './ReadMoreButton.module.scss';

function ReadMoreButton({ link = '/', className = '' }) {
  return (
    <a className={classnames(styles.container, className)} href={link}>
      Read more{' '}
      <img className={styles.image} src="/assets/arrow-long.svg" alt="" />
    </a>
  );
}

export default ReadMoreButton;
