import React, { useState } from 'react';

import CookiesConfirmation from 'components/CookiesConfirmation/CookiesConfirmation';
import Header from 'components/Header/Header';

import styles from './Page.module.scss';

function SvgBackground() {
  return (
    <div className="svg-animated-wrap">
      <svg
        className="svg-animated-rb"
        width="1008"
        height="832"
        viewBox="0 0 1008 832"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g opacity="0.2" filter="url(#filter0_f)">
          <path
            d="M585.369 198.306C709.163 322.101 925.395 431.558 801.6 555.353C677.805 679.148 589.558 509.463 465.764 385.668C341.969 261.873 6.13289 368.648 129.928 244.853C253.723 121.058 461.574 74.5115 585.369 198.306Z"
            fill="url(#paint0_radial)"
          />
        </g>
        <defs>
          <filter
            id="filter0_f"
            x="-31.7008"
            y="-141.59"
            width="1039.16"
            height="1039.16"
            filterUnits="userSpaceOnUse"
            colorInterpolationFilters="sRGB"
          >
            <feFlood floodOpacity={0} result="BackgroundImageFix" />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="BackgroundImageFix"
              result="shape"
            />
            <feGaussianBlur stdDeviation="62" result="effect1_foregroundBlur" />
          </filter>
          <radialGradient
            id="paint0_radial"
            cx="0"
            cy="0"
            r="1"
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(440.519 463.823) rotate(45) scale(316.996 316.996)"
          >
            <stop stopColor="#0255FB">
              <animate
                attributeName="stop-color"
                values="#0255FB; #7A5FFF; #FB02B5; #0255FB"
                dur="4s"
                repeatCount="indefinite"
              ></animate>
            </stop>
          </radialGradient>
        </defs>
      </svg>

      <svg
        className="svg-animated-lt"
        width="758"
        height="356"
        viewBox="0 0 558 356"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g opacity="0.6" filter="url(#filter0_f)">
          <path
            d="M438.056 23.1961C411.673 90.1082 60.5684 115.55 202.686 171.586C344.804 227.623 609.047 113.122 635.43 46.2099C661.813 -20.7023 567.992 -120.372 425.874 -176.408C283.756 -232.444 464.439 -43.7161 438.056 23.1961Z"
            fill="url(#paint0_radial)"
          />
        </g>
        <defs>
          <filter
            id="filter0_f"
            x="0.220581"
            y="-355.386"
            width="808.374"
            height="710.667"
            filterUnits="userSpaceOnUse"
            color-interpolation-filters="sRGB"
          >
            <feFlood flood-opacity="0" result="BackgroundImageFix" />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="BackgroundImageFix"
              result="shape"
            />
            <feGaussianBlur
              stdDeviation="84.3607"
              result="effect1_foregroundBlur"
            />
          </filter>
          <radialGradient
            id="filter0_f"
            cx="0"
            cy="0"
            r="1"
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(378.103 -55.2526) rotate(111.519) scale(164.392 349.16)"
          >
            <stop stop-color="#FB02B5"></stop>
            <stop offset="0.765625" stop-color="#0255FB" />
          </radialGradient>
        </defs>
      </svg>

      <svg
        className="svg-animated-lb"
        width="508"
        height="456"
        viewBox="0 0 508 456"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g opacity="0.7" filter="url(#filter0_f)">
          <path
            d="M348.023 248.622C383.869 282.291 426.676 255.133 393.059 290.923C359.443 326.713 303.132 328.433 267.286 294.764C231.441 261.095 236.744 266.23 270.361 230.44C303.977 194.65 312.177 214.953 348.023 248.622Z"
            fill="url(#paint0_radial)"
          />
        </g>
        <g opacity="0.7" filter="url(#filter1_f)">
          <path
            d="M238.208 115.535C274.218 149.358 252.976 186.474 219.378 222.244C185.78 258.014 152.549 278.893 116.539 245.07C80.5287 211.246 140.538 215.519 174.136 179.749C207.734 143.979 202.197 81.7111 238.208 115.535Z"
            fill="url(#paint1_radial)"
          />
        </g>
        <defs>
          <filter
            id="filter0_f"
            x="136.928"
            y="85.562"
            width="370.679"
            height="369.771"
            filterUnits="userSpaceOnUse"
            colorInterpolationFilters="sRGB"
          >
            <feFlood floodOpacity={0} result="BackgroundImageFix" />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="BackgroundImageFix"
              result="shape"
            />
            <feGaussianBlur
              stdDeviation="49.8033"
              result="effect1_foregroundBlur"
            />
          </filter>
          <filter
            id="filter1_f"
            x="0.393448"
            y="0.393448"
            width="380.667"
            height="384.903"
            filterUnits="userSpaceOnUse"
            colorInterpolationFilters="sRGB"
          >
            <feFlood floodOpacity={0} result="BackgroundImageFix" />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="BackgroundImageFix"
              result="shape"
            />
            <feGaussianBlur
              stdDeviation="49.8033"
              result="effect1_foregroundBlur"
            />
          </filter>
          <radialGradient
            id="paint0_radial"
            cx="0"
            cy="0"
            r="1"
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(328.155 229.96) rotate(43.2064) scale(112.402 112.227)"
          >
            <stop stopColor="#0255FB" />
            <stop offset="1" stopColor="#02D4FB" />
          </radialGradient>
          <radialGradient
            id="paint1_radial"
            cx="0"
            cy="0"
            r="1"
            gradientUnits="userSpaceOnUse"
            gradientTransform="translate(177.373 180.302) rotate(43.2064) scale(112.917 112.164)"
          >
            <stop stopColor="#0255FB" />
            <stop offset="1" stopColor="#02D4FB" />
          </radialGradient>
        </defs>
      </svg>
    </div>
  );
}

export default function Page({ children }) {
  return (
    <>
      <Header/>
      <CookiesConfirmation/>
      <div className={styles.container}>{children}</div>
      <SvgBackground />
    </>
  );
}
