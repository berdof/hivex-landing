import React from 'react';
import classnames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import NavLink from 'components/NavLink/NavLink';
import SocialLinks from 'components/SocialLinks/SocialLinks';
import Button from 'components/Button/Button';
import Dropdown from 'components/Dropdown/Dropdown';

import { TechnologiesSubroutes } from 'constants/mainConstants';
import MenuOpen from 'public/assets/menu-open.svg';
import MenuClose from 'public/assets/menu-close.svg';

import styles from './MobileMenu.module.scss';

function MobileMenu(props) {
  const { setIsMenuOpen, isMenuOpen } = props;

  function toggleMenu() {
    setIsMenuOpen((prevState) => !prevState);
  }

  function moveTo(path) {
    fullpage_api.moveTo(path);
    setIsMenuOpen(false);
  }

  return (
    <>
      <div
        className={classnames(
          styles.control,
          styles.mobileWrap,
        )}
      >
        <button className={styles.toggleMenuContainer}>
          {isMenuOpen ? (
            <MenuClose onClick={toggleMenu} />
          ) : (
            <MenuOpen onClick={toggleMenu} />
          )}
        </button>
      </div>
      <CSSTransition
        className={classnames(styles.container, styles.mobileWrap)}
        classNames={{
          enterDone: styles.menuOpen,
          exitDone: styles.menuClosed,
        }}
        in={isMenuOpen}
        timeout={0}
      >
        <div>
          <nav className={styles.nav}>
            <ul className={styles.navIn}>
              <li className={styles.navItem}>
                <NavLink route="/">
                  <a>Home</a>
                </NavLink>
              </li>
              <li className={styles.navItem}>
                <NavLink route="/work-with-us">
                  <a>Cooperation with us</a>
                </NavLink>
              </li>
              <li className={styles.navItem}>
                <Dropdown
                  name="Technologies"
                  className={styles.dropdown}
                  subroutes={TechnologiesSubroutes}
                />
              </li>
              <li className={styles.navItem}>
                <NavLink route="/company">
                  <a>Company</a>
                </NavLink>
              </li>
            </ul>
          </nav>
          <SocialLinks />
          <Button className={styles.btn} onClick={() => moveTo('contactUs')}>
            Start work with us
          </Button>
        </div>
      </CSSTransition>
      <CSSTransition
        className={classnames(styles.grayWrapper, styles.mobileWrap)}
        classNames={{
          enterDone: styles.wrapperOpen,
          exitDone: styles.wrapperClosed,
        }}
        in={isMenuOpen}
        timeout={0}
      >
        <div onClick={toggleMenu} />
      </CSSTransition>
    </>
  );
}

export default MobileMenu;
