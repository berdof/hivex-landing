import React, { useEffect } from 'react';
import Icon1 from 'public/assets/hexagon-top1.svg';
import Icon2 from 'public/assets/hexagon-top2.svg';
import Icon3 from 'public/assets/hexagon-top3.svg';
import Icon4 from 'public/assets/hexagon-top4.svg';
import Icon5 from 'public/assets/hexagon-top5.svg';
import Icon6 from 'public/assets/hexagon-top6.svg';
import Icon7 from 'public/assets/hexagon-top7.svg';

import styles from './Hexagons.module.scss';

export default function Hexagons({ children }) {
  useEffect(() => {
    const hexagons = document.querySelectorAll('.hexagons .hexagon');
    let hexagonIndex = 0;
    if (false)
      setInterval(() => {
        if (hexagonIndex > 0) {
          hexagons[hexagonIndex - 1].classList = 'hexagon';
        }
        if (hexagonIndex === hexagons.length) {
          hexagonIndex = 0;
        }

        hexagons[hexagonIndex].classList = 'hexagon active';
        hexagonIndex++;
      }, 4000);
  });

  return (
    <div className="hexagons">
      <div className="hexagon">
        <div className="hexagon-text">Front-end</div>
        <Icon1 className="hexagon-top" />
        <div className="hexagon-side"></div>
        <div className="hexagon-bottom"></div>
      </div>
      <div className="hexagon">
        <div className="hexagon-text">Front-end</div>
        <Icon2 className="hexagon-top" />
        <div className="hexagon-side"></div>
        <div className="hexagon-bottom"></div>
      </div>
      <div className="hexagon">
        <div className="hexagon-text">Front-end</div>
        <Icon3 className="hexagon-top" />
        <div className="hexagon-side"></div>
        <div className="hexagon-bottom"></div>
      </div>
      <div className="hexagon">
        <div className="hexagon-text">Front-end</div>
        <Icon4 className="hexagon-top" />
        <div className="hexagon-side"></div>
        <div className="hexagon-bottom"></div>
      </div>
      <div className="hexagon">
        <div className="hexagon-text">Front-end</div>
        <Icon5 className="hexagon-top" />
        <div className="hexagon-side"></div>
        <div className="hexagon-bottom"></div>
      </div>
      <div className="hexagon">
        <div className="hexagon-text">Front-end</div>
        <Icon6 className="hexagon-top" />
        <div className="hexagon-side"></div>
        <div className="hexagon-bottom"></div>
      </div>
    </div>
  );
}
