import React, { useState } from 'react';
import classnames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import NavLink from 'components/NavLink/NavLink';

import DropdownArrow from 'public/assets/dropdown-arrow.svg';

import styles from './Dropdown.module.scss';

function Dropdown(props) {
  const { name, subroutes, className = '' } = props;
  const [isOpen, setIsOpen] = useState(false);

  function toggleIsOpen() {
    setIsOpen((prevState) => !prevState);
  }

  return (
    <div className={classnames(styles.container, className)}>
      <div
        className={classnames(styles.name, {
          [styles.dropdownNameShowed]: isOpen,
        })}
        onClick={toggleIsOpen}
      >
        <span className={styles.title}>{name}</span>
        <DropdownArrow className={styles.arrow}/>
      </div>
      <CSSTransition
        className={styles.subroutesContainer}
        in={isOpen}
        classNames={{
          enterDone: styles.dropdownOpen,
          exitDone: styles.dropdownClosed,
        }}
        timeout={200}
      >
        <div>
          <ul className={styles.dropdownSubroutes} onMouseLeave={toggleIsOpen}>
            {subroutes.map((subroute) => (
              <li className={styles.dropdownSubroutesItem} key={subroute.route}>
                <NavLink route={subroute.route}>
                  <a>{subroute.name}</a>
                </NavLink>
              </li>
            ))}
          </ul>
        </div>
      </CSSTransition>
    </div>
  );
}

export default Dropdown;
