import React, { useState } from 'react';
import classnames from 'classnames';
import { useRouter } from 'next/router';

import Dropdown from 'components/Dropdown/Dropdown';
import NavLink from 'components/NavLink/NavLink';
import MobileMenu from 'components/MobileMenu/MobileMenu';
import Logo from 'public/assets/logo.svg';
import { TechnologiesSubroutes } from 'constants/mainConstants';

import styles from './Header.module.scss';

export default function Header(props) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { route } = useRouter();

  function openMenu() {
    setIsMenuOpen(true);
  }

  const isMainPage = route === '/';

  return (
    <header
      className={classnames(styles.container, {
        [styles.fixedToTop]: isMainPage,
      })}
    >
      <div className={styles.containerIn}>
        <div className={styles.left}>
          <a className={styles.logo} href="/">
            <Logo />
          </a>
        </div>
        <nav className={styles.nav}>
          <ul className={styles.navIn}>
            <li className={styles.navItem}>
              <NavLink route="/">
                <a>Home</a>
              </NavLink>
            </li>
            <li className={styles.navItem}>
              <NavLink route="/interaction-models/dedicated-team">
                <a>Cooperate with us</a>
              </NavLink>
            </li>
            {/*<li className={styles.navItem}>*/}
            {/*  <Dropdown name="Technologies" subroutes={TechnologiesSubroutes} />*/}
            {/*</li>*/}
            <li className={styles.navItem}>
              <NavLink route="/work-with-us">
                <a>Company</a>
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
      <MobileMenu isMenuOpen={isMenuOpen} setIsMenuOpen={setIsMenuOpen} />
    </header>
  );
}
