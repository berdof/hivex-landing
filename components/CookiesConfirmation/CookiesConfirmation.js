import React, { useEffect, useState } from 'react';
import classnames from 'classnames';

import styles from './CookiesConfirmation.module.scss';

const isCookiesAgreed = 'isCookiesAgreed';

function CookiesConfirmation(props) {
  const [isCookiesShowed, setIsCookiesShowed] = useState(false);

  useEffect(() => {
    const storageAgreementValue = JSON.parse(
      localStorage.getItem(isCookiesAgreed)
    );

    if (storageAgreementValue === null || storageAgreementValue === false) {
      setIsCookiesShowed(true);
    }
  }, []);

  function confirmCookies() {
    // ToDo: add actual cookies confirmation
    localStorage.setItem(isCookiesAgreed, true);
    setIsCookiesShowed(false);
  }
  return (
    <div className={classnames(styles.container, {
      [styles.hidden]: !isCookiesShowed
    })}>
      <div className={styles.info}>
        <span className={styles.title}>Cookies</span>
        <p className={styles.description}>
          By continuing to use the site hivex.group you agree to the use of
          cookies.
          <br />
          More information can be found in the Cookie Policy
        </p>
      </div>
      <button className={styles.agreeBtn} onClick={confirmCookies}>
        Agree
      </button>
    </div>
  );
}

export default CookiesConfirmation;
