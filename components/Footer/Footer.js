import classnames from 'classnames';

import SocialLinks from 'components/SocialLinks/SocialLinks';

import styles from './Footer.module.scss';

export default function Footer({className = ''}) {
  return (
    <footer className={classnames(styles.container, className)}>
      <div className={styles.info}>
        <span className={styles.infoHead}>
          Estonia, Tallin, Kesklinna linnaosa, Tuukri tn 19-315
        </span>
        {false && <a className={styles.infoMapView}>View on the map</a>}
        {false &&<SocialLinks />}
        <span className={styles.infoCopyRight}>
          Copyright © {(new Date()).getFullYear()}. Hivex
        </span>
      </div>
      <nav>
        <span className={styles.columnHead}>Company</span>
        <ul className={styles.navIn}>
          <li className={styles.navItem} data-menuanchor="howWeWork">
            <a href="#howWeWork">How we are working</a>
          </li>
          <li className={styles.navItem} data-menuanchor="technologies">
            <a href="#technologies">Technologies</a>
          </li>
          <li className={styles.navItem} data-menuanchor="portfolio">
            <a href="#portfolio">Portfolio</a>
          </li>
          <li className={styles.navItem} data-menuanchor="reviews">
            <a href="#reviews">Reviews</a>
          </li>
          <li className={styles.navItem} data-menuanchor="contactUs">
            <a href="#contactUs">Contact us</a>
          </li>
        </ul>
      </nav>
      <div className={styles.cooperation}>
        <span className={styles.columnHead}>Cooperation</span>
        <a
          className={styles.cooperationEmailUs}
          href="mailto:hello@hivex.group"
        >
          hello@hivex.group
        </a>
      </div>
    </footer>
  );
}
