import React from 'react';
import classnames from 'classnames';

import Team from 'public/assets/interaction-models/team.svg';
import Validated from 'public/assets/interaction-models/validated.svg';
import Complete from 'public/assets/interaction-models/complete.svg';
import Tools from 'public/assets/interaction-models/tools.svg';
import CustomerControl from 'public/assets/interaction-models/validated.svg';
import Changes from 'public/assets/interaction-models/changes.svg';
import Tasks from 'public/assets/interaction-models/tasks.svg';
import Targets from 'public/assets/interaction-models/targets.svg';
import ChangesGear from 'public/assets/interaction-models/changes-gear.svg';

import WebFrontend from 'public/assets/technologies/title/front.svg';
import WebBackend from 'public/assets/technologies/title/back.svg';
import Mobile from 'public/assets/technologies/title/mobile.svg';
import QualityAssurance from 'public/assets/technologies/title/qa.svg';
import DevOps from 'public/assets/technologies/title/devops.svg';
import MachineLearning from 'public/assets/technologies/title/ai.svg';
import BigData from 'public/assets/technologies/title/bigdata.svg';

import Java from 'public/assets/technologies/languages/java.svg';
import AmazonAWS from 'public/assets/technologies/languages/amazon-aws.svg';
import Android from 'public/assets/technologies/languages/android.svg';
import Django from 'public/assets/technologies/languages/django.svg';
import HP from 'public/assets/technologies/languages/hp.svg';
import JMeter from 'public/assets/technologies/languages/jmeter.svg';
import Linux from 'public/assets/technologies/languages/linux.svg';
import Python from 'public/assets/technologies/languages/python.svg';
import Ruby from 'public/assets/technologies/languages/ruby.svg';
import Spark from 'public/assets/technologies/languages/spark.svg';


import styles from './IdIcon.module.scss';

const WorkflowIconList = {
  team: <Team className={styles.workflow} />,
  validated: <Validated className={styles.workflow} />,
  complete: <Complete className={styles.workflow} />,
  tools: <Tools className={styles.workflow} />,
  ['customer-control']: <CustomerControl className={styles.workflow} />,
  ['changes']: <Changes className={styles.workflow} />,
  ['tasks']: <Tasks className={styles.workflow} />,
  ['targets']: <Targets className={styles.workflow} />,
  ['changes-gear']: <ChangesGear className={styles.workflow} />,
};

const TechnologiesIconList = {
  ['web-frontend']: <WebFrontend />,
  ['web-backend']: <WebBackend />,
  mobile: <Mobile />,
  ['dev-ops']: <DevOps />,
  ['machine-learning']: <MachineLearning />,
  qa: <QualityAssurance />,
  ['big-data']: <BigData />,
};

// TODO add more icons
const ProgrammingLanguagesIconList = {
  react: <Java className={styles.langIcon} />,
  ember: <Java className={styles.langIcon} />,
  vue: <Java className={styles.langIcon} />,
  svetle: <Java className={styles.langIcon} />,
  angular: <Java className={styles.langIcon} />,
  android: <Android className={styles.langIcon} />,
  ios: <Java className={styles.langIcon} />,
  java: <Java className={styles.langIcon} />,
  ['c-sharp']: <Java className={styles.langIcon} />,
  swift: <Java className={styles.langIcon} />,
  python: <Python className={styles.langIcon} />,
  ['c-plus-plus']: <Java className={styles.langIcon} />,
  hp: <HP className={styles.langIcon} />,
  jira: <Java className={styles.langIcon} />,
  jmeter: <JMeter className={styles.langIcon} />,
  qa: <Java className={styles.langIcon} />,
  selenium: <Java className={styles.langIcon} />,
  ruby: <Ruby className={styles.langIcon} />,
  django: <Django className={styles.langIcon} />,
  php: <Java className={styles.langIcon} />,
  laravel: <Java className={styles.langIcon} />,
  ['amazon-aws']: <AmazonAWS className={styles.langIcon} />,
  chef: <Java className={styles.langIcon} />,
  linux: <Linux className={styles.langIcon} />,
  puppet: <Java className={styles.langIcon} />,
  ansible: <Java className={styles.langIcon} />,
  spark: <Spark className={styles.langIcon} />,
  kafka: <Java className={styles.langIcon} />,
  hadoop: <Java className={styles.langIcon} />,
};

const proxyHandler = {
  get(target, prop) {
    if (prop in target) {
      return target[prop];
    } else {
      return null;
    }
  },
};

function IdIcon({ IconList = {}, name = '' }) {
  const value = name.toLowerCase();

  return IconList[value];
}

export function WorkflowIcon(props) {
  const IconList = new Proxy(WorkflowIconList, proxyHandler);

  return <IdIcon IconList={IconList} {...props} />;
}

export function TechnologiesIcon(props) {
  const IconList = new Proxy(TechnologiesIconList, proxyHandler);

  return <IdIcon IconList={IconList} {...props} />;
}

export function ProgrammingLanguagesIcon({ isSelected = false, ...props }) {
  const IconList = new Proxy(ProgrammingLanguagesIconList, proxyHandler);

  return (
    <div className={styles.programmingLanguage}>
      <img
        className={styles.hexagonItem}
        src="/assets/technologies/languages/hexagon.svg"
        alt="Hexagon"
      />
      <div className={styles.langContainer}>
        <img
          className={classnames(styles.hexagonBg, {
            [styles.hexagonBgActive]: isSelected,
          })}
          src="/assets/technologies/languages/hexagon-bg.svg"
          alt="Hexagon background"
        />
        <IdIcon IconList={IconList} {...props} />
      </div>
    </div>
  );
}

export default IdIcon;
