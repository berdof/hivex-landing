import React from 'react';
import classnames from 'classnames';

import styles from './Button.module.scss';

export default function Button(props) {
  const {
    className = '',
    children,
    secondary = false,
    onClick = () => {},
    disabled = false,
  } = props;
  return (
    <button
      className={classnames(
        styles.btn,
        className, {
          [styles.btnSecondary]: secondary,
          [styles.btnMain]: !secondary,
        }
      )}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
