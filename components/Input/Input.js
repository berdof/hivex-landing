import React from 'react';

import styles from './Input.module.scss';

export default function Input({ id, placeholder, required, type, rows = 5 }) {
  const inputId = `input_id_${id}`;
  const inputPlaceholder = `${placeholder} ${required ? '*' : ''}`;

  const InputComponent = () =>
    type === 'textarea' ? (
      <textarea
        required={required}
        id={inputId}
        rows={rows}
        className={styles.field}
        name={id}
        placeholder={inputPlaceholder}
      />
    ) : (
      <input
        required={required}
        type={type}
        id={inputId}
        className={styles.field}
        name={id}
        placeholder={inputPlaceholder}
      />
    );

  return (
    <div className={styles.wrapper}>
      <InputComponent />
      <label htmlFor={inputId} className={styles.label}>
        <span>{inputPlaceholder}</span>
      </label>
      <div className={styles.underline} />
    </div>
  );
}
